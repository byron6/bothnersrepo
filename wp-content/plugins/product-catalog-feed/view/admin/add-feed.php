<?php

require_once dirname(__FILE__).'/../../inc/countries.php';

class FeedFBGooglePro {
    /* FeedFBGooglePro - class for optimazing fields render in this view */

    private function _isValidAsArray($d){
       return (count($d) && is_array($d)) ? true : false;
    }

    public function addClassForPro($forpro=false){
        if($forpro) return " not_in_free";
        return "";
    }
    public function addLinkForPro($forpro=false)
    {
        if ($forpro) return '<span class="unlock_pro_features">PRO Option: <a target="_blank" href="http://www.pixelyoursite.com/product-catalog-facebook">Click to Upgrade</a></span>';
        return "";
    }
    public function showAttributes($attributes){
        $sResult = "";
        if( $this->_isValidAsArray($attributes) ){
            foreach ($attributes as $attr => $val){
                $sResult.=" ".$attr."='".str_replace('\'', '\\\'', $val)."' ";
            }
        }
        return $sResult;

    }
    public function showCustomOprions($aCcustom,$selval="") {
        $sResult = "";
        if ($this->_isValidAsArray($aCcustom)) {
            foreach ($aCcustom as $text => $val) {
                $sResult .= "<option " . ( ($selval==$val) ? " selected='selected' " :  "")  . " value='" . htmlspecialchars($val, ENT_QUOTES) . "'>" . htmlspecialchars($text, ENT_QUOTES) . "</option>";
            }
        }
        return $sResult;
    }

    public function addCssForFeed( $feed_type ) {
            $sCssClass="";
            if ($this->_isValidAsArray($feed_type)) {
                foreach ($feed_type as $ftp) $sCssClass .= " stl-" . $ftp;
            }
        return $sCssClass;
    }
    
    public function buidCountryValues($field,$fieldkey){
        $sResult="";
        if(count($field['custom'])) {
            global $wpwoof_values;

            $selected = (empty($wpwoof_values['field_mapping']['tax_countries']['value']));
            $sResult.="<select id='ID".$fieldkey."' name='field_mapping[".$fieldkey."][value]' >";
            $sResult.="<option ".( $selected ? " selected " : "" )." value='' >" . 	__('select', 'woocommerce_wpwoof') ."</option>";
            $flds = array();
            foreach ($field['custom'] as $shcode){
                $shcode['shcode'] = empty($shcode['shcode']) ? '*': $shcode['shcode'];
                if(!in_array($shcode['shcode'],$flds)) {
                    $flds[] = $shcode['shcode'];
                    $sResult .= "<option ";
                    if (!$selected && $shcode['shcode'] == $wpwoof_values['field_mapping']['tax_countries']['value']) {
                        $sResult .= " selected ";
                    }
                    $sResult .= " value='" . htmlspecialchars($shcode['shcode'], ENT_QUOTES) . "'>";
                    $sResult .= WpWoof_get_feed_pro_countries($shcode['shcode']) . "</option>";
                }

            }
            $sResult.="</select>";
        }
        return $sResult;    
    }
}

$oFeedFBGooglePro = new FeedFBGooglePro();


?><div class="wpwoof-box">
    <div class="wpwoof-addfeed-top">

        <div class="addfeed-top-field"><p>
            <label class="addfeed-top-label addfeed-bigger">Feed Name</label>
            <span class="addfeed-top-value">
                <input type="text" name="feed_name" value="<?php echo isset($wpwoof_values['feed_name']) ? $wpwoof_values['feed_name'] : ''; ?>" />
                <?php if( !empty($wpwoofeed_oldname) ) { ?>
                    <input type="hidden" name="old_feed_name" value="<?php echo $wpwoofeed_oldname; ?>" style="display:none" />
                <?php } ?>
            </span>
        </p></div>
        <div class="addfeed-top-field"><p>
            <label class="addfeed-top-label addfeed-bigger">Feed Type</label>
            <span class="addfeed-top-value">
                <select id="ID-feed_type" name="feed_type" onchange="toggleFeedField(this.value);">
                    <option <?php if(isset($wpwoof_values['feed_type'])) { selected( "facebook", $wpwoof_values['feed_type'], true); } ?> value="facebook">Facebook Product Catalog</option>
                    <option <?php if(isset($wpwoof_values['feed_type'])) { selected( "google", $wpwoof_values['feed_type'], true); } ?> value="google">Google Product Catalog</option>
                </select>
            </span>
        </p></div>

    </div>
    <?php
    if (function_exists('icl_get_languages')) {?>
        <hr class="wpwoof-break" />
        <h4 class="wpwoofeed-section-heading"><?php  echo __('Feed Language Settings', 'woocommerce_wpwoof'); ?></h4>
        <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label addfeed-bigger"><?php  echo __('Include multilingual products', 'woocommerce_wpwoof'); ?></label>
                <span class="addfeed-top-value">
                        <select name="feed_use_lang">
                            <?php
                            $sel = (!empty($wpwoof_values['feed_use_lang'])) ? $wpwoof_values['feed_use_lang'] : ICL_LANGUAGE_CODE;
                           
                            ?>
                            <option value="all" <?php if($sel=='all') echo "selected='selected'" ?> ><?php  echo __('All Languages', 'woocommerce_wpwoof'); ?></option>
                            <?php
                            $aLanguages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
                            foreach($aLanguages as $lang){
                                ?><option value="<?php echo $lang['language_code']; ?>" <?php if($sel==$lang['language_code']) echo "selected='selected'" ?>><?php echo $lang['translated_name']; ?></option><?php
                            }
                            ?>
                        </select>
                </span>
        </p>
        <p class="description"><span></span><span>Select to include items for choosen language / All languages.</span></p>
        </div><?php } ?>
    <?php
    $taxSrc = admin_url('admin-ajax.php');
    $taxSrc = add_query_arg( array( 'action'=>'wpwoofgtaxonmy'), $taxSrc);

    $google_cats = '';
    if(isset($wpwoof_values['feed_google_category_id']))
        $google_cats = $wpwoof_values['feed_google_category_id'];
    if( strpos($google_cats, ',') !== false ) {
        $google_cats = explode(',', $google_cats);
        $preselect = '';
        if(!empty($google_cats) && is_array($google_cats)){
            foreach ($google_cats as $google_cat) {
                $preselect .= "'".$google_cat."', ";
            }
            $preselect = rtrim($preselect, ', ');
        }
    } else {
        $preselect = "'$google_cats'";
    }
?>
<script type="text/javascript">
    function showHideCountries(value){
        if(value=='false') jQuery('.CSS_tax_countries').hide();
        else  jQuery('.CSS_tax_countries').show();   
    }

 
    
    
    function showSKUorID(){
        var sText = (jQuery("#ID-feed_type").val()=='google') ? "SKU" : "ID";
        jQuery("#ID_mpn_field option").each(function() {
            if(jQuery(this).text() == sText) {
                jQuery(this).attr('selected', 'selected');
            }
        });

    }
    function toggleFeedField(sClass){
        jQuery(".stl-google").toggle();
        jQuery(".stl-facebook").toggle();
        showSKUorID();
    }
    function toggleExtra(sClass){
        if(sClass){
            jQuery("."+sClass).toggle();
            if(jQuery("."+sClass).is(":visible") ){
                jQuery('#ID_ch_MoreImages').attr("checked",true);
            }else{
                jQuery('#ID_ch_MoreImages').attr("checked",false);
            }
        }else {
            jQuery("#IDextrafields").toggle();
        }
        return false;
    }
jQuery(document).ready(function($) {

   
    
    jQuery('#ID-feed_type').val();
    jQuery("[class*=' stl-']").hide();
    jQuery(".stl-"+jQuery('#ID-feed_type').val()).show();
    jQuery(".additional_image_link").hide();
    showSKUorID();
    var options = {
        empty_value: 'null',
        indexed: true,  // the data in tree is indexed by values (ids), not by labels
        on_each_change: '<?php echo $taxSrc; ?>', // this file will be called with 'id' parameter, JSON data must be returned
        choose: function(level) {
                    if( level < 1 )
                        return 'Select Main Category';
                    else 
                        return 'Select Sub Category';
                },
        loading_image: '<?php echo home_url( '/wp-includes/images/wpspin.gif');?>',
        get_parent_value_if_empty: true,
        set_value_on: 'each',
        preselect: {'wpwoof_google_category': [<?php echo $preselect; ?>]}
    };

    var displayParents = function() {
        var labels = []; // initialize array
        var IDs = []; // initialize array
        $(this).siblings('select') // find all select
        .find(':selected') // and their current options
        .each(function() { 

        if( $(this).text() != 'Select Main Category' &&  $(this).text() != 'Select Sub Category'){     
            if( $(this).val() != ''){
                labels.push($(this).text()); 
                IDs.push($(this).val()); 
            }
        }

        }); // and add option text to array
        $('#wpwoof_google_category_result').text(labels.join(' > ')); // and display the labels
        $('#feed_google_category').val( labels.join(' > ') );
        $('#feed_google_category_id').val( IDs.join(',') );
    }




    if($('#ID_tax_field').length){
        showHideCountries($('#ID_tax_field').val());
    }

    $.getJSON('<?php echo $taxSrc; ?>', function(tree) { // initialize the tree by loading the file first
        $('#wpwoof_google_category').optionTree(tree, options).change(displayParents);
    });
});
</script>
    <hr class="wpwoof-break" />
    <div class="wpwoof-addfeed-fields">
        <h4 class="wpwoofeed-section-heading">Field Mapping</h4>
        <p>We do our best to do the mapping automatically, but if there something you want to change, use the dropdown to do the re-map.</p>
        <?php  
        $all_fields = wpwoof_get_all_fields();

        $required_fields = $all_fields['required'];
        $extra_fields = $all_fields['extra'];

        $condition_fields = array();
        $meta_keys = wpwoof_get_product_fields();
        $meta_keys_sort = wpwoof_get_product_fields_sort();
        $attributes = wpwoof_get_all_attributes();


        foreach ($required_fields as $fieldkey => $field) {

            $sCssClass = "";
            if( isset($field['dependet']) ) continue;
            if(isset($field['feed_type'])) $sCssClass = $oFeedFBGooglePro->addCssForFeed($field['feed_type']);

            if( !empty($field['delimiter']) ) {
                ?><hr class="wpwoof-break wpwoof-break-small<?php echo $sCssClass; ?>"/><?php
            }
            if(!empty($field['header']) ){ ?>
                <div class="wpwoof-definefield-top<?php echo $sCssClass; ?>">
                    <label class="wpwoofeed-section-heading"><?php echo $field['header']; ?></label>
                    <p><?php if(!empty($field['headerdesc'])) echo $field['headerdesc'];?></p>
                </div><?php
            }
            if(isset($field['type']) && $field['type']=='checkbox'){
                ?><div class="wpwoof-requiredfield-settings wpwoof-field-wrap stl-facebook stl-all stl-google" style="display: block;">
                <p>
                    <label class="wpwoof-required-label"></label>
                       <span class="wpwoof-required-value">
                           <label><input type="checkbox" class='wpwoof-mapping' value="1" name="field_mapping[<?php echo $fieldkey; ?>]"<?php
                               echo !empty($wpwoof_values['field_mapping'][$fieldkey]) ? " checked " : '';
                               ?> /> <?php echo $field['label']; ?></label>
                        </span>
                </p>
                </div><?php
            } else   if( !isset($field['define']) ) { ?>
                <div class="wpwoof-requiredfield-settings wpwoof-field-wrap<?php echo $sCssClass ?>">
                <p>
                <label class="wpwoof-required-label"><?php echo $fieldkey ?>:</label>
                <span class="wpwoof-required-value">
                <select <?php
                if (isset($field['attr'])) echo $oFeedFBGooglePro->showAttributes($field['attr']);
                ?> name="field_mapping[<?php echo $fieldkey; ?>][value]" class="wpwoof_mapping wpwoof_mapping_option">
                <?php
                $html = '';
                if (isset($field['custom'])) {
                    $html = $oFeedFBGooglePro->showCustomOprions($field['custom'], ( empty($wpwoof_values['field_mapping'][$fieldkey]['value']) ? "" : $wpwoof_values['field_mapping'][$fieldkey]['value'] ) );
                } else {
                    if (isset($field['woocommerce_default'])) {
                        if (empty($wpwoof_values['field_mapping'][$fieldkey]['value'])) {
                            if (empty($wpwoof_values['field_mapping']) || !is_array($wpwoof_values['field_mapping'])) {
                                $wpwoof_values['field_mapping'] = array();
                            }
                            if (empty($wpwoof_values['field_mapping'][$fieldkey]) || !is_array($wpwoof_values['field_mapping'][$fieldkey])) {
                                $wpwoof_values['field_mapping'][$fieldkey] = array();
                            }
                            $wpwoof_values['field_mapping'][$fieldkey]['value'] = 'wpwoofdefa_' . $field['woocommerce_default']['value'];
                        }
                    } else {
                        $html .= '<option value="">select</option>';
                    }
                    $meta_keys_remove = $meta_keys;
                    foreach ($meta_keys_sort['sort'] as $sort_id => $meta_fields) {
                        $html .= '<optgroup label="' . $meta_keys_sort['name'][$sort_id] . '">';
                        foreach ($meta_fields as $key) {
                            $value = $meta_keys[$key];
                            unset($meta_keys_remove[$key]);
                            $html .= '<option '.( !empty($value['disabled']) ? ' disabled="disabled" ': '' ).' value="wpwoofdefa_' . $key . '" ' . selected('wpwoofdefa_' . $key, isset($wpwoof_values['field_mapping'][$fieldkey]['value']) ? $wpwoof_values['field_mapping'][$fieldkey]['value']:"", false) . ' >' . $value['label'] . '</option>';
                        }
                        $html .= '</optgroup>';
                    }
                    $html .= '<optgroup label="Product Attributes">';
                    foreach ($attributes as $key => $value) {
                        $html .= '<option value="wpwoofattr_' . $key . '" ' . selected('wpwoofattr_' . $key, isset($wpwoof_values['field_mapping'][$fieldkey]['value']) ? $wpwoof_values['field_mapping'][$fieldkey]['value'] : "", false) . ' >' . $value . '</option>';
                    }
                    $html .= '</optgroup>';

                }
                echo $html;
                ?></select><?php
                    wpwoofeed_custom_attribute_input($fieldkey, $field, $wpwoof_values);
                ?></span><?php
                 if(isset($field['forpro'])) {
                        ?><span class="unlock_pro_features">PRO Option: <a target="_blank" href="http://www.pixelyoursite.com/product-catalog-facebook">Click to Upgrade</a></span><?php
                 }
                 ?></p>
                <p class="description"><span></span><span><?php echo $field['desc']; ?></span></p>
                <?php
                if(!empty($field['second_field']) && isset($required_fields[$field['second_field']] ) ) {
                    $depfield = $required_fields[$field['second_field']];
                    ?><p  <?php if( isset($depfield['attr']) )   echo $oFeedFBGooglePro->showAttributes($depfield['attr']); ?> >
                       <label class="wpwoof-required-label"><?php echo ( (!empty($depfield['label'])) ? $depfield['label'] : "") ?>:</label>
                       <span class="wpwoof-define-value">                          
                        <?php
                        if(!empty($depfield['rendervalues']) && method_exists($oFeedFBGooglePro,$depfield['rendervalues'])){
                            echo $oFeedFBGooglePro->{$depfield['rendervalues']}($depfield,$field['second_field']);
                        }?>
                       </span>
                    </p>
                    <p  class="description" >
                        <span></span>
                        <span <?php if( isset($depfield['attr']) )   echo $oFeedFBGooglePro->showAttributes($depfield['attr']); ?> ><?php echo $depfield['desc']; ?></span>
                    </p>
                    <?php
                }
                if( !empty($field['callback']) && function_exists($field['callback']) ) {
                        $field['callback']($fieldkey, $field, $wpwoof_values);
                 }
                ?>
                </div><?php


            } else {
                $condition_fields[$fieldkey] = $field;
            }
        } ?>

        <?php foreach ($condition_fields as $definekey => $definevalue) {

            $sCssClass = "";
            if(isset($definevalue['feed_type'])) foreach ($definevalue['feed_type'] as $ftp){
                $sCssClass.= " stl-".$ftp;
            }
            
            ?>

            <div class="wpwoof-definefield-top<?php echo $sCssClass; ?>">
                <label class="wpwoofeed-section-heading"><?php echo $definevalue['define']['label']; ?></label>
                <p><?php echo $definevalue['define']['desc']; ?></p>
            </div>
            <div class="wpwoof-definefield-settings wpwoof-field-wrap">
                <p>
                <?php
                if( isset($definevalue['woocommerce_default']) && $definekey == "brand" ) {
                    $html2 = '';     
                    $html = '';     
                    $post_type = "product";
                    $check = 0;
                    $default_value = '';
                    foreach( $attributes as $taxonomy_name => $value ) {
                        if( ($taxonomy_name != 'product_cat') && ($taxonomy_name != 'product_tag') && ($taxonomy_name != 'product_type')
                            && ($taxonomy_name != 'product_shipping_class') ) {
                            if( ! $default_value ) {
                                $default_value = 'wpwoofattr_'.$taxonomy_name;
                            }
                            if( strpos($taxonomy_name, "brand") !== false ) {
                                $default_value = 'wpwoofattr_'.$taxonomy_name;
                            }
                        }
                    }
                    if(empty($default_value)) $default_value = 'site_name';
                   
                    if( empty( $wpwoof_values['field_mapping'][$definekey]['value'] ) ) {
                        if( empty($wpwoof_values['field_mapping']) || !is_array($wpwoof_values['field_mapping']) ) {
                            $wpwoof_values['field_mapping'] = array();
                        }
                        if( empty($wpwoof_values['field_mapping'][$definekey]) || !is_array($wpwoof_values['field_mapping'][$definekey]) ) {
                            $wpwoof_values['field_mapping'][$definekey] = array();
                        }
                        $wpwoof_values['field_mapping'][$definekey]['value'] = $default_value;
                    }
                    foreach( $attributes as $taxonomy_name => $value ) {
                        $check = 1;
                        $val = '';
                        if( isset($wpwoof_values['field_mapping'][$definekey]['value']) )
                            $val = $wpwoof_values['field_mapping'][$definekey]['value'];
                        $html2 .= '<option value="wpwoofattr_'.$taxonomy_name.'" '.selected('wpwoofattr_'.$taxonomy_name, $val, false).'>'.$value.'</option>'; 
                    }
                    $html2 = '<optgroup label="Product Attributes">'.$html2.'</optgroup>';
                    $meta_keys = wpwoof_get_product_fields();
                    $meta_keys_remove = $meta_keys;
                    foreach($meta_keys_sort['sort'] as $sort_id => $meta_fields) {
                        $html .= '<optgroup label="'.$meta_keys_sort['name'][$sort_id].'">';
                        foreach($meta_fields as $key) {
                            $value = $meta_keys[$key];
                            unset($meta_keys_remove[$key]);
                            $html .= '<option '.( !empty($value['disabled']) ? ' disabled="disabled" ': '' ).' value="wpwoofdefa_'.$key.'" '.selected('wpwoofdefa_'.$key, (empty($wpwoof_values['field_mapping'][$definekey]['value']) ? '' : $wpwoof_values['field_mapping'][$definekey]['value']), false).' >'.$value['label'].'</option>';
                        }
                        $html .= '</optgroup>';
                    } ?>
                        <label class="wpwoof-define-label"><?php echo $definekey; ?>:</label>
                        <span class="wpwoof-define-value">
                            <select name="field_mapping[<?php echo $definekey ?>][value]" id="ID_wpwoof_brand_select" class="wpwoof_mapping_option">
                                <?php echo $html, $html2; ?>
                            </select>
                        </span><?php
                }

                if($definekey != "brand") { ?>
                    <label class="wpwoof-define-label"><?php echo $definekey; ?>:</label>
                    <span class="wpwoof-define-value">
                        <select name="field_mapping[<?php echo $definekey ?>][value]" class="wpwoof_mapping_option">
                            <?php 
                            $html = '';
                            if( isset($definevalue['woocommerce_default']) ) {
                                if( empty( $wpwoof_values['field_mapping'][$fieldkey]['value'] ) ) {
                                    if( empty($wpwoof_values['field_mapping']) || !is_array($wpwoof_values['field_mapping']) ) {
                                        $wpwoof_values['field_mapping'] = array();
                                    }
                                    if( empty($wpwoof_values['field_mapping'][$fieldkey]) || !is_array($wpwoof_values['field_mapping'][$fieldkey]) ) {
                                        $wpwoof_values['field_mapping'][$fieldkey] = array();
                                    }
                                    $wpwoof_values['field_mapping'][$fieldkey]['value'] = 'wpwoofdefa_'.$definevalue['woocommerce_default']['value'];
                                }
                            } else {
                                $html .= '<option value="">select</option>';
                            }

                            $meta_keys = wpwoof_get_product_fields();
                            $meta_keys_remove = $meta_keys;
                            foreach($meta_keys_sort['sort'] as $sort_id => $meta_fields) {
                                $html .= '<optgroup label="'.$meta_keys_sort['name'][$sort_id].'">';
                                foreach($meta_fields as $key) {
                                    $value = $meta_keys[$key];
                                    unset($meta_keys_remove[$key]);
                                    $html .= '<option '.( !empty($value['disabled']) ? ' disabled="disabled" ': '' ).' value="wpwoofdefa_'.$key.'" '.selected('wpwoofdefa_'.$key, isset($wpwoof_values['field_mapping'][$fieldkey]['value']) ? $wpwoof_values['field_mapping'][$fieldkey]['value'] :"", false).' >'.$value['label'].'</option>';
                                }
                                $html .= '</optgroup>';
                            }
                            $attributes = wpwoof_get_all_attributes();
                            $html .= '<optgroup label="Product Attributes">';
                            foreach ($attributes as $key => $value) {
                                $html .= '<option value="wpwoofattr_'.$key.'" '.selected( 'wpwoofattr_'.$key, isset($wpwoof_values['field_mapping'][$definekey]['value']) ? $wpwoof_values['field_mapping'][$definekey]['value'] : "", false).' >'.$value.'</option>';
                            }
                            $html .= '</optgroup>';
                            echo $html;
                            ?>
                        </select>
                    </span>
                <?php
                } ?>

                </p>
                <p>
                    <span class="wpwoof-define-label<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">Use this when <?php echo $definekey; ?> is missing:</span>
                    <?php if( isset($definevalue['define']['values']) ) { ?>    
                        <span class="wpwoof-define-value<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">
                            <select name="field_mapping[<?php echo $definekey ?>][define][missingvalue]">
                                <?php 
                                $pieces = explode(',', $definevalue['define']['values']);
                                foreach ($pieces as $piecekey => $piecevalue) {
                                    echo '<option 
                                    '.selected( $piecevalue, isset($wpwoof_values['field_mapping'][$definekey]['define']['missingvalue']) ? $wpwoof_values['field_mapping'][$definekey]['define']['missingvalue'] : "", false).' value="'.$piecevalue.'">
                                    '.$piecevalue.'
                                    </option>';
                                }
                                ?>
                            </select>
                        </span>
                    <?php }  else { ?>
                        <span class="wpwoof-define-value<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">
                            <?php 
                            if( isset($wpwoof_values['field_mapping'][$definekey]['define']['missingvalue']) ) {
                                $brand_missingvalue = $wpwoof_values['field_mapping'][$definekey]['define']['missingvalue'];
                            } else {
                                $brand_missingvalue = '';
                            } ?>
                            <input type="text" <?php echo ($definekey == "brand" ) ? "id=\"ID_wpwoof_brand_text\"" : "" ?> name="field_mapping[<?php echo $definekey ?>][define][missingvalue]" value="<?php echo  $brand_missingvalue; ?>" />
                        </span>
                    <?php  } ?>
                    <?php echo $oFeedFBGooglePro->addLinkForPro(isset($definevalue['forpro']));?>
                </p>
                <p style="text-align: center;"><b>OR define a global value</b></p>
                <p>
                    <span class="wpwoof-define-label wpwoof-defineg-label<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">
                        <input type="checkbox" value="1" name="field_mapping[<?php echo $definekey ?>][define][global]" 
                        <?php 
                        $checkvalue = isset($wpwoof_values['field_mapping'][$definekey]['define']['global']) ? $wpwoof_values['field_mapping'][$definekey]['define']['global'] : '';
                        checked(1, $checkvalue, true); 
                        ?>
                        />Global <?php echo $definekey; ?> is:
                    </span>
                    <?php if( isset($definevalue['define']['values']) ) { ?>    
                        <span class="wpwoof-define-value<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">
                        <select name="field_mapping[<?php echo $definekey ?>][define][globalvalue]">
                            <?php 
                            $pieces = explode(',', $definevalue['define']['values']);
                            foreach ( $pieces as $piecekey => $piecevalue ) {
                                echo '<option 
                                '.selected( $piecevalue, isset($wpwoof_values['field_mapping'][$definekey]['define']['globalvalue']) ? $wpwoof_values['field_mapping'][$definekey]['define']['globalvalue'] : "", false).' value="'.$piecevalue.'">
                                '.$piecevalue.'
                                </option>';
                            }
                            ?>
                        </select>
                        </span>
                    <?php } else { ?>
                        <?php if( isset($wpwoof_values['field_mapping'][$definekey]['define']['globalvalue']) ) { 
                            $brand_globalvalue = $wpwoof_values['field_mapping'][$definekey]['define']['globalvalue'];
                        } else {
                            $brand_globalvalue = '';
                        } ?>
                        <span class="wpwoof-define-value<?php echo $oFeedFBGooglePro->addClassForPro(isset($definevalue['forpro']));?>">
                            <input type="text" name="field_mapping[<?php echo $definekey ?>][define][globalvalue]" value="<?php echo $brand_globalvalue; ?>" />
                        </span>
                    <?php } ?>
                    <?php echo $oFeedFBGooglePro->addLinkForPro(isset($definevalue['forpro']));?>
                </p>
            </div>
            <hr class="wpwoof-break wpwoof-break-small"/>
        <?php } ?>

        <?php /* Put Extra fields Block */ ?>

        <div class="addfeed-top-field">
            <strong class="wpwoofeed-section-heading" style="font-size:1.4em;display: block;text-align:center;">Google Product Taxonomy</strong>
            <p>
                <label class="addfeed-top-label" title='If you leave this blank you might see the following warning when creating the Product Catalog: "The following products were uploaded but have issues that might impact ads: Without google_product_category information, your products may not appear the way you want them to in ads.'>
                    Select Google Product Taxonomy <br>
                    <a href="https://support.google.com/merchants/answer/160081" target="_blank" title='If you leave this blank you might see the following warning when creating the Product Catalog: "The following products were uploaded but have issues that might impact ads: Without google_product_category information, your products may not appear the way you want them to in ads.'>Help</a>
                    <input type="hidden" name="feed_google_category" value="" id="feed_google_category" />
                    <input type="hidden" name="feed_google_category_id" value="" id="feed_google_category_id" />
                </label>

                <span class="addfeed-top-value">
                    <input type="text" name="wpwoof_google_category" id="wpwoof_google_category" style='display:none;' />
                </span>
            </p>
        </div>

        <div class="wpwoof-definefield-top" style="margin-bottom: 40px;">
            <a href="javascript:void(0);" class="wpwoofeed-section-heading" onclick="toggleExtra();">Optional Fields</a>
        </div>
        <div id="IDextrafields" style="display:none;margin-bottom: 40px;">
        <?php
            $styleHide="";
            foreach ($extra_fields as $fieldkey => $field) {
                if($fieldkey=='product_image' || $fieldkey=='identifier_exists') continue;
                $sCssClass = "";
                if(isset($field['feed_type'])) foreach ($field['feed_type'] as $ftp){
                    $sCssClass.= " stl-".$ftp;
                }
                

                if( stripos($fieldkey,'additional_image_link')!==false ){
                    $sCssClass="";
                    if(empty($styleHide)){ ?>

                        <div class="wpwoof-requiredfield-settings wpwoof-field-wrap stl-google" style="margin-bottom: 10px;margin-top: 20px;">
                        <p>
                            <label class="wpwoof-required-label"></label>
                            <span class="wpwoof-required-value">
                                <label>
                                    <input type="checkbox"  class="wpwoof_mapping" id="ID_ch_MoreImages" onclick="toggleExtra('additional_image_link');">
                                    Expand more images
                                </label>
                            </span>
                        </p>
                        <p class="description"><span></span><span><?php echo  __( 'You can include up to 10 additional images. If supplying multiple images, send them as comma separated URLs.', 'woocommerce_wpwoof' );?></span></p>
                        </div>
                        <?php

                    }
                    $styleHide=" additional_image_link";

                }else{
                    $styleHide="";
                }
                if (!empty($field['delimiter'])) {
                    echo '<hr class="wpwoof-break wpwoof-break-small' . $sCssClass . '"/>';
                }
                if (!isset($field['define'])) { ?>
                <div class="wpwoof-requiredfield-settings wpwoof-field-wrap<?php echo $sCssClass.$styleHide ?>">
                    <p>
                        <label class="wpwoof-required-label"><?php echo $fieldkey ?>:</label>
                    <span class="wpwoof-required-value">
                        <select name="field_mapping[<?php echo $fieldkey; ?>][value]"
                                class="wpwoof_mapping wpwoof_mapping_option">
                        <?php
                        $html = '';
                        if (isset($field['woocommerce_default'])) {
                            if (empty($wpwoof_values['field_mapping'][$fieldkey]['value'])) {
                                if (empty($wpwoof_values['field_mapping']) || !is_array($wpwoof_values['field_mapping'])) {
                                    $wpwoof_values['field_mapping'] = array();
                                }
                                if (empty($wpwoof_values['field_mapping'][$fieldkey]) || !is_array($wpwoof_values['field_mapping'][$fieldkey])) {
                                    $wpwoof_values['field_mapping'][$fieldkey] = array();
                                }
                                $wpwoof_values['field_mapping'][$fieldkey]['value'] = 'wpwoofdefa_' . $field['woocommerce_default']['value'];
                            }
                        } else {
                            $html .= '<option value="">select</option>';
                        }
                        $meta_keys_remove = $meta_keys;
                        foreach ($meta_keys_sort['sort'] as $sort_id => $meta_fields) {
                            $html .= '<optgroup label="' . $meta_keys_sort['name'][$sort_id] . '">';
                            foreach ($meta_fields as $key) {
                                $value = $meta_keys[$key];
                                unset($meta_keys_remove[$key]);
                                $html .= '<option '.( !empty($value['disabled']) ? ' disabled="disabled" ': '' ).' value="wpwoofdefa_' . $key . '" ' . selected('wpwoofdefa_' . $key, isset($wpwoof_values['field_mapping'][$fieldkey]['value']) ? $wpwoof_values['field_mapping'][$fieldkey]['value'] : "", false) . ' >' . $value['label'] . '</option>';
                            }
                            $html .= '</optgroup>';
                        }
                        $html .= '<optgroup label="Product Attributes">';
                        foreach ($attributes as $key => $value) {
                            $html .= '<option value="wpwoofattr_' . $key . '" ' . selected('wpwoofattr_' . $key, isset($wpwoof_values['field_mapping'][$fieldkey]['value']) ? $wpwoof_values['field_mapping'][$fieldkey]['value'] : "", false) . ' >' . $value . '</option>';
                        }
                        $html .= '</optgroup>';
                        echo $html;
                        ?>
                        </select>
                        <?php wpwoofeed_custom_attribute_input($fieldkey, $field, $wpwoof_values); ?>
                    </span>
                    </p>
                    <p class="description"><span></span><span><?php echo $field['desc']; ?></span></p>
                    <?php
                    if (!empty($field['callback']) && function_exists($field['callback'])) {
                        $field['callback']($fieldkey, $field, $wpwoof_values);
                    }
                    ?>
                    </div><?php

                }
            }?>




    </div>

    <hr class="wpwoof-break" />

    <div class="wpwoof-addfeed-button">
        <div class="wpwoof-addfeed-button-inner">
            <!-- <p><b>Important:</b> The Free version is limited to 50 products</p> -->
            <!-- <p><b>Upgrade Now:</b> <a href="http://www.pixelyoursite.com/product-catalog-facebook" target="_blank"><b class="wpwoof-clr-orange wpwoof-15p">Click here for a big discount</b></a></p> -->
            <p class="wpwoof-action-buttons">
                <input <?php if( !isset($_REQUEST['edit']) || empty($_REQUEST['edit']) ) echo 'style="width:100%;" '; ?>type="submit" name="wpwoof-addfeed-submit" class="wpwoof-button wpwoof-button-blue" value="<?php echo $wpwoof_add_button; ?>" />
                <?php  if( isset($_REQUEST['edit']) && !empty($_REQUEST['edit']) ) { ?>
                    <a href="<?php menu_page_url('wpwoof-settings'); ?>" class="wpwoof-button">Back</a>
                <?php } ?>
            </p>
        </div>
    </div>
        
    <?php if( isset($_REQUEST['edit']) && !empty($_REQUEST['edit']) ) { ?>
        <input type="hidden" name="edit_feed" value="<?php echo $_REQUEST['edit']; ?>">
    <?php } ?>
</div>

    
