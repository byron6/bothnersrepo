<?php
require_once "Encode.php";
$wpwoofeed_wpml_langs='';
function wpwoofeed_currency_code ($lang){
    if( empty($lang)) return "";
    if (!function_exists('icl_get_languages') ) return "";
    global $woocommerce_wpml;
    if ( empty($woocommerce_wpml->settings)
        || empty($woocommerce_wpml->settings['enable_multi_currency'])
    ) return "";
    foreach( $woocommerce_wpml->settings['currency_options'] as $currency => $val){
        if( !empty( $val['languages'][$lang] ) ){
            return $currency;
        }
    }
    return "";
}
function wpwoofeed_currency_rate ($code){
    if( empty($code)) return false;
    if (!function_exists('icl_get_languages') ) return false;
    global $woocommerce_wpml;
    if( empty($woocommerce_wpml->settings)
        || empty($woocommerce_wpml->settings['enable_multi_currency'])
    ) return false;
    global $woocommerce_wpml;
    foreach( $woocommerce_wpml->settings['currency_options'] as $currency => $val){
        if($currency == $code  ){
            if(empty($val['rate'])) return false;
            return $val['rate'];
        }
    }
    return false;
}
function wpwoofeed_query_vars_filter( $vars ){
    /* fix WPML BUG  in cron_schedule */
    global $wpwoofeed_wpml_langs;
    if(!empty($wpwoofeed_wpml_langs)) {
        $pos = stripos($vars, 'AND t.language_code');
        if ($pos !== false) {
            $epos = stripos($vars, ')', $pos);
            $vars = str_replace(substr($vars, $pos, $epos - $pos + 1), 'AND t.language_code IN ' . $wpwoofeed_wpml_langs, $vars);

        }
        $pos = stripos($vars, 't.language_code = ');
        if ($pos !== false) {
            $epos = stripos($vars, ' AND ', $pos);
            $vars = str_replace(substr($vars, $pos, $epos - $pos + 1), ' t.language_code IN ' . $wpwoofeed_wpml_langs, $vars);

        }

    }
    return $vars;
}
function wpwoofeed_generate_feed($data, $type = 'xml', $filepath, $info = array()){
    global $sitepress,$wpwoofeed_wpml_langs;
    global $woocommerce;
    global $woocommerce_wpwoof_common;
    global $store_info;
    global $wpwoofeed_settings;
    global $wpwoofeed_type;

    $feed_data = $data;
    try {


        if (!empty($data['edit_feed'])) {
            $feed_data['status_feed'] = 'starting';
            wpwoof_update_feed(serialize($feed_data), $feed_data['edit_feed'],true);
        }

        $feed_name = sanitize_text_field($data['feed_name']);
        $added_time = $data['added_time'];
        $data = array_merge(array(
            'feed_filter_sale' => 'all',
            'feed_filter_stock' => 'all',
            'feed_variable_price' => 'small',
        ), $data);
        $remove_variations = true;
    
        if(empty($data['feed_use_lang'])) $data['feed_use_lang']=ICL_LANGUAGE_CODE;
    


        $currencyCode="";
        $currencyRate=1;
        if (function_exists('icl_get_languages')) {
            if (empty($data['feed_use_lang'])) $data['feed_use_lang'] = ICL_LANGUAGE_CODE;
            $general_lang = ICL_LANGUAGE_CODE;
            $sitepress->switch_lang($data['feed_use_lang']);
            if($data['feed_use_lang']=='all'){
                $wpwoofeed_wpml_langs=' (';
                $zp="";
                $aLanguages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
                foreach($aLanguages as $lang){
                    $wpwoofeed_wpml_langs.=$zp."'".$lang['language_code']."'";
                    $zp=",";
                }
                $wpwoofeed_wpml_langs.=") ";

            }else{
                $wpwoofeed_wpml_langs=" ('".$data['feed_use_lang']."') ";
                $currencyCode = wpwoofeed_currency_code($data['feed_use_lang']);
                $currencyRate = wpwoofeed_currency_rate($currencyCode);
                if(!$currencyRate)  {
                    $currencyRate=1.0;
                    $currencyCode="";
                }

            }
        }

        $currencyCode = !$currencyCode ? get_woocommerce_currency() : $currencyCode;




    
        if($type == 'csv') {       
            $filep = fopen($filepath, "w");
            $info = array_merge(array('delimiter'=>'tab', 'enclosure' => 'double' ), $info);
            $delimiter = $info['delimiter'];
            if ($delimiter == 'tab') {
                $delimiter = "\t";
            }
            $enclosure = $info['enclosure'];
            if ($enclosure == "double")
                $enclosure = chr(34);
            else if ($enclosure == "single")
                $enclosure = chr(39);
            else
                $enclosure = '"';
            $batch = 0;
        } else {
            $filep = fopen($filepath, "w+");
        }


    
        $aExistsProds = array();
    
        $wpwoofeed_type = $type;
        $wpwoofeed_settings = $data;
    
        $field_rules = wpwoof_get_product_fields();
        $all_fields = $data['field_mapping'];
        $fields =array();
    
        /* Filter fields only for feed type */
        if(count($all_fields)) foreach ($all_fields as $fld => $val){
            if(@in_array($data['feed_type'],$field_rules[$fld]['feed_type'])  ){
                $fields[$fld]=$val;
            }
        }
        $categories = array();
    
       
    
        $store_info = new stdClass();
        $store_info->feed_type = $data['feed_type'] /*'facebook'*/;
        $store_info->site_url = home_url( '/' );
        $store_info->feed_url_base = home_url( '/' );
        $store_info->blog_name = get_option( 'blogname' );
        $store_info->charset = get_option( 'blog_charset' );
        $store_info->currency     =   (!empty($data['field_mapping']['remove_currency'])) ? '' : $currencyCode;
        $store_info->default_currency =  get_woocommerce_currency();
        $store_info->currencyRate = $currencyRate;
        $store_info->weight_units = get_option( 'woocommerce_weight_unit' );
        $store_info->base_country = $woocommerce->countries->get_base_country();
        $store_info = apply_filters( 'wpwoof_store_info', $store_info );
    
        $store_info->feed_url = $store_info->feed_url_base;
    
        if ( ! empty( $store_info->base_country ) && substr( 'US' == $store_info->base_country, 0, 2 ) ) {
            $US_feed = true;
            $store_info->US_feed = true;
        } else {
            $store_info->US_feed = false;
        }
    
        $columns = array();
        $values = array();
    
        global $wp_query, $wpdb, $post, $_wp_using_ext_object_cache;
    
        $wpwoof_feed = -1;
        $wpwoof_term = $categories;
        unset($categories);
    
        $tax_query = array();
    
    
       
        unset($wpwoof_feed, $wpwoof_term);
    
        
    
       
    
        $args['tax_query'] = $tax_query;
        $meta_query['relation'] = 'AND';
        $args['meta_query'] = $meta_query;
    
        unset($tax_query);
    
        $args['post_type'] = 'product';
        $args['post_status'] = 'publish';
        $args['fields'] = 'ids';
        $args['order'] = 'ASC';
        $args['orderby'] = 'ID';
        $args['posts_per_page'] = 110;
    
        $args['paged'] = 1;
        $wpwoof_limit = false;
    
        $output_count = 0;
        $data = '';
        $query_to_update = "UPDATE {$wpdb->postmeta} SET meta_value=%s WHERE meta_key=%s AND post_id IN ";
        if( $type == 'xml' ) {
            $header = '';
            $item = '';
            $footer = '';
    
            $header .= "<?xml version=\"1.0\" encoding=\"".$store_info->charset."\" ?>\n";
            $header .= "<rss xmlns:g=\"http://base.google.com/ns/1.0\" version=\"2.0\">\n";
            $header .= "  <channel>\n";
            $header .= "    <title><![CDATA[" . $store_info->blog_name . " Products]]></title>\n";
            $header .= "    <link><![CDATA[" . $store_info->site_url . "]]></link>\n";
            $header .= "    <description>WooCommerce Product List RSS feed</description>\n";
            fwrite($filep, $header);
            fflush($filep);
            unset($header);
        }
    
        $args['paged'] = 1;
        $args['meta_query'] = $meta_query;
        if (function_exists('icl_get_languages')) {
            add_filter('posts_where', 'wpwoofeed_query_vars_filter', 9999);
        }

        $products = new WP_Query( $args );


        $args['posts_per_page'] = 110;
        $pages = $products->max_num_pages;        
        $columns_name = false;
    
    
        /* before render fields need check tax value include tax or not to price*/
        $tax_rate = '';
        if(isset($fields['tax']['value'])){
    
    
            $prices_include_tax = get_option("woocommerce_prices_include_tax");
            $tax_based_on = get_option("woocommerce_tax_based_on");
    
    
            switch($fields['tax']['value']){
                case "false" : /* Exclude */
                    if($prices_include_tax=="yes"){
                        $tax_rate = "whithout";
                    }
                    break;
                case 'true':/* Include */
                    if($prices_include_tax=="yes"){
                        if($tax_based_on=='shipping' || $tax_based_on=='billing' ){
                            //то сначала отнимаем от цены величину налога BaseLocation, а затем прибавляем к базовой цене величину налога TargetCountry - и выводим в фид.
                            $tax_rate = "target:".$fields['tax_countries']['value'];
                        }else{ /* base */
                            //Ели установлено "Shop base address" - то ничего не добавляем, берем цену как есть и выводим в фид (потому что цена уже введена с учетом локального налоша).
    
                        }
                    }else{
                        if($tax_based_on=='shipping' || $tax_based_on=='billing' ){
                            //Если установлено "Customer shipping/billing address" - то прибавляем к цене величину налога TargetCountry - и выводим в фид.
                            $tax_rate = "addtarget:".$fields['tax_countries']['value'];
                        }else{
                            //Ели установлено "Shop base address" - то прибавляем к цене величину налога BaseLocation (вчера вроде видел в твоем коде $store_info->base_country) - и выводим в фид. Т.е. настройки вукоммерса не оверрайдим, если нужно выводить с налогом - то выводим с тем налогом, который настроен в вукоммерсе.
                            $tax_rate = "addbase";
                        }
                    }
                    break;
            }
    
    
    
        }
    
        for ( $i = 1; $i <= $pages; $i ++ ) {
            $products = $products->get_posts();
            foreach($products as $id_post => $post) {
    
    
    
                $product = wpwoofeed_load_product($post);
    
                //guard for multilanguage plugin
                if(in_array($product->id,$aExistsProds)) continue;
                $aExistsProds[] = $product->id;
    
                $item = '';
                if ($product->has_child() ){
                    $fields['item_group_id']['define']=true;
    
                }
                $data = wpwoofeed_item($fields, $product,$tax_rate);
                if( $type == 'xml' ) {
                    if($data) fwrite($filep, $data);
                } else {
                    if( ! $columns_name ) {
                        $columns  = $data[0];
                        $header = array();
                        foreach ($columns as $column_name => $value) {
                            $header[] = $column_name;
                        }
                        fputcsv($filep, $header, $delimiter, $enclosure);
                        $columns_name = true;
                    }
                    $fields_item = $data[1];
                    if( count($fields_item) == count($columns) ) {
                        fputcsv($filep, $fields_item, $delimiter, $enclosure);
                    }
                }
                if ($product->has_child() ) {
                    $children = $product->get_children();
                    $my_counter=0;
                    foreach ($children as $child) {
                        $child = $product->get_child( $child );
                        //guard for multilanguage plugin
                        if(!$child) continue;
                        if(in_array($child->id,$aExistsProds)) continue;
                        $aExistsProds[] = $child->id;
                        $my_counter ++;
                        $data = wpwoofeed_item($fields, $child,$tax_rate );
                        if( $type == 'xml' ) {
                            if($data) fwrite($filep, $data);
                        } else {
                            $fields_item = $data[1];
                            fputcsv($filep, $fields_item, $delimiter, $enclosure);
                        }
                        $output_count++;
                    }
                } else {
                    $output_count++;
                }
    
    
            }
            $args['paged']++;
            $products = new WP_Query( $args );
        }
        if( $type == 'xml' ) {
            $footer = '';
            
            $footer .= "  </channel>\n";
            $footer .= "</rss>";
            fwrite($filep, $footer);
            unset($footer);
        }
        fclose($filep);
        if (function_exists('icl_get_languages')) {
            $sitepress->switch_lang($general_lang);
        }
        if(!empty($feed_data['edit_feed'])) {
            $feed_data['status_feed'] = 'finished';
            wpwoof_update_feed(serialize($feed_data), $feed_data['edit_feed'],true);
        }

    }catch(Exception $e){
        $feed_data['status_feed'] = 'error: '.$e->getMessage();
        if(!empty($feed_data['edit_feed'])) {
            wpwoof_update_feed(serialize($feed_data), $feed_data['edit_feed'],true);
        }
        update_option("wpwoofeed_errors",$feed_data['status_feed'] );


        return false;
    }
   
    return true;
}
function wpwoofeed_item( $fields, $product,$tax_rate=null ) {
    if( empty($fields) || empty($product) )
        return '';
    
    global $wpwoofeed_type;
    global $woocommerce;
    global $woocommerce_wpwoof_common;
    global $store_info;
    global $wpwoofeed_settings;

    $item = '';

    $field_rules = wpwoof_get_product_fields();

    if( 'xml' == $wpwoofeed_type ) {
        $item    = "    <item>" . "\n";
    } else if( 'csv' == $wpwoofeed_type ) {
        $columns = array();
        $values = array();
    }
    $req_identifier_exists = 0;
  
    foreach ($fields as $tag => $field) {
        if (  $tag == 'identifier_exists') continue;
        if ( $wpwoofeed_settings['feed_type'] == 'google' && ($tag == 'mpn' || $tag=='gtin')
            &&
            (isset($fields['identifier_exists']) &&  $fields['identifier_exists']['value']=='false')) {  continue;}

        if( ( $tag=='mpn' || $tag=='gtin' )  ) {
            $req_identifier_exists++;
        }        
        if( $tag == 'product_type'
            && !empty( $wpwoofeed_settings['feed_google_category'])
            && $wpwoofeed_settings['feed_google_category'] != '' ) {
            if( 'xml' == $wpwoofeed_type ) {
                if($tag=='mpn' || $tag=='gtin')  $req_identifier_exists++;
                $item .="        <g:google_product_category>".wpwoofeed_text($wpwoofeed_settings['feed_google_category'])."</g:google_product_category>" . "\n";
            } else if('csv' == $wpwoofeed_type) {
                $columns['google_product_category'] = 1;
                $values[] = htmlspecialchars($wpwoofeed_settings['feed_google_category'],ENT_QUOTES);
            }
        }
        /* New block for skiping empty fields  */
        $isEmpty=true;
        if (count($field)) { foreach($field as $fldata) { if(!empty($fldata)) { $isEmpty=false; break; } } }
        else if( !isset($field_rules[$tag]['needcheck']) || $field_rules[$tag]['needcheck'] != true ){
            continue;
        }
        if( $isEmpty && (!isset($field_rules[$tag]['needcheck']) || $field_rules[$tag]['needcheck'] != true) ){
            continue;
        }
        /* END New block for skiping empty fields  */
        $extra_param = 0;
        if( strpos($tag, 'custom_label_') !== false ) {
            $func = 'custom_label';
        } else if( strpos($tag, 'additional_image_link') !== false ) {
            $func = 'additional_image_link';
            $extra_param = str_replace('additional_image_link_', '', $tag);
        } else {
            $func = $tag;
        }
        $func = trim($func);
        $tagvalue = '';
        $field['rules'] = isset($field_rules[$tag]) ? $field_rules[$tag] : '';
        if( ! empty($field_rules[$tag]['additional_options']) && is_array($field_rules[$tag]['additional_options']) ) {
            foreach($field_rules[$tag]['additional_options'] as $adopt_key => $adopt_val) {
                $field[$adopt_key] = (isset( $field[$adopt_key] ) ? $field[$adopt_key] : $adopt_val);
            }
        }
        try{
            if(($tag=='price' || $tag=='sale_price') && !empty($tax_rate)) {
                $field['tax_rate']=$tax_rate;
            }
            $tagvalue = ( function_exists ( 'wpwoofeed_' . $func ) ) ? call_user_func('wpwoofeed_' . $func , $product, $item, $field, $tag) : wpwoofeed_custom_user_function($product, $item, $field, $tag);
        } catch (Exception $e) {
            echo $e->getMessage();
            $tagvalue = '';
        }
        if( strpos($tag, 'additional_image_link') !== false ) {
            $tag = 'additional_image_link';
        }
        if( 'xml' == $wpwoofeed_type ) {
            if( $tagvalue && !empty($tagvalue) ) {
                $item .="        <g:" . $tag .">".$tagvalue."</g:" . $tag .">" . "\n";
            }
        } else if('csv' == $wpwoofeed_type) {
            $columns[$tag] = 1;
            $values[] = $tagvalue;
        }
       
    }
    if( $wpwoofeed_settings['feed_type'] == 'google' && (!$req_identifier_exists || (isset($fields['identifier_exists']) && $fields['identifier_exists']['value']=='false')) ){
        if( 'xml' == $wpwoofeed_type ) {
            $item .="        <g:identifier_exists>false</g:identifier_exists>" . "\n";
        } else if('csv' == $wpwoofeed_type) {
            $columns['identifier_exists'] = 1;
            $values[] = 'false';
        }
    }
    if( 'xml' == $wpwoofeed_type ) {
        $item .= "    </item>" . "\n";
        return $item;
    } else if('csv' == $wpwoofeed_type) {
        return array($columns, $values);
    }
}
function wpwoofeed_meta( $product, $item, $field, $id ) {
    $attribute = str_replace('wpwoofmeta_', '', $field['value']);
    $tagvalue = get_post_meta( $product->id, $attribute, true);
    $tagvalue = is_array($tagvalue) ? '' : $tagvalue;    
    $tagvalue = do_shortcode( $tagvalue );
    
    $tagvalue = wpwoofeed_text($tagvalue, false);
    return $tagvalue;
}
function wpwoofeed_attr( $product, $item, $field, $id ) {
    if( isset( $field['define'] ) && isset($field['define']['option']) )
        $taxonomy = $field['define']['option'];
    else
        $taxonomy = $field['value'];

    $taxonomy = str_replace('wpwoofattr_', '', $taxonomy);
    $taxonomy = str_replace('wpwoofdefa_', '', $taxonomy);

    
    
    $the_terms = wp_get_post_terms( $product->id, $taxonomy, array( 'fields' => 'names' ));

    $tagvalue = '';
    if( !is_wp_error($the_terms) && !empty($the_terms) ) {
            foreach ($the_terms as $term) {
                $tagvalue .= $term.', ';
            }
            $tagvalue = rtrim($tagvalue, ', ');
    }
    
    $tagvalue = do_shortcode( $tagvalue );

    $tagvalue = wpwoofeed_text($tagvalue, false);
    return $tagvalue;
}
function wpwoofeed_xml_has_error($message) {
    global $xml_has_some_error;
    if( ! $xml_has_some_error &&  !empty($$message) ) {
        add_action( 'admin_notices', create_function( '', 'echo "'.$message.'";' ), 9999 );
        $xml_has_some_error = true;
    }
}
function wpwoofeed_custom_user_function( $product, $item, $field, $id, $data = array() ) {
    $value = $field['value'];
    
    $tagvalue = '';
    if( empty($value) ) {
        return '';
    } 
    $data = array_merge(array(
        'wpwoofdefa' => true,
        'wpwoofmeta' => true,
        'wpwoofattr' => true,
    ), $data);
    if( strpos($value, 'wpwoofdefa_') !== false && $data['wpwoofdefa'] ){
        $return = wpwoofeed_tagvalue($value, $product, $id, $field);
        return $return;
    } else if( strpos($value, 'wpwoofmeta_') !== false && $data['wpwoofmeta'] ){
        $return = wpwoofeed_meta($product, $item, $field, $id);
        return $return;
    } else if( strpos($value, 'wpwoofattr_') !== false && $data['wpwoofattr'] ){
        $return = wpwoofeed_attr($product, $item, $field, $id);
        return $return;
    }

    return false;
}
function wpwoofeed_id($product, $item, $field, $id){
    return wpwoofeed_custom_user_function($product, $item, $field, $id);
}
/* For Google Feed */
function wpwoofeed_is_bundle($product, $item, $field, $id){
    return ( $product->is_type( 'bundle' ) /*$product->is_type( 'grouped' )*/ ) ? "TRUE" : "";
   // return wpwoofeed_custom_user_function($product, $item, $field, $id);
}
/* For Google Feed */
function wpwoofeed_adult($product, $item, $field, $id){
    return ($field['value']=="true") ? "TRUE" : "";
}
function wpwoofeed_description($product, $item, $field, $id){
    if( isset( $field['use_child'] ) && ! $field['use_child'] ) {
        $product_parent = wpwoofeed_load_product($product->id);
    } else {
        $product_parent = $product;
    }
    $desc = wpwoofeed_custom_user_function($product_parent, $item, $field, $id);
    if( empty($desc) ) {
        wpwoofeed_xml_has_error('Description missing in some products');
    }
    return $desc;
    
}
function wpwoofeed_description_short($product, $item, $field, $id){
    $desc = wpwoofeed_custom_user_function($product, $item, $field, $id);   
    return $desc;
}
function wpwoofeed_variation_description($product, $item, $field, $id){
    $desc = wpwoofeed_custom_user_function($product, $item, $field, $id);   
    return $desc;
}
function wpwoofeed_image_link($product, $item, $field, $id){
    $link = wpwoofeed_custom_user_function($product, $item, $field, $id);
    if( empty($link) ) {
        wpwoofeed_xml_has_error('Image link missing in some products');
    }
    return $link;
}
function wpwoofeed_title($product, $item, $field, $id){
    $title = wpwoofeed_custom_user_function($product, $item, $field, $id);
    if(!empty($title)) {
        if (!empty($field['uc_every_first'])) {
            return wpwoofeed_text(wpwoof_sentence_case($title));
        }
        return wpwoofeed_text($title);
    }    
    return '';
}
function wpwoofeed_brand($product, $item, $field, $id){
    return  wpwoofeed_text(wpwoofeed_custom_user_function($product, $item, $field, $id),false);
}
function wpwoofeed_product_prices( &$feed_item, $woocommerce_product ) {

    // Grab the price of the main product.
    $prices = wpwoofeed_generate_prices_for_product( $woocommerce_product );
    // Set the selected prices into the feed item.
    $feed_item->regular_price    = $prices->regular_price;
    $feed_item->sale_price       = $prices->sale_price;

}
function wpwoofeed_price($product, $item, $field, $id){
    $price = $product->get_regular_price();
    $prices = new stdClass();
    $prices->sale_price    = null;
    $prices->regular_price  = $price;
    $prices = wpwoofeed_adjust_prices_for_children( $prices, $product );
    $price = $prices->regular_price;
    if(!isset($field['tax_rate']) || empty($field['tax_rate']))  return price_format($price);
    $aBaseTax = WC_Tax::get_base_tax_rates();
    $BaseTax = 0.0;
    if(count($aBaseTax)) foreach($aBaseTax as $tax) {
        if(isset($tax['rate'])){
            $BaseTax = $tax['rate']*1.0;
        }
    }
    if($field['tax_rate']=='addbase')  {
        return price_format($price + $price*$BaseTax/100);
    }
    if($field['tax_rate']=='whithout') {
        return  price_format($price*1.0/(1.0 + $BaseTax*1.0/100.0));
    }
    if(strpos($field['tax_rate'],'addtarget' )!==false){
        $sep = explode(":",$field['tax_rate']);
        if(isset($sep[1]) && !empty($sep[1]) ){
            $atax = WC_Tax::find_rates(array("country"=>$sep[1]));
            if(count($atax)) foreach($atax as $tax) {
                if(isset($tax['rate'])){
                    return price_format($price + $price * $tax['rate']*1.0 / 100.0);
                }
            }

        }
        return  price_format($price);
    }
    if(strpos($field['tax_rate'],'target' )!==false){
        $price = $price*1.0/(1.0 + $BaseTax*1.0/100);
        $sep = explode(":",$field['tax_rate']);
        if(isset($sep[1]) && !empty($sep[1]) ){
            $atax = WC_Tax::find_rates(array("country"=>$sep[1]));
            if(count($atax)) foreach($atax as $tax) {
                if(isset($tax['rate'])){
                    return price_format($price + $price * $tax['rate']*1.0 / 100.0);
                }
            }
        }
        return  price_format($price);
    }
    return  price_format($price);
}
//need rebuild
function wpwoofeed_sale_price($product, $item, $field, $id){


    $prices = new stdClass();


    $prices->sale_price    = $product->get_sale_price();
    $prices->regular_price  = $product->get_regular_price();

    $prices = wpwoofeed_adjust_prices_for_children( $prices, $product );

    if($prices->sale_price<0.001 || $prices->sale_price==$prices->regular_price ) return '';

    if(!isset($field['tax_rate']) || empty($field['tax_rate']))  return price_format($prices->sale_price);


    $aBaseTax = WC_Tax::get_base_tax_rates();
    $BaseTax = 0.0;
    if(count($aBaseTax)) foreach($aBaseTax as $tax) {
        if(isset($tax['rate'])){
            $BaseTax = $tax['rate']*1.0;
        }
    }
    $price = $prices->sale_price;



    if($field['tax_rate']=='addbase')  {
        return price_format($price + $price*$BaseTax/100);
    }
    if($field['tax_rate']=='whithout') {
        return  price_format($price*1.0/(1.0 + $BaseTax*1.0/100));
    }
    if(strpos($field['tax_rate'],'addtarget' )!==false){
        $sep = explode(":",$field['tax_rate']);
        if(isset($sep[1]) && !empty($sep[1]) ){
            $atax = WC_Tax::find_rates(array("country"=>$sep[1]));
            if(count($atax)) foreach($atax as $tax) {
                if(isset($tax['rate'])){
                    return price_format($price + $price * $tax['rate']*1.0 / 100.0);
                }
            }

        }
        return  price_format($price);
    }
    if(strpos($field['tax_rate'],'target' )!==false){
        $price = $price*1.0/(1.0 + $BaseTax*1.0/100);
        $sep = explode(":",$field['tax_rate']);
        if(isset($sep[1]) && !empty($sep[1]) ){
            $atax = WC_Tax::find_rates(array("country"=>$sep[1]));
            if(count($atax)) foreach($atax as $tax) {
                if(isset($tax['rate'])){
                    return price_format($price + $price * $tax['rate']*1.0 / 100.0);
                }
            }

        }
        return  price_format($price);
    }

    return  price_format($prices->sale_price);
    //return wpwoofeed_custom_user_function($product, $item, $field, $id);
}
function price_format($price){
    global $store_info;
    return number_format((float)$price * $store_info->currencyRate, 2, ".", ''). ' ' . $store_info->currency;
}

function wpwoofeed_gtin($product, $item, $field, $id){
    $gtin =  wpwoofeed_text(wpwoofeed_custom_user_function($product, $item, $field, $id),false);
    return $gtin;
}

function wpwoofeed_tax($product, $item, $field, $id){
    if(empty($field['value']) || $field['value']!="true") return "";
    return "";
}
function wpwoofeed_tax_countries($product, $item, $field, $id){
    return "";
}
function wpwoofeed_condition($product, $item, $field, $id){
	$value = $field['define'];
    $tagvalue = wpwoofeed_custom_user_function($product, $item, $field, $id);

    if( empty($tagvalue) ) {
        $tagvalue = 'new';
    } else {
        $tagvalue = str_replace(',', ' , ', $tagvalue);
        $tagvalue = ' '.$tagvalue.' ';
        $tagvalue = strtolower($tagvalue);
        if( strpos($tagvalue, ' new ') !== false ) {
            $tagvalue = 'new';
        } elseif( strpos($tagvalue, ' used ') !== false ) {
            $tagvalue = 'used';
        } elseif( strpos($tagvalue, ' refurbished ') !== false ) {
            $tagvalue = 'refurbished';
        } else {
            $tagvalue = 'new';
        }
    }
    if( empty($tagvalue) ) {
        wpwoofeed_xml_has_error('Condition missing in some products');
    }
    return $tagvalue;
}
function wpwoofeed_load_product( $post ) {
    if ( function_exists( 'wc_get_product' ) ) {
        // 2.2 compat.
        return wc_get_product( $post );
    } else if ( function_exists( 'get_product' ) ) {
        // 2.0 compat.
        return get_product( $post );
    } else {
        return new WC_Product( $post->ID );
    }
}
function wpwoofeed_enforce_length($text, $length, $full_words = false){
    if ( !$length  || strlen( $text ) <= $length ) {
        return $text;
    }

    if ( $full_words === true ) {
        $text = substr( $text, 0, $length );
        $pos = strrpos($text, ' ');
        $text = substr( $text, 0, $pos );
    } else {
        $text = substr( $text, 0, $length );
    }

    return $text;
}

function wpwoofeed_encode_callback($m)
{
    return utf8_encode($m[0]);
}

function wpwoofeed_text($text, $use_cdata = true){
    global $wpwoofeed_type;
    if( ! empty($text) ) {
        $text = (strpos($text,"<![CDATA[")!==false) ? str_replace("<![CDATA[","",str_replace(']]>','',$text)) : $text;
        $text =  WPWOOF_Encoding::toUTF8(html_entity_decode($text));
        $text = strip_tags($text);
        $text = do_shortcode( $text );
        if('xml' == $wpwoofeed_type) {
            $text = wp_kses_decode_entities($text);
            if( $use_cdata ) {
                $text = "<![CDATA[" . $text . "]]>";
            }else{
                $text = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', htmlspecialchars($text,ENT_QUOTES));
            }
        }
    }

    return preg_replace('/[\x{0a}-\x{1f}]/u', '*', $text);
}

function wpwoofeed_check_url($src){
    $src=trim($src);
    if(empty($src)) return '';   
    return (!preg_match("~^(?:f|ht)tps?://~i", $src)) ?  home_url($src) : $src; 
}

function wpfeed_thumbnail_src( $post_id = null, $size = 'post-thumbnail' ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post_id );
    if ( ! $post_thumbnail_id ) {
        return false;
    }
    list( $src ) = wp_get_attachment_image_src( $post_thumbnail_id, $size, false );

    return wpwoofeed_check_url($src);
}
function wpwoofeed_generate_prices_for_product( $woocommerce_product ) {
    global $store_info, $woocommerce_wpml;
    $prices = new stdClass();

    $prices->sale_price    = null;
    $prices->regular_price  = null;


        if ( /** WPML MULTYCURRENCY BLOCK */
            $store_info->currency != $store_info->default_currency
            && function_exists('icl_get_languages') && !empty($woocommerce_wpml->settings)
            && !empty($woocommerce_wpml->settings['enable_multi_currency'])

        ) {
            $rate =  ( $store_info->currencyRate>0 ) ?  $store_info->currencyRate*1.0 : 1.0;
            $id = isset($woocommerce_product->variation_id) ?  $woocommerce_product->variation_id : $woocommerce_product->id;
            if ( get_post_meta($id,'_wcml_custom_prices_status',true)=='1' ){
                $prices->regular_price = get_post_meta($id,'_regular_price_'.$store_info->currency,true);

                if( get_post_meta($id,'_wcml_schedule__'.$store_info->currency,true)=='1'){
                    $dt_from = get_post_meta($id,'_sale_price_dates_from_'.$store_info->currency,true);
                    $dt_to = get_post_meta($id,'_sale_price_to_from_'.$store_info->currency,true);
                    if(    ( empty($dt_from) || $dt_from<=time() )
                        && ( empty($dt_to) ||   $dt_from>=time() )
                    ){
                        $prices->sale_price = get_post_meta($id,'_sale_price_'.$store_info->currency,true);
                    }else{
                        $prices->sale_price = null;
                    }
                }else{
                    $prices->sale_price  = get_post_meta($id,'_sale_price_'.$store_info->currency,true);
                }
                if( $prices->sale_price>0 )    $prices->sale_price = $prices->sale_price / $rate;
                if( $prices->regular_price>0 ) {
                    $prices->regular_price = $prices->regular_price / $rate;
                    return $prices;
                }
            }
        } /*  if (  WPML MULTYCURRENCY BLOCK */





    // Grab the regular price of the base product.
    $regular_price  = $woocommerce_product->get_regular_price();
    if ( '' != $regular_price ) {
        $prices->regular_price = $regular_price;
    }

    // Grab the sale price of the base product.
    $sale_price                    = $woocommerce_product->get_sale_price();
    if ( $sale_price != '' ) {
        $prices->sale_price    = $sale_price ;
    }
    if(  empty($prices->regular_price) && $woocommerce_product->is_type('subscription_variation')){
        $rerular = get_post_meta( $woocommerce_product->variation_id,'_unit_price_regular',true);
        if(!empty($rerular) && is_numeric($rerular)){
            $prices->sale_price     = get_post_meta( $woocommerce_product->variation_id,'_unit_price_sale',true);
            $prices->regular_price  = $rerular;
        }else {
            $rerular = get_post_meta( $woocommerce_product->variation_id,'_subscription_price',true);
            if(!empty($rerular) && is_numeric($rerular)){
                $prices->regular_price  = $rerular;
            }else {
                $rerular = get_post_meta( $woocommerce_product->variation_id,'_subscription_sign_up_fee',true);
                if(!empty($rerular) && is_numeric($rerular)){
                    $prices->regular_price  = $rerular;
                }
            }
        } /* else  _unit_price_regular if(!empty($rerular) && is_numeric($rerular)){ */
    }
    return $prices;
}
function wpwoofeed_adjust_prices_for_children( $prices, $woocommerce_product ) {
    global $wpwoofeed_settings;

    if ( ! $woocommerce_product->has_child() ) {
        if($woocommerce_product->is_type( 'bundle' )) {
            $prices->sale_price    = $woocommerce_product->get_bundle_price();
            $prices->regular_price = $woocommerce_product->get_bundle_regular_price();
        }
        return $prices;
    }

    $first_child = true;
    $aImages = array();
    $children = $woocommerce_product->get_children();
    foreach ( $children as $child ) {
        $child_product = $woocommerce_product->get_child( $child );
        if ( ! $child_product ) {
            continue;
        }
        if ( in_array( $child_product->product_type, array('variation','subscription_variation') )) {
            $child_is_visible = wpwoofeed_variation_is_visible( $child_product );
        } else {
            $child_is_visible = $child_product->is_visible();
        }
        if ( ! $child_is_visible ) {
            continue;
        }
        

        $image = wpwoofeed_text(wpfeed_thumbnail_src($child_product->id, 'shop_single'), false);
        if(!empty($image )) $aImages[]=$image;



        $child_prices = wpwoofeed_generate_prices_for_product( $child_product );
        if( $first_child ){
            $first_child_prices = $child_prices;
            $first_child = false;
        }
        if ( ( 0 == $prices->regular_price ) && ( $child_prices->regular_price > 0 ) ) {
            $prices = $child_prices;
        } else {
            if( $wpwoofeed_settings['feed_variable_price'] == 'big' ) {
                if ( ($child_prices->regular_price > 0) && ($child_prices->regular_price > $prices->regular_price) )
                    $prices = $child_prices;
            } else if( $wpwoofeed_settings['feed_variable_price'] == 'first' ) {
                if ( ($child_prices->regular_price > 0) )
                    $prices = $first_child_prices;
            } else {
                if ( ($child_prices->regular_price > 0) && ($child_prices->regular_price < $prices->regular_price) )
                    $prices = $child_prices;
            }
        }
    }
    return $prices;
}
function wpwoofeed_variation_is_visible($variation) {
    if ( method_exists( $variation, 'variation_is_visible' ) ) {
        return $variation->variation_is_visible();
    }
    $visible = true;
    // Published == enabled checkbox
    if ( 'publish' != get_post_status( $variation->variation_id ) ) {
        $visible = false;
    }
    // Out of stock visibility
    elseif ( 'yes' == get_option( 'woocommerce_hide_out_of_stock_items' ) && ! $variation->is_in_stock() ) {
        $visible = false;
    }
    // Price not set
    elseif ( $variation->get_price() === '' ) {
        $visible = false;
    }
    return $visible;
}
function wpwoofeed_tagvalue($tag, $product, $tagid, $tag_option = array()){
    global $wpwoofeed_settings;
    global $store_info;
    global $woocommerce_wpwoof_common;
    $field_rules = wpwoof_get_product_fields();   
    $tag = str_replace('wpwoofdefa_', '', $tag);

    if( strpos($tagid, 'additional_image_link') !== false ) {
        $image_position = str_replace('additional_image_link_', '', $tagid);
        $image_position = (int) $image_position - 1;
    } else if( strpos($tag, 'additional_image_link') !== false ) {
        $image_position = str_replace('additional_image_link_', '', $tag);
        $image_position = (int) $image_position - 1;
    }
    if( strpos($tag, 'additional_image_link') !== false ){
        $tag = 'additional_image_link';
    }

    $length = false;
    if( isset($field_rules[$tag][$store_info->feed_type.'_len']) && $field_rules[$tag][$store_info->feed_type.'_len'] != false ){
        $length = $field_rules[$tag][$store_info->feed_type.'_len'];
    } 

    switch ($tag) {
        case 'id':
            $return = $product->id;
            if( in_array( $product->product_type, array('variation','subscription_variation') ) ) {
                $return = $product->variation_id;
            }
            $return = wpwoofeed_enforce_length( $return, $length );
            return $return;
            break;
        case 'availability':
            if( $product->is_in_stock() ) {
                $stock = 'in stock';
            } else {
                $stock = 'out of stock';
            }
            return $stock;
            break;
        case 'description':
            $description = '';
            if(  in_array( $product->product_type, array('variation','subscription_variation') )  && isset($product->variation_id)){
                $product_id = $product->variation_id;
                $description = get_post_meta($product_id, '_variation_description', true);
            }
            if( empty($description) ) {
                $description = $product->post->post_content;
            }
            $description = strip_tags($description);
            $description = wpwoofeed_enforce_length($description, $length, true);
            $description = wpwoofeed_text($description);
            return $description;
            break;
        case 'description_short':
            $short_description = $product->post->post_excerpt;
            $short_description = strip_tags($short_description);
            $short_description = wpwoofeed_enforce_length($short_description, $length, true);
            $short_description = wpwoofeed_text($short_description);

            return $short_description;
            break;
        case 'variation_description':
            $variation_description = '';
            if(  in_array( $product->product_type, array('variation','subscription_variation') ) ){
                    $product_id = $product->variation_id;
                    $variation_description = get_post_meta($product_id, '_variation_description', true);
            }
            $variation_description = strip_tags($variation_description);
            $variation_description = wpwoofeed_enforce_length( $variation_description, $length, true );
            $variation_description = wpwoofeed_text($variation_description);

            return $variation_description;
            break;
        case 'use_custom_attribute':
            $attribute_value = '';
            if( isset( $wpwoofeed_settings['field_mapping'][$tagid]['custom_attribute'] ) ) {
                $custom_attribute = $wpwoofeed_settings['field_mapping'][$tagid]['custom_attribute'];
                $taxonomy = strtolower($custom_attribute);
                if( !empty($taxonomy) &&  in_array( $product->product_type, array('variation','subscription_variation') ) ) {
                    $attributes = $product->get_variation_attributes();
                    foreach ($attributes as $attribute => $attribute_value) {
                        $attribute = strtolower($attribute);
                        if( strpos($attribute, $taxonomy) !== false ) {
                            $attribute_value = wpwoofeed_text($attribute_value, false);
                            return $attribute_value;
                        }
                    }
                }
            }
            return $attribute_value;
            break;
        case 'image_link':
            $link = wpfeed_thumbnail_src($product->id);
            $link = wpwoofeed_text($link);
            return $link;
            break;
        case 'product_image':
            $image = wpfeed_thumbnail_src($product->id, 'shop_single');
            $image = wpwoofeed_text($image);
            return $image;
            break;
        case 'yoast_seo_product_image':
            $data = get_post_custom($product->id);

            if(isset($data['_yoast_wpseo_opengraph-image']) && $data['_yoast_wpseo_opengraph-image'][0]){
                return $data['_yoast_wpseo_opengraph-image'][0];
            }
            return '';
            break;
        case 'mashshare_product_image':
            $data = get_post_custom($product->id);
            if(isset($data['mashsb_og_image']) && $data['mashsb_og_image'][0]){
                $src = wp_get_attachment_image_src( $data['mashsb_og_image'][0], 'post-thumbnail', false );
                return   wpwoofeed_check_url( ($src && !empty($src[0])) ? $src[0] : '') ;
            }else  if(isset($data['mashsb_pinterest_image']) && $data['mashsb_pinterest_image'][0]){
                $src =  wp_get_attachment_image_src( $data['mashsb_pinterest_image'][0], 'post-thumbnail', false );
                return   wpwoofeed_check_url( ($src && !empty($src[0])) ? $src[0] : '' ) ;
            }
            return '';
            break;
        case 'link':
            $url = get_permalink( $product->id );
            if(  in_array( $product->product_type, array('variation','subscription_variation') )  && isset($product->variation_id) ) {
                $wc_product = new WC_Product_Variation($product->variation_id);
                $url = $wc_product->get_permalink();
                unset($wc_product);
            }
            $url = wpwoofeed_text($url);
            return $url;
            break;
        case 'title':
            $title = $product->post->post_title;
            $title = wpwoofeed_enforce_length( $title, $length, true );
            return $title;
            break;
        case 'price':
            wpwoofeed_price($product, null, null, null);
            return $price;
            break;
        case 'site_name':            
            return wpwoofeed_enforce_length( $store_info->blog_name, $length, true );
            break;   
        case 'mpn':
            $return = $product->get_sku();
            $return = wpwoofeed_text($return,false);
            return $return;
            break;
        case 'condition':
            break;
        case 'brand':
            break;
        case 'additional_image_link':
            $tagvalue = '';
            $imgIds = $product->get_gallery_attachment_ids();
            $images = array();
            if (count($imgIds)) {
                foreach ($imgIds as $key => $value) {
                    if ($key < 9) {
                        $images[$key] = wpwoofeed_check_url(wp_get_attachment_url($value));
                    }
                }
            }
            if ($images && is_array($images)) {
                if( isset( $images[$image_position] ) )
                    $tagvalue .= $images[$image_position] . ',';
            }
            $tagvalue = rtrim($tagvalue, ',');
            $tagvalue = wpwoofeed_text($tagvalue, false);
            return $tagvalue;
            break;
        case 'item_group_id':
            if(  in_array( $product->product_type, array('variation','subscription_variation') )   || $product->has_child() && !empty($tag_option['define'])  ) {
                return $product->id;
            }else {
                return '';
            }
            break;
        case 'product_type':
            $categories = wp_get_object_terms($product->id, 'product_cat');
            $categories_string = array();
            if( ! is_wp_error($categories) ) {
                foreach($categories as $cat) {
                    $categories_string[] = $cat->name;
                }
            }
            $categories_string = implode(', ', $categories_string);
            $categories_string = wpwoofeed_enforce_length( $categories_string, $length, true );
            $categories_string = wpwoofeed_text($categories_string);
            return $categories_string;
            break;
        case 'product_type_normal':
            $product_type = $product->get_type();
            $product_type = wpwoofeed_text($product_type);
            return $product_type;
            break;
        case 'sale_price':
            $feed_item = new stdClass();
            wpwoofeed_product_prices($feed_item, $product);
            // If there's no sale price, then we're done.
            if ( empty( $feed_item->sale_price_inc_tax ) )
                return false;
            // Otherwise, include the sale_price tag.
            if ( $store_info->US_feed ) {
                // US prices have to be submitted excluding tax.
                $sale_price = number_format( (float)$feed_item->sale_price_ex_tax, 2, '.', '' );
            } else {
                $sale_price = number_format( (float)$feed_item->sale_price_inc_tax, 2, '.', '' );
            }
            if ( empty( $sale_price) )
                return false;
            $sale_price = $sale_price . ' ' . $store_info->currency;
            $sale_price = wpwoofeed_text($sale_price, false);
            return $sale_price;
            break;
        case 'sale_price_effective_date':
            if( isset($product->variation_id) )
                $product_id = $product->variation_id;
            else
                $product_id = $product->id;
            $from = get_post_meta($product_id, '_sale_price_dates_from', true);
            $to = get_post_meta($product_id, '_sale_price_dates_to', true);

            if (!empty($from) && !empty($to)) {
                $from = date_i18n('Y-m-d\TH:iO', $from);
                $to = date_i18n('Y-m-d\TH:iO', $to);
                $date = "$from" . "/" . "$to";
            } else {
                $date = "";
            }
            $tagvalue = $date;
            
            $tagvalue = wpwoofeed_text($tagvalue, false);
            return $tagvalue;
            break;
        case 'shipping':
            $shipping = $product->get_shipping_class();
            $shipping = wpwoofeed_text($shipping, false);
            return $shipping;
            break;
        case 'shipping_weight':
            $tagvalue = $product->get_weight();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_weight_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            break;
      
        case 'shipping_length':
            $tagvalue = $product->get_length();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            }
            return '';
            break;
        case 'shipping_width':
            $tagvalue = $product->get_width();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            return $tagvalue;
            break;
        case 'shipping_height':
            $tagvalue = $product->get_height();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            return $tagvalue;
            break;
        case 'shipping_weight':
            $tagvalue = $product->get_weight();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_weight_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            break;
        case 'size':
            $tagvalue = $product->get_dimensions();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_size_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            }
            return '';
            break;
        case 'length':
            $tagvalue = $product->get_length();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            }else {
                return '';
            }
            break;
        case 'width':
            $tagvalue = $product->get_width();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            return $tagvalue;
            break;
        case 'height':
            $tagvalue = $product->get_height();
            if( !empty($tagvalue) ) {
                $unit = get_option( 'woocommerce_dimension_unit' );
                $tagvalue = $tagvalue . ' ' . esc_attr($unit);
                $tagvalue = wpwoofeed_text($tagvalue, false);
                return $tagvalue;
            } else {
                return '';
            }
            return $tagvalue;
            break;
        
        case 'tags':
            $tags = wp_get_object_terms($product->id, 'product_tag');
            $tags_string = array();
            if( ! is_wp_error($tags) ) {
                foreach($tags as $tag) {
                    $tags_string[] = $tag->name;
                }
            }
            $tags_string = implode(', ', $tags_string);
            $tags_string = wpwoofeed_enforce_length( $tags_string, $length, true );
            $tags_string = wpwoofeed_text($tags_string, false);
            return $tags_string;
            break;
        case 'custom_label_':
            break;
        case 'stock_quantity':
            $stock_quantity = $product->get_stock_quantity();
            if( empty($stock_quantity) ) {
                $stock_quantity = 0;
            }
            return $stock_quantity;
            break;
        case 'average_rating':
            $average_rating = $product->get_average_rating();
            return $average_rating;
            break;
        case 'total_rating':
            $total_rating = $product->get_rating_count();
            return $total_rating;
            break;
        case 'sale_start_date':
            if( isset($product->variation_id) )
                $product_id = $product->variation_id;
            else
                $product_id = $product->id;
            $from = get_post_meta($product_id, '_sale_price_dates_from', true);

            if (!empty($from)) {
                $tagvalue = date_i18n('Y-m-d\TH:iO', $from);
            } else {
                $tagvalue = "";
            }
            $tagvalue = wpwoofeed_text($tagvalue, false);
            return $tagvalue;
            break;
        case 'sale_end_date':
            if( isset($product->variation_id) )
                $product_id = $product->variation_id;
            else
                $product_id = $product->id;
            $to = get_post_meta($product_id, '_sale_price_dates_to', true);

            if (!empty($to)) {
                $tagvalue = date_i18n('Y-m-d\TH:iO', $to);
            } else {
                $tagvalue = "";
            }
            $tagvalue = wpwoofeed_text($tagvalue, false);
            return $tagvalue;
            break;
        default:
            return '';
            break;
    }
    return '';
}
function wpwoofeed_product_is_excluded($woocommerce_product){
    $excluded = false;
    // Check to see if the product is set as Hidden within WooCommerce.
    if ( 'hidden' == $woocommerce_product->visibility ) {
        $excluded = true;
    }
    // Check to see if the product has been excluded in the feed config.
    if ( $tmp_product_data = wpwoofeed_get_product_meta( $woocommerce_product, 'woocommerce_wpwoof_data' ) ) {
        $tmp_product_data = maybe_unserialize( $tmp_product_data );
    } else {
        $tmp_product_data = array();
    }
    if ( isset ( $tmp_product_data['exclude_product'] ) ) {
        $excluded = true;
    }

    return apply_filters( 'wpwoof_exclude_product', $excluded, $woocommerce_product->id, 'facebook');
}
function wpwoofeed_get_product_meta( $product, $field_name ) {
    if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0' ) >= 0 ) {
        // even in WC >= 2.0 product variations still use the product_custom_fields array apparently
        if ( $product->variation_id && isset( $product->product_custom_fields[ '_' . $field_name ][0] ) && $product->product_custom_fields[ '_' . $field_name ][0] !== '' ) {
            return $product->product_custom_fields[ '_' . $field_name ][0];
        }
        // use magic __get
        return $product->$field_name;
    } else {
        // variation support: return the value if it's defined at the variation level
        if ( isset( $product->variation_id ) && $product->variation_id ) {
            if ( ( $value = get_post_meta( $product->variation_id, '_' . $field_name, true ) ) !== '' ) {
                return $value;
            }
            // otherwise return the value from the parent
            return get_post_meta( $product->id, '_' . $field_name, true );
        }
        // regular product
        return isset( $product->product_custom_fields[ '_' . $field_name ][0] ) ? $product->product_custom_fields[ '_' . $field_name ][0] : null;
    }
}
function wpwoofeed_send_via_ftp($data, $file, $file_name) {   
    return false;
}