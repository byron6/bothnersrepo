<?php

if ( ! defined( 'ABSPATH' ) || ! defined( 'YITH_YWPAR_VERSION' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Implements admin features of YITH WooCommerce Points and Rewards
 *
 * @class   YITH_WC_Points_Rewards_Admin
 * @package YITH WooCommerce Points and Rewards
 * @since   1.0.0
 * @author  YITH
 */
if ( ! class_exists( 'YITH_WC_Points_Rewards_Admin' ) ) {

	/**
	 * Class YITH_WC_Points_Rewards_Admin
	 */
	class YITH_WC_Points_Rewards_Admin {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WC_Points_Rewards_Admin
		 */
		protected static $instance;

		/**
		 * @var $_panel Panel Object
		 */
		public $_panel;

		/**
		 * @var $_premium string Premium tab template file name
		 */
		protected $_premium = 'premium.php';

		/**
		 * @var string Premium version landing link
		 */
		protected $_premium_landing = 'https://yithemes.com/themes/plugins/yith-woocommerce-points-and-rewards/';

		/**
		 * @var string Panel page
		 */
		protected $_panel_page = 'yith_woocommerce_points_and_rewards';

		/**
		 * @var string Doc Url
		 */
		public $doc_url = 'https://docs.yithemes.com/yith-woocommerce-points-and-rewards/';

		/**
		 * @var string name of plugin options
		 */
		public $plugin_options = 'yit_ywpar_options';

		/**
		 * @var Wp List Table
		 */
		public $cpt_obj;


		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WC_Points_Rewards_Admin
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina
		 */
		public function __construct() {

			$this->create_menu_items();

			// Register plugin to licence/update system.
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
			add_action( 'admin_init', array( $this, 'actions_from_settings_panel' ), 9 );
			// Add action links.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_YWPAR_DIR . '/' . basename( YITH_YWPAR_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 5 );

			// Custom styles and javascripts.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles_scripts' ), 11 );

			// Product Categories fields.
			add_action( 'product_cat_add_form_fields', array( $this, 'product_cat_add_form_fields' ), 10 );
			add_action( 'product_cat_edit_form_fields', array( $this, 'edit_category_fields' ), 11 );
			add_action( 'created_term', array( $this, 'save_category_fields' ), 10, 3 );
			add_action( 'edit_term', array( $this, 'save_category_fields' ), 10, 3 );

			/* Ajax action for reset points */
			add_action( 'wp_ajax_ywpar_reset_points', array( $this, 'reset_points' ) );
			add_action( 'wp_ajax_nopriv_ywpar_reset_points', array( $this, 'reset_points' ) );
			add_action( 'wp_ajax_ywpar_bulk_action', array( $this, 'bulk_action' ) );
			add_action( 'wp_ajax_nopriv_ywpar_bulk_action', array( $this, 'bulk_action' ) );

			/* Ajax action for apply points previous order */
			add_action( 'wp_ajax_ywpar_apply_wc_points_rewards', array( $this, 'apply_wc_points_rewards' ) );
			add_action( 'wp_ajax_nopriv_ywpar_apply_wc_points_rewards', array( $this, 'apply_wc_points_rewards' ) );

			/* Add widgets into the dashboard */
			add_action( 'wp_dashboard_setup', array( $this, 'ywpar_points_widgets' ) );

			// APPLY_FILTER : ywpar_enable_product_meta: enable or disable the points and rewards meta fields on product editor.
			if ( ! apply_filters( 'ywpar_enable_product_meta', true ) ) {
				return;
			}
			// Custom fields for single product.
			add_action( 'woocommerce_product_options_general_product_data', array( $this, 'add_custom_fields_for_single_products' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_custom_fields_for_single_products' ), 10, 2 );

			// Custom fields for variation.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_custom_fields_for_variation_products' ), 14, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'save_custom_fields_for_variation_products' ), 10 );

			// Add a message in administrator panel if there's the old mode.
			add_action( 'admin_notices', array( $this, 'new_expiration_mode_message' ) );
			// Check if WooCommerce Coupons are enabled.
			add_action( 'admin_notices', array( $this, 'check_coupon' ) );

			add_action( 'woocommerce_settings_save_points', array( $this, 'save_value_of_custom_type_field' ) );
			add_action( 'admin_init', array( $this, 'add_filter_to_get_option' ) );

			add_action( 'woocommerce_admin_settings_sanitize_option_ywpar_my_account_page_endpoint', array( $this, 'end_point_sanitize' ) );

			if ( YITH_WC_Points_Rewards()->get_option( 'enable_points_on_birthday_exp' ) === 'yes' ) {
				if ( ! ( class_exists( 'YITH_WC_Coupon_Email_System' ) && get_option( 'ywces_enable_birthday' ) === 'yes' ) ) {
					add_action( 'show_user_profile', array( $this, 'add_birthday_field_admin' ) );
					add_action( 'edit_user_profile', array( $this, 'add_birthday_field_admin' ) );
					add_action( 'personal_options_update', array( $this, 'save_birthday_field_admin' ) );
					add_action( 'edit_user_profile_update', array( $this, 'save_birthday_field_admin' ) );
				}
			}

			add_action( 'woocommerce_admin_settings_sanitize_option_ywpar_affiliates_earning_conversion', array( $this, 'sanitize_affiliates_earning_conversion_field' ) );

			YITH_WC_Points_Rewards_Porting();

			
			// transient management
			add_action( 'save_post', array( $this, 'delete_transient_on_product_saved'), 10, 1);
			add_action( 'wp_trash_post', array( $this, 'delete_transient_on_product_saved' ), 10 ,1 );
			add_action( 'untrash_post', array( $this, 'delete_transient_on_product_saved' ), 10 ,1 );
			add_action( 'delete_post', array( $this, 'delete_transient_on_product_saved' ), 10 ,1 );
			add_action( 'yit_panel_wc_after_update', array( $this, 'delete_transient_on_settings_updated' ) );
			add_action( 'yit_panel_wc_after_reset', array( $this, 'delete_transient_on_settings_updated' ) );
			
		}

		/**
		 * Clean Points Transient on Points Tab Settings Updated
		 * 
		 * @since   2.0.1
		 * @author  Armando Liccardo
		 * @return void
		 */
		public function delete_transient_on_settings_updated() {
			if ( apply_filters( 'ywpar_use_points_transient', false ) ) {
				$page = isset( $_GET['page'] ) ? $_GET['page'] : '';
				$tab = isset( $_GET['tab'] ) ? $_GET['tab'] : '';
				
				if ( 'yith_woocommerce_points_and_rewards' === $page && 'points' === $tab ) {
					delete_site_transient('ywpar_product_points');
				}

				if ( 'yith_woocommerce_points_and_rewards' === $page && isset( $_POST['yit-action'] ) && 'wc-options-reset' === $_POST['yit-action']) {
					delete_site_transient('ywpar_product_points');
				}

			}
		}

		/**
		 * Clean Points Transient by product ID on Product Saved
		 * 
		 * @param int 	  $id
		 * @param WP_POST $post
		 * @param bool	  $update
		 * @since   2.0.1
		 * @author  Armando Liccardo
		 * @return void
		 */
		public function delete_transient_on_product_saved( $id ) {
			if ( apply_filters( 'ywpar_use_points_transient', false ) ) {
			
				if ( 'product' === get_post_type( $id )  ) {
					$p = wc_get_product($id);
					if ( $p->is_type( 'variable' )) {
						$vars = $p->get_available_variations();
						foreach( $vars as $v ) {
							$this->clean_points_transient( $v['variation_id'] );
						}
					}

					$this->clean_points_transient( $id );
					
				}
				if ( 'product_variation' === get_post_type( $id ) ) {
					$this->clean_points_transient( $id );
				}
			}
		}

		/**
		 * Clean Points Transient by product ID
		 * 
		 * @param int $id
		 * @since   2.0.1
		 * @author  Armando Liccardo
		 * @return void
		 */
		public function clean_points_transient( $id ) {
			$points_transient = get_site_transient( 'ywpar_product_points' );
			$points_transient = empty( $points_transient ) ? array() : $points_transient;
			$active_currency       = get_woocommerce_currency();

			if ( is_user_logged_in() ) {
				$user = wp_get_current_user();
				$user_role = $user->roles[0];
			} else {
				$user_role = 'guest';
			}

			if ( isset( $points_transient[$id]) ) {
				unset($points_transient[$id]);
				set_site_transient( 'ywpar_product_points', $points_transient );
			}
		}

		/**
		 * Display Admin Notice if coupons are enabled
		 *
		 * @access public
		 * @return void
		 * @author  Armando Liccardo
		 * @since  1.7.6
		 */
		public function check_coupon() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			if ( isset( $_GET['page']) && 'yith_woocommerce_points_and_rewards' === $_GET['page'] ) { //phpcs:ignore
				if ( 'yes' !== get_option( 'woocommerce_enable_coupons' ) && 'yes' !== get_option( 'ywpar_dismiss_disabled_coupons_warning_message', 'no' ) ) { ?>
					<div id="message" class="notice notice-warning ywpar_disabled_coupons">
						<p>
							<strong><?php esc_html_e( 'YITH WooCommerce Points and Rewards', 'yith-woocommerce-points-and-rewards' ); ?></strong>
						</p>

						<p>
							<?php esc_html_e( 'WooCommerce coupon system has been disabled. In order to make YITH WooCommerce Points and Rewards work correctly, you have to enable coupons.', 'yith-woocommerce-points-and-rewards' ); ?>
						</p>

						<p>
							<a href="<?php echo esc_url( admin_url( 'admin.php?page=wc-settings&tab=general' ) ); ?>"><?php echo esc_html__( 'Enable the use of coupons', 'yith-woocommerce-points-and-rewards' ); ?></a>
						</p>
					</div>
					<?php
				}
			}
		}

		/**
		 *
		 * Fix My Points page endpoint, replacing spaces with -
		 *
		 * @since 1.7.3
		 * @author  Armando Liccardo
		 * @param $value
		 * @return string|string[]|null
		 */
		public function end_point_sanitize( $value ) {
			if ( empty( $value ) ) {
				$value = '';
			} else {
				$value = strtolower( preg_replace( '/\s+/', '-', $value ) );

			}

			return $value;
		}


		/**
		 *
		 */
		public function add_filter_to_get_option() {
			$option_list = array(
				'ywpar_rewards_percentual_conversion_rate',
				'ywpar_rewards_points_role_rewards_fixed_conversion_rate',
				'ywpar_rewards_points_role_rewards_percentage_conversion_rate',
				'ywpar_earn_points_conversion_rate',
				'ywpar_rewards_conversion_rate',
				'ywpar_earn_points_role_conversion_rate',
				'ywpar_review_exp',
				'ywpar_num_order_exp',
				'ywpar_amount_spent_exp',
				'ywpar_number_of_points_exp',
				'ywpar_checkout_threshold_exp',
			);

			foreach ( $option_list as $option ) {
				add_filter( 'option_' . $option, array( $this, 'maybe_unserialize' ), 10, 2 );
			}
		}


		/**
		 *
		 * @param $value
		 * @param $option
		 *
		 * @return mixed
		 */
		public function maybe_unserialize( $value, $option ) {
			return maybe_serialize( $value );
		}

		/**
		 * Add customer birthday field
		 *
		 * @since   1.1.3
		 *
		 * @param   $user
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 * @throws Exception
		 */
		public function add_birthday_field_admin( $user ) {

			if ( ! current_user_can( 'manage_woocommerce' ) ) {
				return;
			}

			$date_format      = YITH_WC_Points_Rewards()->get_option( 'birthday_date_format' );
			$date_formats     = ywpar_get_date_formats();
			$date_placeholder = ywpar_date_placeholders();
			$date_patterns    = ywpar_get_date_patterns();
			$birth_date       = '';
			$registered_date  = YITH_WC_Points_Rewards()->get_user_birthdate( $user->ID );

			if ( ! empty( $user ) && $registered_date ) {
				$date       = DateTime::createFromFormat( 'Y-m-d', esc_attr( $registered_date ) );
				$birth_date = $date->format( $date_formats[ $date_format ] );
			}
			?>
			<h3><?php esc_html_e( 'Points and Rewards', 'yith-woocommerce-points-and-rewards' ); ?></h3>
			<table class="form-table">
				<tr>
					<th>
						<label for="yith_birthday"><?php esc_html_e( 'Date of birth', 'yith-woocommerce-points-and-rewards' ); ?></label>
					</th>
					<td>
						<input
								type="text"
								class="ywpar_date"
								name="yith_birthday"
								id="yith_birthday"
								value="<?php echo esc_attr( $birth_date ); ?>"
								placeholder="<?php echo esc_attr( $date_placeholder[ $date_format ] ); ?>"
								maxlength="10"
								pattern="<?php echo esc_attr( $date_patterns[ $date_format ] ); ?>"

						/>
					</td>
				</tr>
			</table>

			<?php

		}

		/**
		 * Save customer birth date from admin page
		 *
		 * @since   1.0.0
		 *
		 * @param   $customer_id
		 *
		 * @return  void
		 * @author  Alberto Ruggiero
		 */
		public function save_birthday_field_admin( $customer_id ) {
			if ( isset( $_POST['yith_birthday'] ) ) {
				$date_format   = YITH_WC_Points_Rewards()->get_option( 'birthday_date_format' );
				$date_patterns = ywpar_get_date_patterns();
				if ( $_POST['yith_birthday'] != '' ) {
					if ( preg_match( "/{$date_patterns[$date_format]}/", $_POST['yith_birthday'] ) ) {
						YITH_WC_Points_Rewards()->save_birthdate( $customer_id );
					}
				} else {
					delete_user_meta( $customer_id, 'yith_birthday' );

				}
			}

		}

		/**
		 * Add a message in my admin if expiration points is enabled
		 *
		 * @since 1.3.0
		 */
		public function new_expiration_mode_message() {
			// APPLY_FILTER : ywpar_expiration_old_mode: return true if you want disable the fix for expiration mode
			if ( ! current_user_can(
				'
}manage_options'
			) || 'from_1.3.0' == get_option( 'yit_ywpar_expiration_mode' ) || isset( $_COOKIE['ywpar_notice'] ) || apply_filters( 'ywpar_expiration_old_mode', false ) ) {
				return;
			}
				// since 1.3.0!
			?>
				<div id="ywpar-notice-is-dismissable" class="error notice is-dismissible">
					<p>
						<strong><?php echo esc_html_x( 'YITH WooCommerce Points and Rewards', 'Do not translate', 'yith-woocommerce-points-and-rewards' ); ?></strong>
					</p>

					<p>
						<?php esc_html_e( 'Due to some bugs, the point expiration system has been disabled.', 'yith-woocommerce-points-and-rewards' ); ?>
					</p>

					<p>
						<a href="<?php echo esc_url( admin_url( 'admin.php?page=' . $this->_panel_page . '&tab=expiration' ) ); ?>"><?php esc_html_e( 'To enable it, please follow these instructions.', 'yith-woocommerce-points-and-rewards' ); ?></a>
					</p>
				</div>

				<?php
		}


		/**
		 * Add csv to mime file type
		 *
		 * @since 1.1.3
		 *
		 * @param $mime_types
		 *
		 * @return mixed
		 */
		public function add_mime_types( $mime_types ) {
			$mime_types['csv'] = 'text/csv';
			$mime_types['txt'] = 'text/plain';
			return $mime_types;
		}

		/**
		 * Set the settings tabs enabled to shop manager
		 *
		 * @access public
		 * @
		 *
		 * @param $panel_options
		 *
		 * @return array
		 * @since 1.1.3
		 */
		public function admin_panel_options_for_shop_manager( $panel_options ) {
			add_filter( 'option_page_capability_yit_' . $panel_options['parent'] . '_options', array( $this, 'change_capability' ) );
			$panel_options['capability'] = 'manage_woocommerce';
			return $panel_options;
		}

		/**
		 * Enqueue styles and scripts
		 *
		 * @access public
		 * @return void
		 * @since 1.0.0
		 */
		public function enqueue_styles_scripts() {

			if ( ( isset( $_GET['page'] ) && $_GET['page'] == 'yith_woocommerce_points_and_rewards' ) ) {
				wp_enqueue_script( 'jquery-ui-datepicker' );

				if ( ! wp_script_is( 'selectWoo' ) ) {
					wp_enqueue_script( 'selectWoo' );
					wp_enqueue_script( 'wc-enhanced-select' );
				}
				// load select2

				wp_enqueue_style( 'woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css', array(), WC_VERSION );
				wp_enqueue_style(
					'select2',
					str_replace(
						array(
							'http:',
							'https:',
						),
						'',
						WC()->plugin_url()
					) . '/assets/' . 'css/select2.css'
				);

				// backend enqueue scripts/css
				wp_enqueue_style( 'yith_ywpar_backend', YITH_YWPAR_ASSETS_URL . '/css/backend.css', array(), YITH_YWPAR_VERSION );
				wp_enqueue_script(
					'yith_ywpar_admin',
					YITH_YWPAR_ASSETS_URL . '/js/ywpar-admin' . YITH_YWPAR_SUFFIX . '.js',
					array(
						'jquery',
						'jquery-ui-sortable',
					),
					YITH_YWPAR_VERSION,
					true
				);
				wp_enqueue_script( 'jquery-blockui', YITH_YWPAR_ASSETS_URL . '/js/jquery.blockUI.min.js', array( 'jquery' ), false, true );

				// load cookie for administator message dismiss.
				$assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
				wp_enqueue_script( 'jquery-cookie', $assets_path . 'js/jquery-cookie/jquery.cookie.min.js', array( 'jquery' ), '1.4.1', true );
				// APPLY_FILTER : yith_ywpar_block_loader_admin: to filter the block loader gif
				wp_localize_script(
					'yith_ywpar_admin',
					'yith_ywpar_admin',
					array(
						'ajaxurl'                         => admin_url( 'admin-ajax.php' ),
						'apply_wc_points_rewards'         => wp_create_nonce( 'apply_wc_points_rewards' ),
						'fix_expiration_points'           => wp_create_nonce( 'fix_expiration_points' ),
						'reset_points'                    => wp_create_nonce( 'reset_points' ),
						'reset_points_confirm'            => __( 'Are you sure that want reset all points? This process is irreversible', 'yith-woocommerce-points-and-rewards' ),
						'import_points_import_file_empty' => __( 'The import file is empty', 'yith-woocommerce-points-and-rewards' ),
						// from 1.2.0
						'block_loader'                    => apply_filters( 'yith_ywpar_block_loader_admin', YITH_YWPAR_ASSETS_URL . '/images/block-loader.gif' ),
						'reset_point_message'             => __( 'Do you want to reset points for user', 'yith-woocommerce-points-and-rewards' ),
						'import_button_label'             => esc_html__( 'Start Import', 'yith-woocommerce-points-and-rewards'),
						'export_button_label'             => esc_html__( 'Start Export', 'yith-woocommerce-points-and-rewards'),
					)
				);
			}

			global $pagenow;
			/* load needed css and js from plugin-fw for settings in product editing page */
			if ( in_array( $pagenow, array( 'post-new.php' ) ) || isset( $_GET['post'] ) && 'product' === get_post_type( $_GET['post'] ) || isset( $_GET['taxonomy'] ) && 'product_cat' === $_GET['taxonomy'] ) {
				wp_enqueue_style( 'yith_ywpar_backend', YITH_YWPAR_ASSETS_URL . '/css/backend.css',array(), YITH_YWPAR_VERSION );
				wp_enqueue_style( 'yith-plugin-fw-fields' );
				wp_enqueue_style( 'ywctm-admin-premium' );
				wp_enqueue_script( 'yith-plugin-fw-fields' );
				wp_enqueue_script( 'ywctm-admin-premium' );
				wp_enqueue_script( 'yit-plugin-panel' );
				wp_register_script( 'ywpar-admin-product', YITH_YWPAR_ASSETS_URL . '/js/ywpar-admin-product' . YITH_YWPAR_SUFFIX . '.js', array( 'jquery' ), YITH_YWPAR_VERSION,  true );
				wp_enqueue_script( 'ywpar-admin-product' );

				if ( isset( $_GET['taxonomy'] ) && 'product_cat' === $_GET['taxonomy'] ) {
					wp_enqueue_script(
						'yith_ywpar_admin',
						YITH_YWPAR_ASSETS_URL . '/js/ywpar-admin' . YITH_YWPAR_SUFFIX . '.js',
						array(
							'jquery',
							'jquery-ui-sortable',
						),
						YITH_YWPAR_VERSION,
						true
					);
				}

			}
		}

		/**
		 * Create Menu Items
		 *
		 * Print admin menu items
		 *
		 * @since  1.0.0
		 * @author Emanuela Castorina
		 */
		private function create_menu_items() {
			// Add a panel under YITH Plugins tab
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'yith_ywpar_customers', array( $this, 'customers_tab' ) );
			add_action( 'yith_ywpar_bulk', array( $this, 'bulk_tab' ) );
			add_action( 'yith_import_export', array( $this, 'import_export' ) );
		}

		/**
		 * Add a panel under YITH Plugins tab
		 *
		 * @return   void
		 * @since    1.0.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @use      /Yit_Plugin_Panel class
		 * @see      plugin-fw/lib/yit-plugin-panel.php
		 */
		public function register_panel() {

			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = apply_filters(
				'ywpar_show_admin_tabs',
				array(
					'customers-tab' => __( 'Customers\' points', 'yith-woocommerce-points-and-rewards' ),
					'general'      => __( 'General options', 'yith-woocommerce-points-and-rewards' ),
					'points'       => __( 'Points Options', 'yith-woocommerce-points-and-rewards' ),
					'labels'       => __( 'Labels', 'yith-woocommerce-points-and-rewards' ),
					'emails'       => __( 'Emails', 'yith-woocommerce-points-and-rewards' ),
					'import'       => __( 'Import/Export', 'yith-woocommerce-points-and-rewards' ),
				)
			);

			// APPLY_FILTER : ywpar_admin_panel_options: to filter the arguments to create admin panel
			$args = apply_filters(
				'ywpar_admin_panel_options',
				array(
					'create_menu_page' => true,
					'parent_slug'      => '',
					'page_title'       => _x( 'YITH WooCommerce Points and Rewards', 'Plugin name, do not translate', 'yith-woocommerce-points-and-rewards' ),
					'menu_title'       => _x( 'Points and Rewards', 'Plugin name, do not translate', 'yith-woocommerce-points-and-rewards' ),
					'capability'       => 'manage_options',
					'parent'           => 'ywpar',
					'parent_page'      => 'yith_plugin_panel',
					'page'             => $this->_panel_page,
					'admin-tabs'       => $admin_tabs,
					'options-path'     => YITH_YWPAR_DIR . '/plugin-options',
					'class'            => yith_set_wrapper_class(),
				)
			);

			// enable shop manager to change Customer points
			if ( YITH_WC_Points_Rewards()->get_option( 'enabled_shop_manager' ) == 'yes' ) {
				// enable shop manager to set Points Setting
				add_filter( 'option_page_capability_yit_' . $args['parent'] . '_options', array( $this, 'change_capability' ) );
				add_filter( 'yit_plugin_panel_menu_page_capability', array( $this, 'change_capability' ) );
				$args['capability'] = 'manage_woocommerce';
			}

			/* === Fixed: not updated theme  === */
			if ( ! class_exists( 'YIT_Plugin_Panel_WooCommerce' ) ) {
				require_once YITH_YWPAR_DIR . '/plugin-fw/lib/yit-plugin-panel-wc.php';
			}

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
			add_filter( 'yith_plugin_fw_get_field_template_path', array( $this, 'get_yith_panel_custom_template' ), 10, 2 );
			add_filter( 'yith_plugin_fw_wc_panel_pre_field_value', array( $this, 'get_value_of_custom_type_field' ), 10, 2 );

			$this->save_default_options();

		}

		/**
		 * @param $template
		 * @param $field
		 *
		 * @return string
		 */
		public function get_yith_panel_custom_template( $template, $field ) {
			$custom_option_types = array(
				'options-conversion',
				'options-conversion-earning',
				'options-expire',
				'options-role-conversion',
				'options-percentage-conversion',
				'options-role-percentage-conversion',
				'options-extrapoints',
				'options-import-form',
				'options-bulk-form',
				'points-previous-order',
				'options-restrictions-text-input'
			);
			$field_type          = $field['type'];
			if ( isset( $field['type'] ) && in_array( $field['type'], $custom_option_types ) ) {
				$template = YITH_YWPAR_TEMPLATE_PATH . "/panel/types/{$field_type}.php";
			}

			return $template;
		}

		/**
		 * @param $value
		 * @param $field
		 *
		 * @return mixed|void
		 */
		public function get_value_of_custom_type_field( $value, $field ) {
			$custom_option_types = array(
				'options-conversion',
				'options-role-conversion',
				'options-percentage-conversion',
				'options-role-percentage-conversion',
				'options-extrapoints',
				'options-bulk-form',
				'options-import-form',
				'points-previous-order',
			);

			if ( isset( $field['type'] ) && in_array( $field['type'], $custom_option_types ) ) {
				$value = get_option( $field['id'], $field['default'] );
			}

			return $value;
		}


		/**
		 * Modify the capability
		 *
		 * @param $capability
		 *
		 * @return string
		 */
		function change_capability( $capability ) {
			return 'manage_woocommerce';
		}

		/**
		 * Save default options when the plugin is installed
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function save_default_options() {

			$options                = maybe_unserialize( get_option( 'yit_ywpar_options', array() ) );
			$current_option_version = get_option( 'yit_ywpar_option_version', '0' );
			$forced                 = isset( $_GET['update_ywpar_options'] ) && $_GET['update_ywpar_options'] == 'forced';
			$multicurrency          = get_option( 'yit_ywpar_multicurrency' );

			if ( version_compare( $current_option_version, YITH_YWPAR_VERSION, '>=' ) && ! $forced ) {
				return;
			}

			if ( version_compare( $current_option_version, '1.7.0', '<' ) ) {
				// check if there's the old expiration mode
				YITH_WC_Points_Rewards_Earning()->yith_ywpar_reset_expiration_points();

				// retro-compatibility
				$new_option = array_merge( $this->_panel->get_default_options(), (array) $options );
				update_option( 'yit_ywpar_options', $new_option );

				ywpar_options_porting( $options );
			}

			if ( false === $multicurrency ) {
				ywpar_conversion_points_multilingual();
			}

			update_option( 'yit_ywpar_option_version', YITH_YWPAR_VERSION );

		}



		/**
		 * Customers Tab Template
		 *
		 * Load the customers tab template on admin page
		 *
		 * @return   void
		 * @since    1.0.0
		 * @author   Emanuela Castorina
		 */
		public function customers_tab() {
			$points = 0;
			$type   = 'view';
			if ( isset( $_REQUEST['action'] ) && isset( $_REQUEST['user_id'] ) ) {
				$user_id = $_REQUEST['user_id'];
				$type    = 'customer';

				switch ( $_REQUEST['action'] ) {
					case 'reset':
						YITH_WC_Points_Rewards()->reset_user_points( $user_id );
						break;
					case 'ban':
						YITH_WC_Points_Rewards()->ban_user( $user_id );
						break;
					case 'unban':
						YITH_WC_Points_Rewards()->unban_user( $user_id );
						break;
				}
				$link = remove_query_arg( array( 'action', 'user_id' ) );

				$this->cpt_obj = new YITH_WC_Points_Rewards_Customer_History_List_Table();
			} else {
				$this->cpt_obj = new YITH_WC_Points_Rewards_Customers_List_Table();
			}

			$customers_tab = YITH_YWPAR_TEMPLATE_PATH . '/admin/customers-tab.php';
			if ( file_exists( $customers_tab ) ) {
				include_once $customers_tab;
			}
		}
		/**
		 * Bulk Form Tab Template
		 *
		 * Load the Bulk tab template on admin page
		 *
		 * @return   void
		 * @since    2.0.0
		 * @author   Armando Liccardo
		 */
		public function bulk_tab() {

			$bulk_form = YITH_YWPAR_TEMPLATE_PATH . '/admin/bulk-tab.php';
			if ( file_exists( $bulk_form ) ) {
				include_once $bulk_form;
			}

		}

		/**
		 * Import Export Tab Template
		 *
		 * Load the Bulk tab template on admin page
		 *
		 * @return   void
		 * @since    2.0.0
		 * @author   Armando Liccardo
		 */
		public function import_export() {

			$import_export = YITH_YWPAR_TEMPLATE_PATH . '/admin/import_export-tab.php';
			if ( file_exists( $import_export ) ) {
				include_once $import_export;
			}

		}

		/**
		 * Add custom fields for single product
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function add_custom_fields_for_single_products() {
			global $thepostid;

			// Compatibility with Multivendor
			if ( function_exists( 'yith_get_vendor' ) ) {
				$vendor = yith_get_vendor( 'current', 'user' );
				if ( $vendor->is_valid() && $vendor->has_limited_access() ) {
					return;
				}
			}

			$product = wc_get_product( $thepostid );
			$points_to_earn = yit_get_prop( $product, '_ywpar_point_earned', true );
			/* check this for previous versions compatibility */
			if ( metadata_exists('post', $thepostid, '_ywpar_override_points_earning' ) ) {
				$override_enabled = yit_get_prop( $product, '_ywpar_override_points_earning', true );
			} else {
				if ( $points_to_earn === '0' || !empty( $points_to_earn)  ) {
					$override_enabled = 'yes';
				} else { $override_enabled = 'no'; }
			}

			if ( metadata_exists('post', $thepostid, '_ywpar_fixed_or_percentage' ) ) {
				$ywpar_fixed_or_percentage = yit_get_prop( $product, '_ywpar_fixed_or_percentage', true );
			} else {
				if ( $points_to_earn === '0' ) {
					$ywpar_fixed_or_percentage = 'not_assign';
				} else if ( !empty( $points_to_earn) && strpos( $points_to_earn, '%') > 0 ) {
					$ywpar_fixed_or_percentage = 'percentage';
				} else { $ywpar_fixed_or_percentage = 'fixed'; }
			}

			$points_to_earn = str_replace( '%', '', $points_to_earn );

			/* date check */
			$ywpar_point_earned_dates_from = ( $date = yit_get_prop( $product, '_ywpar_point_earned_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
			$ywpar_point_earned_dates_to   = ( $date = yit_get_prop( $product, '_ywpar_point_earned_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';

			if ( metadata_exists('post', $thepostid, '_ywpar_override_points_date' ) ) {
				$override_date = yit_get_prop( $product, '_ywpar_override_points_date', true );
			} else {
				if ( !empty( $ywpar_point_earned_dates_from) && !empty( $ywpar_point_earned_dates_to ) ) {
					$override_date = 'yes';
				} else { $override_date = 'no'; }
			}


			/* maximum discount */
			$max_discount_val = yit_get_prop( $product, '_ywpar_max_point_discount', true );
			if ( metadata_exists('post', $thepostid, '_ywpar_override_maximum_discount' ) ) {
				$ywpar_apply_maximum_discount_onoff = yit_get_prop( $product, '_ywpar_override_maximum_discount', true );
			} else {
				if ( '0' === $max_discount_val || !empty( $max_discount_val) ) {
					$ywpar_apply_maximum_discount_onoff = 'yes';
				} else { $ywpar_apply_maximum_discount_onoff = 'no'; }
			}

			/* max discount type */
			if ( metadata_exists('post', $thepostid, '_ywpar_maximum_discount_type' ) ) {
				$ywpar_maximum_discount_type = yit_get_prop( $product, '_ywpar_maximum_discount_type', true );
			} else {
				if ( '0' === $max_discount_val ) {
					$ywpar_maximum_discount_type = 'not_set';
				}
				elseif ( !empty( $max_discount_val) && strpos( $max_discount_val, '%') > 0 ) {
					$ywpar_maximum_discount_type = 'percentage';
				} else { $ywpar_maximum_discount_type = 'fixed'; }
			}

			$conversion_type = YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' );

			$fields = array(
				'ywpar_override_points_earning' => array(
					'name'      => '_ywpar_override_points_earning',
					'title'     => esc_html__( 'Override points options', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable to override the global points options for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'value'   => $override_enabled,
					'id'        => '_ywpar_override_points_earning',
					'class'     => 'show_if_simple show_if_ywf_deposit'
				),
				'ywpar_fixed_or_percentage' => array(
					'name'      => '_ywpar_fixed_or_percentage',
					'title'     => esc_html__( 'For this product', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => '',
					'type'      => 'radio',
					'options'   => array(
						'not_assign' => esc_html__( 'Don\'t assign points', 'yith-woocommerce-points-and-rewards' ). '<br />' . esc_html__( '(Exclude this product from point collection)', 'yith-woocommerce-points-and-rewards' ),
						'fixed'      => esc_html__( 'Set a fixed number of points', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: 10 points only for this product)', 'yith-woocommerce-points-and-rewards' ),
						'percentage' => esc_html__( 'Set a % amount of points based on global points rules.', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: with a global rule of 10 points for each 10$, if you set a 50% amount for this product, the user will get 5 points for each 10$)', 'yith-woocommerce-points-and-rewards' ),
					),
					'value'   => $ywpar_fixed_or_percentage,
					'id'        => '_ywpar_fixed_or_percentage',
					'class'     => 'show_if_simple show_if_ywf_deposit',
					'deps'      => array(
						'id'    => '_ywpar_override_points_earning',
						'value' => 'yes',
						'type'  => 'hide',
					),
				),
				'ywpar_ywpar_point_earned' => array(
					'name'      => '_ywpar_point_earned',
					'title'     => esc_html__( 'Points to apply', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Set how many points to apply for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'text',
					'value'     => $points_to_earn,
					'id'        => '_ywpar_point_earned',
					'class'     => 'show_if_simple show_if_ywf_deposit',
					'deps'      => array(
						'id'    => '_ywpar_override_points_earning',
						'value' => 'yes',
						'type'  => 'hide',
					),
				),
				'ywpar_override_points_date' => array(
					'name'      => '_ywpar_override_points_date',
					'title'     => esc_html__( 'Override will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose to schedule the global points overriding or if to start and end it manually.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'radio',
					'options'   => array(
						'no'  => esc_html__( 'From now, until it\'s ended manually', 'yith-woocommerce-points-and-rewards' ),
						'yes' => esc_html__( 'Schedule a start and end date', 'yith-woocommerce-points-and-rewards' ),
					),
					'value'     => $override_date,
					'id'        => '_ywpar_override_points_date',
					'class'     => 'show_if_simple show_if_ywf_deposit'
				),

				'ywpar_override_points_date_fields' => array(
					'name'      => '_ywpar_override_points_date_fields',
					'title'     => esc_html__( 'Override rules will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Set a start and end time for these overriding rules. When the rules end, the global points options will be applied on this product.', 'yith-woocommerce-points-and-rewards' ),
					'id'        => '_ywpar_override_points_date_fields',
					'class'     => 'show_if_simple show_if_ywf_deposit',
					'type'      => 'text',
					'deps'      => array(
						'id'    => '_ywpar_override_points_date',
						'value' => 'yes',
						'type'  => 'hide',
					),
				),
				'ywpar_apply_maximum_discount_onoff' => array(
					'name'      => '_ywpar_override_maximum_discount',
					'title'     => esc_html__( 'Override maximum discount', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable to override the global max discount for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'value'     => $ywpar_apply_maximum_discount_onoff,
					'id'        => '_ywpar_override_maximum_discount',
					'class'     => 'show_if_simple show_if_ywf_deposit'
				),
			);

			if ( 'fixed' === $conversion_type ) {
				$fixed_fields = array(
					'ywpar_maximum_discount_type' => array(
						'name'      => '_ywpar_maximum_discount_type',
						'title'     => esc_html__( 'For this product', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => '',
						'type'      => 'radio',
						'options'   => array(
							'not_set' => esc_html__( 'Don\'t set a max discount', 'yith-woocommerce-points-and-rewards' ),
							'fixed'      => esc_html__( 'Set a fixed max discount value', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: max 5$ of discount for this product).', 'yith-woocommerce-points-and-rewards' ),
							'percentage' => esc_html__( 'Set a % max discount based on the global max discount.', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: with a global max discount of 50$ if you set a max discount of 10% for this product the user will get a max discount of 5$ for this product).', 'yith-woocommerce-points-and-rewards' ),
						),
						'value'   => $ywpar_maximum_discount_type,
						'id'        => '_ywpar_maximum_discount_type',
						'class'     => 'show_if_simple show_if_ywf_deposit show_if_fixed_reedem',
						'deps'      => array(
							'id'    => '_ywpar_override_maximum_discount',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),
					'ywpar_max_point_discount' => array(
						'name'      => '_ywpar_max_point_discount',
						'title'     => esc_html__( 'Max discount value', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => esc_html__( 'Set the max discount that can be applied to this product.', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'value'     => $max_discount_val,
						'id'        => '_ywpar_max_point_discount',
						'class'     => 'show_if_simple show_if_ywf_deposit',
						'deps'      => array(
							'id'    => '_ywpar_override_maximum_discount',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),

				);
				$fields = array_merge( $fields, $fixed_fields );
			} else if ( 'percentage' === $conversion_type ) {
				$max_discount_val_percentage_reedem = yit_get_prop( $product, '_ywpar_redemption_percentage_discount', true );
				$fields = array_merge( $fields, array(
					'ywpar_redemption_percentage_discount' => array(
						'name'      => '_ywpar_redemption_percentage_discount',
						'title'     => esc_html__( 'Max discount value', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => esc_html__( 'Set the max percentage discount that can be applied to this product.', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'value'     => $max_discount_val_percentage_reedem,
						'id'        => '_ywpar_redemption_percentage_discount',
						'class'     => 'show_if_simple show_if_ywf_deposit',
						'deps'      => array(
							'id'    => '_ywpar_override_maximum_discount',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),
				) );
			}

			?>
				<div class="options_group yith-plugin-ui show_if_simple show_if_ywf_deposit">
				<h3 class="ywpar-product-options-title"><?php esc_html_e('Points options', 'yith-woocommerce-points-and-rewards')?></h3>
				<table id="ywpar_product_otions" >
					<tbody>
					<?php foreach ( $fields as $field ) : ?>
					<?php
						$data_deps = '';
						if ( isset($field['deps']) ){
							$data_deps = 'data-dep-target="'. $field['id'] .'" data-dep-id="'.$field['deps']['id'].'" data-dep-value="'.$field['deps']['value'].'" data-dep-type="'.$field['deps']['type'].'"';
						}
						?>
						<tr valign="top" class="yith-plugin-fw-panel-wc-row <?php echo esc_html( $field['type'] ); ?> <?php echo esc_html( $field['name'] ); ?> <?php echo isset( $field['class'] ) ? esc_html( $field['class'] ) : ''; ?>" <?php echo $data_deps; ?> >
							<th scope="row" class="titledesc">
								<label for="<?php echo esc_html( $field['id'] ); ?>"><?php echo esc_html( $field['title'] ); ?></label>
							</th>
							<td class="forminp forminp-<?php echo esc_html( $field['type'] ); ?>">
								<?php
								if ( '_ywpar_override_points_date_fields' === $field['id'] ) {
									?>
									<div class="yith-plugin-fw-field-wrapper yith-plugin-fw-text-field-wrapper">
						                    <input type="text" name="_ywpar_point_earned_dates_from" id="_ywpar_point_earned_dates_from" value="<?php esc_attr_e( $ywpar_point_earned_dates_from ); ?>" placeholder="<?php echo esc_attr_x( 'From&hellip;', 'placeholder', 'yith-woocommerce-points-and-rewards' ) ?> YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"/>
						                    <input type="text" name="_ywpar_point_earned_dates_to" id="_ywpar_point_earned_dates_to" value="<?php esc_attr_e( $ywpar_point_earned_dates_to ); ?>" placeholder="<?php echo esc_attr_x( 'To&hellip;', 'placeholder', 'yith-woocommerce-points-and-rewards' ) ?> YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"/>
									</div>
										<?php
								} else {
									yith_plugin_fw_get_field( $field, true );
								}

								if ( '_ywpar_point_earned' === $field['id'] ) {
									echo '<span class="ywpar_type_sign">' . esc_html__( 'points', 'yith-woocommerce-points-and-rewards' ) . '</span>';
								}
								if ( '_ywpar_redemption_percentage_discount' === $field['id'] ) {
									echo '<span class="ywpar_type_sign">%</span>';
								}

								if ( isset( $field['desc'] ) && '' !== $field['desc'] ) {
									?>
									<div style="clear: both;"></div>
									<span class="description"><?php echo esc_html( $field['desc'] ); ?></span>
									<?php
								}
								?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				</div>

			<?php
		}

		/**
		 * Save custom fields for single product
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $post_id
		 * @param $post
		 *
		 * @return void
		 */
		public function save_custom_fields_for_single_products( $post_id, $post ) {

			$args = array();
			if ( isset( $_POST['_ywpar_override_points_earning'] ) ) {
				$args['_ywpar_override_points_earning'] = $_POST['_ywpar_override_points_earning'];
			} else {
				$args['_ywpar_override_points_earning'] = 'no';
			}

			if ( isset( $_POST['_ywpar_override_points_date'] ) ) {
				$args['_ywpar_override_points_date'] = $_POST['_ywpar_override_points_date'];
			} else {
				$args['_ywpar_override_points_date'] = 'no';
			}

			if ( isset( $_POST['_ywpar_override_maximum_discount'] ) ) {
				$args['_ywpar_override_maximum_discount'] = $_POST['_ywpar_override_maximum_discount'];
			} else {
				$args['_ywpar_override_maximum_discount'] = 'no';
			}

			if ( isset( $_POST['_ywpar_maximum_discount_type'] ) ) {
				$args['_ywpar_maximum_discount_type'] = $_POST['_ywpar_maximum_discount_type'];
			}

			if ( isset( $_POST['_ywpar_fixed_or_percentage'] ) ) {
				$args['_ywpar_fixed_or_percentage'] = $_POST['_ywpar_fixed_or_percentage'];
			}
			if ( isset( $_POST['_ywpar_point_earned'] ) ) {
				$args['_ywpar_point_earned'] = $_POST['_ywpar_point_earned'];
			}

			if ( isset( $_POST['_ywpar_point_earned_dates_from'] ) ) {
				$args['_ywpar_point_earned_dates_from'] = strtotime( $_POST['_ywpar_point_earned_dates_from'] );
			}
			if ( isset( $_POST['_ywpar_point_earned_dates_to'] ) ) {
				$args['_ywpar_point_earned_dates_to'] = strtotime( $_POST['_ywpar_point_earned_dates_to'] );
			}
			if ( isset( $_POST['_ywpar_max_point_discount'] ) ) {
				$args['_ywpar_max_point_discount'] = $_POST['_ywpar_max_point_discount'];
			}
			if ( isset( $_POST['_ywpar_redemption_percentage_discount'] ) ) {
				$args['_ywpar_redemption_percentage_discount'] = $_POST['_ywpar_redemption_percentage_discount'];
			}

			if ( ! empty( $args ) ) {
				$product = wc_get_product( $post_id );
				yit_save_prop( $product, $args, false, true );
			}

		}

		/**
		 * Add custom fields for variation products
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $loop
		 * @param $variation_data
		 * @param $variations
		 *
		 * @return void
		 */
		public function add_custom_fields_for_variation_products( $loop, $variation_data, $variations ) {

			$product                              = wc_get_product( $variations->ID );
			$ywpar_point_earned                   = yit_get_prop( $product, '_ywpar_point_earned', true );
			$ywpar_max_point_discount             = yit_get_prop( $product, '_ywpar_max_point_discount', true );
			$ywpar_redemption_percentage_discount = yit_get_prop( $product, '_ywpar_redemption_percentage_discount', true );
			$ywpar_point_earned_dates_from        = ( $date = yit_get_prop( $product, '_ywpar_point_earned_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
			$ywpar_point_earned_dates_to          = ( $date = yit_get_prop( $product, '_ywpar_point_earned_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';


			/* check this for previous versions compatibility */
			if ( metadata_exists('post', $variations->ID, '_ywpar_override_points_earning' ) ) {
				$override_enabled = yit_get_prop( $product, '_ywpar_override_points_earning', true );
			} else {
				if ( '0' === $ywpar_point_earned || !empty( $ywpar_point_earned) ) {
					$override_enabled = 'yes';
				} else { $override_enabled = 'no'; }
			}

			if ( metadata_exists('post', $variations->ID, '_ywpar_fixed_or_percentage' ) ) {
				$ywpar_fixed_or_percentage = yit_get_prop( $product, '_ywpar_fixed_or_percentage', true );
			} else {
				if ( '0' === $ywpar_point_earned ) {
					$ywpar_fixed_or_percentage = 'not_set';
				} elseif ( !empty( $ywpar_point_earned) && strpos( $ywpar_point_earned, '%') > 0 ) {
					$ywpar_fixed_or_percentage = 'percentage';
				} else { $ywpar_fixed_or_percentage = 'fixed'; }
			}

			/* date check */
			if ( metadata_exists('post', $variations->ID, '_ywpar_override_points_date' ) ) {
				$override_date = yit_get_prop( $product, '_ywpar_override_points_date', true );
			} else {
				if ( !empty( $ywpar_point_earned_dates_from) && !empty( $ywpar_point_earned_dates_to ) ) {
					$override_date = 'yes';
				} else { $override_date = 'no'; }
			}

			/* maximum discount */
			if ( metadata_exists('post', $variations->ID, '_ywpar_override_maximum_discount' ) ) {
				$ywpar_apply_maximum_discount_onoff = yit_get_prop( $product, '_ywpar_override_maximum_discount', true );
			} else {
				if ( '0' === $ywpar_max_point_discount || !empty( $ywpar_max_point_discount) ) {
					$ywpar_apply_maximum_discount_onoff = 'yes';
				} else { $ywpar_apply_maximum_discount_onoff = 'no'; }
			}

			$fields = array(
				'variable_ywpar_override_points_earning' => array(
					'name'      => 'variable_ywpar_override_points_earning['.$loop.']',
					'title'     => esc_html__( 'Override points options', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable to override the global points options for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'value'   => $override_enabled,
					'id'        => 'variable_ywpar_override_points_earning',
					'class'     => 'variable_ywpar_override_points_earning'
				),
				'variable_ywpar_fixed_or_percentage' => array(
					'name'      => 'variable_ywpar_fixed_or_percentage['.$loop.']',
					'title'     => esc_html__( 'For this product', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => '',
					'type'      => 'custom-radio',
					'options'   => array(
						'not_assign' => esc_html__( 'Don\'t assign points', 'yith-woocommerce-points-and-rewards' ). '<br />' . esc_html__( '(Exclude this product from point collection)', 'yith-woocommerce-points-and-rewards' ),
						'fixed'      => esc_html__( 'Set a fixed number of points', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: 10 points only for this product)', 'yith-woocommerce-points-and-rewards' ),
						'percentage' => esc_html__( 'Set a % amount of points based on global points rules.', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: with a global rule of 10 points for each 10$, if you set a 50% amount for this product, the user will get 5 points for each 10$)', 'yith-woocommerce-points-and-rewards' ),
					),
					'value'   => $ywpar_fixed_or_percentage,
					'id'        => 'variable_ywpar_fixed_or_percentage',
					'class'        => 'variable_ywpar_fixed_or_percentage',
				),
				'variable_ywpar_ywpar_point_earned' => array(
					'name'      => 'variable_ywpar_point_earned['.$loop.']',
					'title'     => esc_html__( 'Points to apply', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Set how many points to apply for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'text',
					'value'     => $ywpar_point_earned,
					'id'        => 'variable_ywpar_point_earned',
					'class'        => 'variable_ywpar_point_earned',
				),
				'variable_ywpar_override_points_date' => array(
					'name'      => 'variable_ywpar_override_points_date['.$loop.']',
					'title'     => esc_html__( 'Override will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose to schedule the global points overriding or if to start and end it manually.', 'yith-woocommerce-points-and-rewards' ),
					'options'   => array(
						'no'  => esc_html__( 'From now, until it\'s ended manually', 'yith-woocommerce-points-and-rewards' ),
						'yes' => esc_html__( 'Schedule a start and end date', 'yith-woocommerce-points-and-rewards' ),
					),
					'type'      => 'custom-radio',
					'value'     => $override_date,
					'id'        => 'variable_ywpar_override_points_date',
					'class'     => 'variable_ywpar_override_points_date'
				),
				'variable_ywpar_override_points_date_fields' => array(
					'name'      => 'variable_ywpar_override_points_date_fields['.$loop.']',
					'title'     => esc_html__( 'Override rules will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Set a start and end time for these overriding rules. When the rules end, the global points options will be applied on this product.', 'yith-woocommerce-points-and-rewards' ),
					'id'        => 'variable_ywpar_override_points_date_fields',
					'class'     => 'variable_ywpar_override_points_date_fields',
					'type'      => 'text',
				),
				'variable_ywpar_apply_maximum_discount_onoff' => array(
					'name'      => 'variable_ywpar_override_maximum_discount['.$loop.']',
					'title'     => esc_html__( 'Override maximum discount', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable to override the global max discount for this product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'value'     => $ywpar_apply_maximum_discount_onoff,
					'id'        => 'variable_ywpar_override_maximum_discount',
					'class'     => 'variable_ywpar_override_maximum_discount'
				),
			);

			$conversion_type = YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' );
			/* max discount type */
			if ( metadata_exists('post', $variations->ID, '_ywpar_maximum_discount_type' ) ) {
				$ywpar_maximum_discount_type = yit_get_prop( $product, '_ywpar_maximum_discount_type', true );
			} else {
				if ( '0' === $ywpar_max_point_discount ) {
					$ywpar_maximum_discount_type = 'not_assign';
				} elseif ( !empty( $ywpar_max_point_discount) && strpos( $ywpar_max_point_discount, '%') > 0 ) {
					$ywpar_maximum_discount_type = 'percentage';
				} else { $ywpar_maximum_discount_type = 'fixed'; }
			}

			if ( empty($ywpar_maximum_discount_type)) { $ywpar_maximum_discount_type = 'fixed'; }


			if ( 'fixed' === $conversion_type ) {
				$fixed_fields = array(
					'variable_ywpar_maximum_discount_type' => array(
						'name'      => 'variable_ywpar_maximum_discount_type['.$loop.']',
						'title'     => esc_html__( 'For this product', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => '',
						'type'      => 'custom-radio',
						'options'   => array(
							'not_set' => esc_html__( 'Don\'t set a max discount', 'yith-woocommerce-points-and-rewards' ),
							'fixed'      => esc_html__( 'Set a fixed max discount value', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: max 5$ of discount for this product).', 'yith-woocommerce-points-and-rewards' ),
							'percentage' => esc_html__( 'Set a % max discount based on the global max discount.', 'yith-woocommerce-points-and-rewards' ) . '<br />' . esc_html__( '(Example: with a global max discount of 50$ if you set a max discount of 10% for this product the user will get a max discount of 5$ for this product).', 'yith-woocommerce-points-and-rewards' ),
						),
						'value'   => $ywpar_maximum_discount_type,
						'id'        => 'variable_ywpar_maximum_discount_type',
						'class'     => 'variable_ywpar_maximum_discount_type',
					),
					'variable_ywpar_max_point_discount' => array(
						'name'      => 'variable_ywpar_max_point_discount['.$loop.']',
						'title'     => esc_html__( 'Max discount value', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => esc_html__( 'Set the max discount that can be applied to this product.', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'value'     => $ywpar_max_point_discount,
						'id'        => 'variable_ywpar_max_point_discount',
						'class'     => 'variable_ywpar_max_point_discount',
					),

				);
				$fields = array_merge( $fields, $fixed_fields );
			}else if ( 'percentage' === $conversion_type ) {
				$fields = array_merge( $fields, array(
					'variable_ywpar_redemption_percentage_discount' => array(
						'name'      => 'variable_ywpar_redemption_percentage_discount['.$loop.']',
						'title'     => esc_html__( 'Max discount value', 'yith-woocommerce-points-and-rewards' ),
						'desc'      => esc_html__( 'Set the max percentage discount that can be applied to this product.', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'value'     => $ywpar_redemption_percentage_discount,
						'id'        => 'variable_ywpar_redemption_percentage_discount',
						'class'     => 'variable_ywpar_redemption_percentage_discount',
					),
				) );
			}

			?>

			<div class="options_group yith-plugin-ui">
			<h3 class="ywpar-product-options-title"><?php esc_html_e('Points options', 'yith-woocommerce-points-and-rewards')?></h3>
			<table id="ywpar_product_otions">
				<tbody>
				<?php foreach ( $fields as $field ) : ?>
					<?php
					$data_deps = '';
					if ( isset($field['deps']) ){
						$data_deps = 'data-dep-target="'. $field['id'] .'" data-dep-id="'.$field['deps']['id'].'" data-dep-value="'.$field['deps']['value'].'" data-dep-type="'.$field['deps']['type'].'"';
					}
					?>
					<tr valign="top" class="yith-plugin-fw-panel-wc-row <?php echo esc_html( $field['type'] ); ?> <?php echo esc_html( $field['name'] ); ?> <?php echo isset( $field['class'] ) ? esc_html( $field['class'] ) : ''; ?>" <?php echo $data_deps; ?> >
						<th scope="row" class="titledesc">
							<label for="<?php echo esc_html( $field['id'] ); ?>"><?php echo esc_html( $field['title'] ); ?></label>
						</th>
						<td class="forminp forminp-<?php echo esc_html( $field['type'] ); ?>">
							<?php
							if ( 'variable_ywpar_override_points_date_fields' === $field['id'] ) {
								?>
								<div class="yith-plugin-fw-field-wrapper yith-plugin-fw-text-field-wrapper">
									<input type="text" name="variable_ywpar_point_earned_dates_from[<?php echo esc_attr( $loop ); ?>]" id="variable_ywpar_point_earned_dates_from[<?php echo esc_attr( $loop ); ?>]" value="<?php esc_attr_e( $ywpar_point_earned_dates_from ); ?>" placeholder="<?php echo esc_attr_x( 'From&hellip;', 'placeholder', 'yith-woocommerce-points-and-rewards' ) ?> YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"/>
									<input type="text" name="variable_ywpar_point_earned_dates_to[<?php echo esc_attr( $loop ); ?>]" id="variable_ywpar_point_earned_dates_to[<?php echo esc_attr( $loop ); ?>]" value="<?php esc_attr_e( $ywpar_point_earned_dates_to ); ?>" placeholder="<?php echo esc_attr_x( 'To&hellip;', 'placeholder', 'yith-woocommerce-points-and-rewards' ) ?> YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"/>
								</div>
								<?php
							} elseif ( 'custom-radio' === $field['type'] ) {
								echo '<ul class="ywpar_custom_radios">';
								foreach($field['options']  as $opi => $opl) {
									echo '<li><label>';
									$checked = '';
									if ( $opi === $field['value'] ) {
										$checked = 'checked="checked"';
									}
									echo '<input name="'.$field['name'].'" type=radio value="'.$opi.'" ' . $checked . ' />';
									echo $opl;
									echo '</label></li>';
								}
								echo '</ul>';
							} else {
								yith_plugin_fw_get_field( $field, true );
							}

							if ( 'variable_ywpar_point_earned' === $field['id'] ) {
								echo '<span class="ywpar_type_sign">' . esc_html__( 'points', 'yith-woocommerce-points-and-rewards' ) . '</span>';
							}

							if ( 'variable_ywpar_redemption_percentage_discount' === $field['id'] ) {
								echo '<span class="ywpar_type_sign">%</span>';
							} elseif ( 'variable_ywpar_max_point_discount' === $field['id'] ) {
								echo '<span class="ywpar_type_sign">$</span>';
							}

							if ( isset( $field['desc'] ) && '' !== $field['desc'] ) {
								?>
								<div style="clear: both;"></div>
								<span class="description"><?php echo esc_html( $field['desc'] ); ?></span>
								<?php
							}
							?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			</div>
			<?php
		}

		/**
		 * Save custom fields for variation products
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $variation_id
		 *
		 * @return mixed
		 */
		public function save_custom_fields_for_variation_products( $variation_id ) {
			if ( isset( $_POST['variable_post_id'] ) && ! empty( $_POST['variable_post_id'] ) ) {
				$current_variation_index = array_search( $variation_id, $_POST['variable_post_id'] );
			}

			if ( $current_variation_index === false ) {
				return false;
			}

			$args = array();
			if ( isset( $_POST['variable_ywpar_override_points_earning'][ $current_variation_index ] ) ) {
				$args['_ywpar_override_points_earning'] = $_POST['variable_ywpar_override_points_earning'][ $current_variation_index ];
			} else {
				$args['_ywpar_override_points_earning'] = 'no';
			}

			if ( isset( $_POST['variable_ywpar_fixed_or_percentage'][ $current_variation_index ] ) ) {
				$args['_ywpar_fixed_or_percentage'] = $_POST['variable_ywpar_fixed_or_percentage'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywpar_override_points_date'][ $current_variation_index ] ) ) {
				$args['_ywpar_override_points_date'] = $_POST['variable_ywpar_override_points_date'][ $current_variation_index ];
			} else {
				$args['_ywpar_override_points_date'] = 'no';
			}
			if ( isset( $_POST['variable_ywpar_override_maximum_discount'][ $current_variation_index ] ) ) {
				$args['_ywpar_override_maximum_discount'] = $_POST['variable_ywpar_override_maximum_discount'][ $current_variation_index ];
			} else {
				$args['_ywpar_override_maximum_discount'] = 'no';
			}

			if ( isset( $_POST['variable_ywpar_maximum_discount_type'][ $current_variation_index ] ) ) {
				$args['_ywpar_maximum_discount_type'] = $_POST['variable_ywpar_maximum_discount_type'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywpar_point_earned'][ $current_variation_index ] ) ) {
				$args['_ywpar_point_earned'] = $_POST['variable_ywpar_point_earned'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywpar_point_earned_dates_from'][ $current_variation_index ] ) ) {
				$args['_ywpar_point_earned_dates_from'] = strtotime( $_POST['variable_ywpar_point_earned_dates_from'][ $current_variation_index ] );
			}

			if ( isset( $_POST['variable_ywpar_point_earned_dates_to'][ $current_variation_index ] ) ) {
				$args['_ywpar_point_earned_dates_to'] = strtotime( $_POST['variable_ywpar_point_earned_dates_to'][ $current_variation_index ] );
			}

			if ( isset( $_POST['variable_ywpar_max_point_discount'][ $current_variation_index ] ) ) {
				$args['_ywpar_max_point_discount'] = $_POST['variable_ywpar_max_point_discount'][ $current_variation_index ];
			}

			if ( isset( $_POST['variable_ywpar_redemption_percentage_discount'][ $current_variation_index ] ) ) {
				$args['_ywpar_redemption_percentage_discount'] = $_POST['variable_ywpar_redemption_percentage_discount'][ $current_variation_index ];
			}

			if ( ! empty( $args ) ) {
				$product = wc_get_product( $variation_id );
				yit_save_prop( $product, $args, false, true );
			}
		}

		/**
		 * Add additional fields in product_cat in add form
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  string The premium landing link
		 */
		public function product_cat_add_form_fields() {
			$fields = array(
				'ywpar_override_global' => array(
					'name'  => 'ywpar_override_global_product_category',
					'id'    => 'ywpar_override_global_product_category',
					'title'     => esc_html__( 'Override global points rules for products of this category', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable if you want to override the global rules for the collection of points for all products belonging to this category. Rules applied to this category can be overridden by rules specified for single product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'class'     => 'variable_ywpar_override_points_earning',
					'value' => 'no',
				),
				'ywpar_earning_type'    => array(
					'name'  => 'ywpar_earning_type_product_category',
					'id'    => 'ywpar_earning_type_product_category',
					'title'     => esc_html__( 'For products this category:', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose if exclude the products of this category from the points collection, if set a specific amount of points for all products of this category or if set a % amount of points based on the global amount of points.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'radio',
					'options'   => array(
						'not_assign' => esc_html__( 'Don\'t assign points', 'yith-woocommerce-points-and-rewards' ),
						'fixed'      => esc_html__( 'Set a fixed number of points', 'yith-woocommerce-points-and-rewards' ),
						'percentage' => esc_html__( 'Set a % amount of points based on global points rules', 'yith-woocommerce-points-and-rewards' ),
					),
					'class'     => 'ywpar_earning_type_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
					'value'     => 'fixed'
				),
				'ywpar_points'    => array(
					'name'  => 'point_earned',
					'id'    => 'point_earned',
					'title'     => esc_html__( 'When a user purchases a product of this category, the buyer would get:', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'text',
					'class'     => 'ywpar_points_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
				),
				'ywpar_earning_schedule'    => array(
					'name'  => 'ywpar_earning_schedule_product_category',
					'id'    => 'ywpar_earning_schedule_product_category',
					'title'     => esc_html__( 'Override will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose to schedule the global points overriding or if to start and end it manually.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'radio',
					'options'   => array(
						'manual' => esc_html__( 'From now, until it\'s ended manually', 'yith-woocommerce-points-and-rewards' ),
						'scheduled'      => esc_html__( 'Schedule a start and end date', 'yith-woocommerce-points-and-rewards' ),
					),
					'class'     => 'ywpar_earning_schedule_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
					'value'     => 'manual'
				),

				'ywpar_start_end_date'    => array(
					'name'  => 'ywpar_start_end_date',
					'id'    => 'ywpar_start_end_date',
					'title'     => '',
					'type'      => 'text',
					'class'     => 'ywpar_start_end_date',
					'deps'      => array(
						'id'    => 'ywpar_earning_schedule_product_category',
						'value' => 'scheduled',
						'type'  => 'hide',
					),
				),

				'ywpar_override_global_discount' => array(
					'name'  => 'ywpar_override_global_discount_product_category',
					'id'    => 'ywpar_override_global_discount_product_category',
					'title' => esc_html__( 'Override max discount for products of this category', 'yith-woocommerce-points-and-rewards' ),
					'type'  => 'onoff',
					'class' => 'ywpar_override_global_discount_product_category',
					'value' => 'no'
				),



			);

			if ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'fixed' ) {
				$fixed_fields = array(
					'ywpar_redeem_type'    => array(
						'name'  => 'ywpar_redeem_type_product_category',
						'id'    => 'ywpar_redeem_type_product_category',
						'title'     => esc_html__( 'When the user redeems points, for products of this category:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'radio',
						'options'   => array(
							'no_limit' => esc_html__( 'Set no discount limit', 'yith-woocommerce-points-and-rewards' ),
							'fixed'      => esc_html__( 'Set a fixed max discount value', 'yith-woocommerce-points-and-rewards' ),
							'percentage' => esc_html__( 'Set a % max discount based on global max discount', 'yith-woocommerce-points-and-rewards' ),
						),
						'class'     => 'ywpar_redeem_type_product_category',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
						'value'     => 'fixed'
					),
					'ywpar_max_discount_fixed'    => array(
						'name'  => 'max_point_discount',
						'id'    => 'max_point_discount',
						'title'     => esc_html__( 'For products of this category the user can get a max discount of:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'class'     => 'ywpar_max_point_discount',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),
				);
				$fields = array_merge( $fields, $fixed_fields );
			} else if ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'percentage' ) {
				$percentage_fields = array(
					'ywpar_max_discount_percentage'    => array(
						'name'  => 'redemption_percentage_discount',
						'id'    => 'redemption_percentage_discount',
						'title'     => esc_html__( 'For products of this category the user can get a max percentage discount of:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'class'     => 'ywpar_redemption_percentage_discount',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),
				);
				$fields = array_merge( $fields, $percentage_fields );
			}
			?>
			<div id="ywpar_product_category_settings" class="yith-plugin-ui">
				<h3><?php esc_html_e( 'Points Options', 'yith-woocommerce-points-and-rewards' ); ?></h3>
				<?php
					foreach ( $fields as $field ) {
						if ( isset( $field['deps'] ) ) {
							$deps = 'data-dep-target="' . $field['id'] . '" data-dep-id="' . $field['deps']['id'] . '" data-dep-value="' . $field['deps']['value'] . '" data-dep-type="' . $field['deps']['type'] . '"';
						} else {
							$deps = '';
						}
						?>
						<div class="yith-plugin-fw-panel-wc-row" <?php echo !empty($deps) ? $deps: ''; ?>>
							<div class="form-field <?php echo esc_html( $field['class'] ); ?>">
								<label for="<?php echo esc_html( $field['id'] ); ?>"><?php echo esc_html( $field['title']); ?></label>
								<?php

								if ( 'ywpar_start_end_date' === $field['id'] ) {
									?>
									<div class="yith-plugin-fw-field-wrapper yith-plugin-fw-text-field-wrapper">
										<input type="text" name="point_earned_dates_from" id="ywpar-point-earned-dates-from" value="" size="10" placeholder="<?php echo esc_attr_x( 'From&hellip;', 'placeholder', 'woocommerce' ); ?>  YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" />
										<input type="text" name="point_earned_dates_to" id="ywpar-point-earned-dates-to" value="" size="10" placeholder="<?php echo esc_attr_x( 'To&hellip;', 'placeholder', 'woocommerce' ); ?>  YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" />
									</div>
									<?php
								} else {
									yith_plugin_fw_get_field( $field, true );
								}

								if ( 'point_earned' === $field['id'] ) {
									echo '<span class="ywpar_points_type points">' . esc_html__( 'points', 'yith-woocommerce-points-and-rewards' ) . '</span>';
									echo '<span class="ywpar_points_type percentage">%</span>';
								} else if ( 'max_point_discount' === $field['id'] ) {
									echo '<span class="ywpar_points_type points">'. get_woocommerce_currency_symbol() . '(' . get_woocommerce_currency() . ')</span>';
									echo '<span class="ywpar_points_type percentage">%</span>';
								} else if ( 'redemption_percentage_discount' === $field['id'] ) {
									echo '<span class="ywpar_points_type percentage">%</span>';
								}

								if ( isset( $field['desc'] ) ) {
									?>
										<span class="desc"><?php esc_html_e( $field['desc'] ); ?></span>
									<?php
								}

								?>
							</div>
						</div>
						<?php
					}
				?>

			</div>
			<?php
		}

		/**
		 * Add additional fields in product_cat edit form
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $term
		 *
		 * @return void
		 */
		public function edit_category_fields( $term ) {

			$point_earned = get_term_meta( $term->term_id, 'point_earned', true );

			if ( metadata_exists('term', $term->term_id, 'ywpar_override_global_product_category' ) ) {
				$ywpar_override_global = get_term_meta( $term->term_id, 'ywpar_override_global_product_category', true );
			} else {
				if ( '0' === $point_earned || !empty( $point_earned ) ) {
					$ywpar_override_global = 'yes';
				} else { $ywpar_override_global = 'no'; }
			}

			if ( metadata_exists('term', $term->term_id, 'ywpar_earning_type_product_category' ) ) {
				$ywpar_earning_type = get_term_meta( $term->term_id, 'ywpar_earning_type_product_category', true );
			} else {
				if ( '0' === $point_earned ) {
					$ywpar_earning_type = 'not_assign';
				} elseif ( !empty( $point_earned) && strpos( $point_earned, '%') > 0 ) {
					$ywpar_earning_type = 'percentage';
				} else { $ywpar_earning_type = 'fixed'; }
			}

			$point_earned = str_replace( '%', '', $point_earned);

			/* date check */
			$point_earned_dates_from        = ( $date = get_term_meta( $term->term_id, 'point_earned_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
			$point_earned_dates_to          = ( $date = get_term_meta( $term->term_id, 'point_earned_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';

			if ( metadata_exists('term', $term->term_id, 'ywpar_earning_schedule_product_category' ) ) {
				$override_date = get_term_meta( $term->term_id, 'ywpar_earning_schedule_product_category', true );
			} else {
				if ( !empty( $point_earned_dates_from) && !empty( $point_earned_dates_to ) ) {
					$override_date = 'scheduled';
				} else { $override_date = 'manual'; }
			}

			$max_point_discount             = get_term_meta( $term->term_id, 'max_point_discount', true );
			$redemption_percentage_discount = get_term_meta( $term->term_id, 'redemption_percentage_discount', true );

			if ( metadata_exists('term', $term->term_id, 'ywpar_override_global_discount_product_category' ) ) {
				$ywpar_override_global_discount = get_term_meta( $term->term_id, 'ywpar_override_global_discount_product_category', true );
			} else {
				if ( ('0' === $max_point_discount || !empty( $max_point_discount ) ) || ('0' === $redemption_percentage_discount || !empty( $redemption_percentage_discount ) ) ) {
					$ywpar_override_global_discount = 'yes';
				} else { $ywpar_override_global_discount = 'no'; }
			}

			$fields = array(
				'ywpar_override_global' => array(
					'name'  => 'ywpar_override_global_product_category',
					'id'    => 'ywpar_override_global_product_category',
					'title'     => esc_html__( 'Override global points rules for products of this category', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Enable if you want to override the global rules for the collection of points for all products belonging to this category. Rules applied to this category can be overridden by rules specified for single product.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'onoff',
					'class'     => 'variable_ywpar_override_points_earning',
					'value' => $ywpar_override_global,
				),
				'ywpar_earning_type'    => array(
					'name'  => 'ywpar_earning_type_product_category',
					'id'    => 'ywpar_earning_type_product_category',
					'title'     => esc_html__( 'For products this category:', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose if exclude the products of this category from the points collection, if set a specific amount of points for all products of this category or if set a % amount of points based on the global amount of points.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'radio',
					'options'   => array(
						'not_assign' => esc_html__( 'Don\'t assign points', 'yith-woocommerce-points-and-rewards' ),
						'fixed'      => esc_html__( 'Set a fixed number of points', 'yith-woocommerce-points-and-rewards' ),
						'percentage' => esc_html__( 'Set a % amount of points based on global points rules', 'yith-woocommerce-points-and-rewards' ),
					),
					'class'     => 'ywpar_earning_type_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
					'value'     => $ywpar_earning_type,
				),
				'ywpar_points'    => array(
					'name'  => 'point_earned',
					'id'    => 'point_earned',
					'title'     => esc_html__( 'When a user purchases a product of this category, the buyer would get:', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'text',
					'class'     => 'ywpar_points_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
					'value'     => $point_earned,
				),
				'ywpar_earning_schedule'    => array(
					'name'  => 'ywpar_earning_schedule_product_category',
					'id'    => 'ywpar_earning_schedule_product_category',
					'title'     => esc_html__( 'Override will be valid', 'yith-woocommerce-points-and-rewards' ),
					'desc'      => esc_html__( 'Choose to schedule the global points overriding or if to start and end it manually.', 'yith-woocommerce-points-and-rewards' ),
					'type'      => 'radio',
					'options'   => array(
						'manual'    => esc_html__( 'From now, until it\'s ended manually', 'yith-woocommerce-points-and-rewards' ),
						'scheduled' => esc_html__( 'Schedule a start and end date', 'yith-woocommerce-points-and-rewards' ),
					),
					'class'     => 'ywpar_earning_schedule_product_category',
					'deps'      => array(
						'id'    => 'ywpar_override_global_product_category',
						'value' => 'yes',
						'type'  => 'hide',
					),
					'value'     => $override_date,
				),
				'ywpar_start_end_date'    => array(
					'name'  => 'ywpar_start_end_date',
					'id'    => 'ywpar_start_end_date',
					'title'     => '',
					'type'      => 'text',
					'class'     => 'ywpar_start_end_date',
					'deps'      => array(
						'id'    => 'ywpar_earning_schedule_product_category',
						'value' => 'scheduled',
						'type'  => 'hide',
					),
				),
				'ywpar_override_global_discount' => array(
					'name'  => 'ywpar_override_global_discount_product_category',
					'id'    => 'ywpar_override_global_discount_product_category',
					'title' => esc_html__( 'Override max discount for products of this category', 'yith-woocommerce-points-and-rewards' ),
					'type'  => 'onoff',
					'class' => 'ywpar_override_global_discount_product_category',
					'value' => $ywpar_override_global_discount
				),
			);

			if ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'fixed' ) {
				if ( metadata_exists('term', $term->term_id, 'ywpar_redeem_type_product_category' ) ) {
					$ywpar_redeem_type = get_term_meta( $term->term_id, 'ywpar_redeem_type_product_category', true );
				} else {
					if ( isset($max_point_discount) && $max_point_discount !== '' ) {
						$ywpar_redeem_type = 'fixed';
					} elseif ( $max_point_discount >= 0 && strpos( $max_point_discount, '%') > 0 ) {
						$ywpar_redeem_type = 'percentage';
					} else { $ywpar_redeem_type = 'no_limit'; }
				}

				$fixed_fields = array(
					'ywpar_redeem_type'    => array(
						'name'  => 'ywpar_redeem_type_product_category',
						'id'    => 'ywpar_redeem_type_product_category',
						'title'     => esc_html__( 'When the user redeems points, for products of this category:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'radio',
						'options'   => array(
							'no_limit' => esc_html__( 'Set no discount limit', 'yith-woocommerce-points-and-rewards' ),
							'fixed'      => esc_html__( 'Set a fixed max discount value', 'yith-woocommerce-points-and-rewards' ),
							'percentage' => esc_html__( 'Set a % max discount based on global max discount', 'yith-woocommerce-points-and-rewards' ),
						),
						'class'     => 'ywpar_redeem_type_product_category',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
						'value'     => $ywpar_redeem_type,
					),
					'ywpar_max_discount_fixed'    => array(
						'name'  => 'max_point_discount',
						'id'    => 'max_point_discount',
						'title'     => esc_html__( 'For products of this category the user can get a max discount of:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'class'     => 'ywpar_max_point_discount',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
						'value'     => $max_point_discount,
					),
				);
				$fields = array_merge( $fields, $fixed_fields );
			} else if ( YITH_WC_Points_Rewards()->get_option( 'conversion_rate_method' ) == 'percentage' ) {
				$percentage_fields = array(
					'ywpar_max_discount_percentage'    => array(
						'name'  => 'redemption_percentage_discount',
						'id'    => 'redemption_percentage_discount',
						'title'     => esc_html__( 'For products of this category the user can get a max percentage discount of:', 'yith-woocommerce-points-and-rewards' ),
						'type'      => 'text',
						'class'     => 'ywpar_redemption_percentage_discount',
						'deps'      => array(
							'id'    => 'ywpar_override_global_discount_product_category',
							'value' => 'yes',
							'type'  => 'hide',
						),
					),
				);
				$fields = array_merge( $fields, $percentage_fields );
			}

			?>
			<tr id="ywpar_product_category_settings" class="yith-plugin-ui ywpar_edit_category">
				<th><h3><?php esc_html_e( 'Points Options', 'yith-woocommerce-points-and-rewards' ); ?></h3></th>
				<td>
				<?php
				foreach ( $fields as $field ) {
					if ( isset( $field['deps'] ) ) {
						$deps = 'data-dep-target="' . $field['id'] . '" data-dep-id="' . $field['deps']['id'] . '" data-dep-value="' . $field['deps']['value'] . '" data-dep-type="' . $field['deps']['type'] . '"';
					} else {
						$deps = '';
					}
					?>
					<div class="yith-plugin-fw-panel-wc-row" <?php echo !empty($deps) ? $deps: ''; ?>>
						<div class="form-field <?php echo esc_html( $field['class'] ); ?>">
							<label for="<?php echo esc_html( $field['id'] ); ?>"><?php echo esc_html( $field['title']); ?></label>
							<?php

							if ( 'ywpar_start_end_date' === $field['id'] ) {
								?>
								<div class="yith-plugin-fw-field-wrapper yith-plugin-fw-text-field-wrapper">
									<input type="text" name="point_earned_dates_from" id="ywpar-point-earned-dates-from" value="<?php echo esc_html( $point_earned_dates_from ); ?>" size="10" placeholder="<?php echo esc_attr_x( 'From&hellip;', 'placeholder', 'woocommerce' ); ?>  YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" />
									<input type="text" name="point_earned_dates_to" id="ywpar-point-earned-dates-to" value="<?php echo esc_html( $point_earned_dates_to ); ?>" size="10" placeholder="<?php echo esc_attr_x( 'To&hellip;', 'placeholder', 'woocommerce' ); ?>  YYYY-MM-DD" maxlength="10" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" />
								</div>
								<?php
							} else {
								yith_plugin_fw_get_field( $field, true );
							}

							if ( 'point_earned' === $field['id'] ) {
								echo '<span class="ywpar_points_type points">' . esc_html__( 'points', 'yith-woocommerce-points-and-rewards' ) . '</span>';
								echo '<span class="ywpar_points_type percentage">%</span>';
							} else if ( 'max_point_discount' === $field['id'] ) {
								echo '<span class="ywpar_points_type points">'. get_woocommerce_currency_symbol() . '(' . get_woocommerce_currency() . ')</span>';
								echo '<span class="ywpar_points_type percentage">%</span>';
							} else if ( 'redemption_percentage_discount' === $field['id'] ) {
								echo '<span class="ywpar_points_type percentage">%</span>';
							}

							if ( isset( $field['desc'] ) ) {
								?>
								<span class="desc"><?php esc_html_e( $field['desc'] ); ?></span>
								<?php
							}

							?>
						</div>
					</div>
					<?php
				}
				?>
				</td>
			</tr>
		<?php

		}

		/**
		 * Save custom category fields
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 *
		 * @param $term_id
		 * @param string  $tt_id
		 * @param string  $taxonomy
		 *
		 * @return void
		 */
		public function save_category_fields( $term_id, $tt_id = '', $taxonomy = '' ) {
			if ( 'product_cat' !== $taxonomy ) {
				return;
			}

			if ( isset( $_POST['ywpar_override_global_product_category'] ) ) {
				update_term_meta( $term_id, 'ywpar_override_global_product_category', $_POST['ywpar_override_global_product_category'] );
			} else {
				update_term_meta( $term_id, 'ywpar_override_global_product_category', 'no' );
			}

			if ( isset( $_POST['ywpar_earning_type_product_category'] ) ) {
				update_term_meta( $term_id, 'ywpar_earning_type_product_category', $_POST['ywpar_earning_type_product_category'] );
			}

			if ( isset( $_POST['point_earned'] ) ) {
				update_term_meta( $term_id, 'point_earned', $_POST['point_earned'] );
			}

			if ( isset( $_POST['ywpar_earning_schedule_product_category'] ) ) {
				update_term_meta( $term_id, 'ywpar_earning_schedule_product_category', $_POST['ywpar_earning_schedule_product_category'] );
			}

			if ( isset( $_POST['point_earned_dates_from'] ) ) {
				update_term_meta( $term_id, 'point_earned_dates_from', strtotime( wc_clean( $_POST['point_earned_dates_from'] ) ) );
			}

			if ( isset( $_POST['point_earned_dates_to'] ) ) {
				update_term_meta( $term_id, 'point_earned_dates_to', strtotime( wc_clean( $_POST['point_earned_dates_to'] ) ) );
			}

			if ( isset( $_POST['ywpar_override_global_discount_product_category'] ) ) {
				update_term_meta( $term_id, 'ywpar_override_global_discount_product_category', $_POST['ywpar_override_global_discount_product_category'] );
			} else {
				update_term_meta( $term_id, 'ywpar_override_global_discount_product_category', 'no' );
			}

			if ( isset( $_POST['ywpar_redeem_type_product_category'] ) ) {
				update_term_meta( $term_id, 'ywpar_redeem_type_product_category', $_POST['ywpar_redeem_type_product_category'] );
			}

			if ( isset( $_POST['max_point_discount'] ) ) {
				update_term_meta( $term_id, 'max_point_discount', $_POST['max_point_discount'] );
			}

			if ( isset( $_POST['redemption_percentage_discount'] ) ) {
				update_term_meta( $term_id, 'redemption_percentage_discount', $_POST['redemption_percentage_discount'] );
			}

		}

		/**
		 * Apply Points to Previous Orders
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  string
		 */
		public function apply_previous_order( $from = '' ) {

			$success_count  = 0;
			$offset         = 0;
			$posts_per_page = apply_filters( 'ywpar_apply_previous_order_posts_per_page', 300);

			// APPLY_FILTER : ywpar_previous_orders_statuses: to filter the block loader gif
			do {
				$args = array(
					'post_type'      => 'shop_order',
					'fields'         => 'ids',
					'offset'         => $offset,
					'posts_per_page' => $posts_per_page,
					'post_status'    => apply_filters( 'ywpar_previous_orders_statuses', array( 'wc-processing', 'wc-completed' ) ),
					'meta_query'     => array(
						array(
							'key'     => '_ywpar_points_earned',
							'compare' => 'NOT EXISTS',
						),
					),
				);

				if ( '' !== $from ) {
					$d                  = explode( '-', $from );
					$args['date_query'] = array(
						array(
							'after'     => array(
								'year'  => $d[0],
								'month' => $d[1],
								'day'   => $d[2],
							),
							'inclusive' => true,
						),
					);
				}

				$order_ids = get_posts( $args );

				if ( is_array( $order_ids ) ) {
					foreach ( $order_ids as $order_id ) {
						YITH_WC_Points_Rewards_Earning()->add_order_points( $order_id );

						if ( apply_filters( 'yith_points_rewards_remove_rewards_points', false, $order_id ) ) {

							YITH_WC_Points_Rewards_Redemption()->rewards_order_points( $order_id );
						}
						$success_count ++;
					}
				}

				$offset += $posts_per_page;

			} while ( count( $order_ids ) == $posts_per_page );

			$response = sprintf( _nx( '<strong>%d</strong> order has been updated', '<strong>%d</strong> orders have been updated', $success_count, 'Total number of orders updated.', 'yith-woocommerce-points-and-rewards' ), $success_count );

			return $response;
		}

		/**
		 * Apply Points to Previous Orders
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		public function apply_wc_points_rewards() {

			check_ajax_referer( 'apply_wc_points_rewards', 'security' );

			$from = isset( $_POST['from'] ) ? $_POST['from'] : '';

			$success_count = YITH_WC_Points_Rewards_Porting()->migrate_points( $from );

			$response = sprintf( _nx( '<strong>%d</strong> point has been updated', '<strong>%d</strong> points have been updated', $success_count, '', 'yith-woocommerce-points-and-rewards' ), $success_count );

			wp_send_json( $response );
		}



		/**
		 * Add a widgets to the dashboard.
		 *
		 * This function is hooked into the 'wp_dashboard_setup' action below.
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		function ywpar_points_widgets() {

			if ( current_user_can( 'manage_woocommerce' ) ) {

				wp_add_dashboard_widget(
					'ywpar_points_hit_widget',
					__( 'Best Point Earners', 'yith-woocommerce-points-and-rewards' ),
					array( $this, 'points_hit_widget' )
				);

				wp_add_dashboard_widget(
					'ywpar_points_best_rewards_widget',
					__( 'Best Point Rewards', 'yith-woocommerce-points-and-rewards' ),
					array( $this, 'best_rewards_widget' )
				);
			}
		}

		/**
		 * Print the dashboard widget with the users with best points
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		function points_hit_widget() {

			$users = YITH_WC_Points_Rewards()->user_list_points( apply_filters( 'ywpar_points_hit_widget_items_number', 10 ) );

			if ( ! empty( $users ) ) {
				echo '<table cellpadding="5" class="ywpar_points_hit_widget">';
				foreach ( $users as $user ) {
					echo '<tr>';
					echo '<td width="1">' . get_avatar( $user->ID, '32' ) . '</td>';
					$points      = get_user_meta( $user->ID, '_ywpar_user_total_points', true );
					$history_url = admin_url( 'admin.php?yit_plugin_panel&page=yith_woocommerce_points_and_rewards&tab=customers&action=update&user_id=' . $user->ID );

					echo '<td>' . esc_html( $user->display_name ) . '</td>';
					echo '<td class="points">' . esc_html( $points ) . '</td>';
					echo '<td class="history"><a href="' . esc_url( $history_url ) . '" class="button button-primary"> ' . esc_html( __( 'View History', 'yith-woocommerce-points-and-rewards' ) ) . '</a></td>';
					echo '</tr>';
				}
				echo '</table>';
			} else {
				esc_html_e( 'No users found', 'yith-woocommerce-points-and-rewards' );
			}

		}

		/**
		 * Print the dashboard widget with the users with best discounts
		 *
		 * @since   1.0.0
		 * @author  Emanuela Castorina
		 * @return  void
		 */
		function best_rewards_widget() {

			$users = YITH_WC_Points_Rewards()->user_list_discount( apply_filters( 'ywpar_best_rewards_widget_items_number', 10 ) );

			if ( ! empty( $users ) ) {
				echo '<table cellpadding="5" class="ywpar_points_hit_widget">';
				foreach ( $users as $user ) {
					echo '<tr>';
					echo '<td width="1">' . get_avatar( $user->ID, '32' ) . '</td>';
					$discount    = get_user_meta( $user->ID, '_ywpar_user_total_discount', true );
					$history_url = admin_url( 'admin.php?yit_plugin_panel&page=yith_woocommerce_points_and_rewards&tab=customers&action=update&user_id=' . $user->ID );

					echo '<td>' . esc_html( $user->display_name ) . '</td>';
					echo '<td class="points">' . wp_kses_post( wc_price( $discount ) ) . '</td>';
					echo '<td class="history"><a href="' . esc_url( $history_url ) . '" class="button button-primary"> ' . esc_html__( 'View History', 'yith-woocommerce-points-and-rewards' ) . '</a></td>';
					echo '</tr>';
				}
				echo '</table>';
			} else {
				echo esc_html( __( 'No users found', 'yith-woocommerce-points-and-rewards' ) );
			}

		}

		/**
		 * Action Links
		 *
		 * add the action links to plugin admin page
		 *
		 * @param $links | links plugin array
		 *
		 * @return   mixed Array
		 * @since    1.0.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @return mixed
		 * @use      plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			if ( function_exists( 'yith_add_action_links' ) ) {
				$links = yith_add_action_links( $links, $this->_panel_page, true, YITH_YWPAR_SLUG );
			}

			return $links;
		}

		/**
		 * plugin_row_meta
		 *
		 * add the action links to plugin admin page
		 *
		 * @param $new_row_meta_args
		 * @param $plugin_meta
		 * @param $plugin_file
		 * @param $plugin_data
		 * @param $status
		 * @param $init_file string
		 *
		 * @return   Array
		 * @since    1.0.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 * @use      plugin_row_meta
		 */

		public function plugin_row_meta( $new_row_meta_args, $plugin_meta, $plugin_file, $plugin_data, $status, $init_file = 'YITH_YWPAR_INIT' ) {
			if ( defined( $init_file ) && constant( $init_file ) == $plugin_file ) {
				$new_row_meta_args['slug'] = YITH_YWPAR_SLUG;
			}

			if ( defined( $init_file ) && constant( $init_file ) == $plugin_file ) {
				$new_row_meta_args['is_premium'] = true;
			}

			return $new_row_meta_args;
		}
		/**
		 * Get the premium landing uri
		 *
		 * @since   1.0.0
		 * @author  Andrea Grillo <andrea.grillo@yithemes.com>
		 * @return  string The premium landing link
		 */
		public function get_premium_landing_uri() {
			return defined( 'YITH_REFER_ID' ) ? $this->_premium_landing . '?refer_id=' . YITH_REFER_ID : $this->_premium_landing . '?refer_id=1030585';
		}

		/**
		 * Register plugins for activation tab
		 *
		 * @return void
		 * @since    1.0.0
		 * @author   Andrea Grillo <andrea.grillo@yithemes.com>
		 */
		public function register_plugin_for_activation() {
			if ( ! class_exists( 'YIT_Plugin_Licence' ) ) {
				require_once YITH_YWPAR_DIR . 'plugin-fw/licence/lib/yit-licence.php';
				require_once YITH_YWPAR_DIR . 'plugin-fw/licence/lib/yit-plugin-licence.php';
			}
			YIT_Plugin_Licence()->register( YITH_YWPAR_INIT, YITH_YWPAR_SECRET_KEY, YITH_YWPAR_SLUG );
		}

		/**
		 * Register plugins for update tab
		 *
		 * @return void
		 * @since  1.0.0
		 * @author Andrea Grillo <andrea.grillo@yithemes.com>
		 */
		public function register_plugin_for_updates() {
			if ( ! class_exists( 'YIT_Upgrade' ) ) {
				require_once YITH_YWPAR_DIR . 'plugin-fw/lib/yit-upgrade.php';
			}
			YIT_Upgrade()->register( YITH_YWPAR_SLUG, YITH_YWPAR_INIT );
		}

		/**
		 * Reset points from administrator points
		 *
		 * @return void
		 * @since 1.1.1
		 * @author Emanuela Castorina
		 */
		public function reset_points() {

			check_ajax_referer( 'reset_points', 'security' );
			YITH_WC_Points_Rewards()->reset_points();

			// from 1.1.1
			$response = __( 'Done!', 'yith-woocommerce-points-and-rewards' );

			wp_send_json( $response );
		}

		/**
		 * Import point from csv
		 *
		 * @return void
		 * @author Emanuela Castorina
		 */
		public function actions_from_settings_panel() {

			if ( ! isset( $_REQUEST['page'] ) || 'yith_woocommerce_points_and_rewards' != $_REQUEST['page'] || ! isset( $_REQUEST['ywpar_safe_submit_field'] ) ) {
				return;
			}

			switch ( $_REQUEST['ywpar_safe_submit_field'] ) {
				case 'import_points':
					if ( ! isset( $_FILES['file_import_csv'] ) || ! is_uploaded_file( $_FILES['file_import_csv']['tmp_name'] ) ) {
						return;
					}

					$uploaddir = wp_upload_dir();

					$temp_name = $_FILES['file_import_csv']['tmp_name'];
					$file_name = $_FILES['file_import_csv']['name'];

					if ( ! move_uploaded_file( $temp_name, $uploaddir['basedir'] . '\\' . $file_name ) ) {
						return;
					}

					YITH_WC_Points_Rewards_Porting()->import_from_csv( $uploaddir['basedir'] . '\\' . $file_name, $_REQUEST['delimiter'], $_REQUEST['csv_format'], $_REQUEST['csv_import_action'] );

					break;
				case 'export_points':
					YITH_WC_Points_Rewards_Porting()->export();
					break;

				default:
			}


		}

		/**
		 * Manage Bulk Actions
		 *
		 * @return void
		 * @author Emanuela Castorina
		 */
		public function bulk_action() {

			$result = array(
				'status'  => false,
				'message' => '',
				'step' => 1
			);

			if ( ! isset( $_POST['step'] ) || ! isset( $_POST['form'] ) || ! isset( $_POST['action'] ) || 'ywpar_bulk_action' != $_POST['action'] ) {
				wp_send_json(
					array(
						'status'  => false,
						'error' => __( 'An error occurred during the bulk operation', 'yith-woocommerce-points-and-rewards' ),
					)
				);
			}

			parse_str( $_POST['form'], $datas );

			/* add points to previous orders */
			if ( "add_to_order" === $datas['ywpar_bulk_action_type'] ) {
				$from = '';

				if ( 'from' === $datas['ywpar_apply_points_previous_order_to'] ) {
					$from = $datas['ywpar_apply_points_previous_order'];
				}

				$previous_order_result = $this->apply_previous_order( $from );
				$result['status']  = true;
				$result['message'] = $previous_order_result;
				$result['step'] = 'done';
				wp_send_json( $result );
			}

			$step        = $_POST['step'];
			$users       = ywpar_get_users_for_bulk( $datas, $step );
			if ( $users['users'] ) {
				switch ( $datas['ywpar_bulk_action_type'] ) {
					case 'add_to_user':
					case 'remove':
						$points = (int) $datas['ywpar_bulk_add_points'];
						$description   = $datas['ywpar_bulk_add_description'];
						if ( $points != 0 ) {

							if ( "remove" === $datas['ywpar_bulk_action_type'] && $points > -1 ) {
								$points = -$points;
							}

							foreach ( $users['users'] as $user ) {
								YITH_WC_Points_Rewards()->add_point_to_customer( $user, $points, 'admin_action', $description );
							}
						}
						break;
					case 'ban':
						foreach ( $users['users'] as $user ) {
							YITH_WC_Points_Rewards()->ban_user( $user );
						}
						break;
					case 'unban':
						foreach ( $users['users'] as $user ) {
							YITH_WC_Points_Rewards()->unban_user( $user );
						}
						break;
					case 'reset':
						foreach ( $users['users'] as $user ) {
							YITH_WC_Points_Rewards()->reset_user_points( $user );
						}
						break;
				}
			}

			wp_send_json(
				array(
					'step'       => $users['next_step'],
					'percentage' => $users['percentage'],
					'message' => esc_html__( 'Operation Complete', 'yith-woocommerce-points-and-rewards')
				)
			);
		}

		/**
		 * @param $value
		 *
		 * @return mixed|string
		 */
		public function sanitize_affiliates_earning_conversion_field( $value ) {
			if ( empty( $value ) ) {
				$value = '';
			} else {
				$value = maybe_serialize( $value );
			}

			return $value;
		}

	}
}

/**
 * Unique access to instance of YITH_WC_Points_Rewards_Admin class
 *
 * @return \YITH_WC_Points_Rewards_Admin
 */
function YITH_WC_Points_Rewards_Admin() {
	return YITH_WC_Points_Rewards_Admin::get_instance();
}
