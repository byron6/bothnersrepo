<?php
/**
 * Customer tab
 *
 * Show the list table for customer.
 *
 * @since   1.0.0
 * @author  YITH
 * @package YITH WooCommerce Points and Rewards
 */

/**
 * @var WC_Customer $user
 * @var string      $link
 * @var string      $type
 */


?>
<div id="yith_woocommerce_points_and_rewards_bulk" class="yith-plugin-fw-wp-page-wrapper yith-plugin-fw  yit-admin-panel-container">
	<div class="yit-admin-panel-content-wrap wrap subnav-wrap">
		<h1 class="wp-heading-inline"><?php esc_html_e( 'Customers\' Points', 'yith-woocommerce-points-and-rewards' ); ?>
		<?php
		if ( isset( $_GET['action'] ) && isset( $link ) ) :
			?>
			<a href="<?php echo esc_url( $link ); ?>"
			class="add-new-h2"><?php esc_html_e( '< Back to list', 'yith-woocommerce-points-and-rewards' ); ?></a><?php endif ?>
	</h1>

	<?php
	if ( 'customer' === $type ) :
		$banned_users = (array) YITH_WC_Points_Rewards()->get_option( 'banned_users' );
		$user         = new WC_Customer( $user_id );
		$arg          = remove_query_arg( array( 'paged', 'orderby', 'order' ) );
		$name         = '';
		if ( $user->get_first_name() || $user->get_last_name() ) {
			$name = sprintf( _x( '#%1$d - %2$s %3$s', 'First placeholder: user id; second placeholder: user first name; third placeholder: user last name','yith-woocommerce-points-and-rewards' ), $user->get_id(), $user->get_first_name(), $user->get_last_name() );
		} else {
			$name = sprintf( _x( '#%1$d - %2$s', 'First placeholder: user id; second placeholder: user display name', 'yith-woocommerce-points-and-rewards' ), $user->get_id(), $user->get_display_name() );
		}

		$points = get_user_meta( $user_id, '_ywpar_user_total_points', true );
		?>
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="post-body-content">
					<div class="ywpar_user_info_wrapper">
						<div class="ywpar_user_info">
							<div id="ywpar_user_info">
							<h2><?php echo esc_html( $name ); ?>
								<?php
								if ( in_array( $user_id, $banned_users ) ) : ?>
									<span
										class="ywpar_ban"><?php esc_html_e( 'banned', 'yith-woocommerce-points-and-rewards' ); ?></span>
								<?php endif; ?></h2>
							<?php echo '<span>' . esc_html__( 'Email: ', 'yith-woocommerce-points-and-rewards' ) . '</span><span>' . $user->get_email() . '</span>';
							?>
							<p><a href="
						<?php
								echo esc_url(
									add_query_arg(
										array(
											'action' => 'reset',
											'user'   => $user_id,
										),
										$arg
									)
								)
						?>
							" class="ywpar_update_points ywpar_reset_points button action button button-primary"
									data-username="<?php echo esc_attr( $user->get_display_name() ); ?>"><?php esc_html_e( 'Reset Points', 'yith-woocommerce-points-and-rewards' ); ?></a>
								<?php if ( ! in_array( $user_id, $banned_users ) ) : ?>
									<a href="<?php echo esc_url( add_query_arg( array( 'action' => 'ban' ), $arg ) ); ?>"
										class="ywpar_update_points ywpar_ban_user button button-primary action"
										data-username="<?php echo esc_attr( $user->get_display_name() ); ?>"><?php esc_html_e( 'Ban user', 'yith-woocommerce-points-and-rewards' ); ?></a>
								<?php else : ?>
									<a href="<?php echo esc_url( add_query_arg( array( 'action' => 'unban' ), $arg ) ); ?>"
										class="ywpar_update_points ywpar_unban_user button action"
										data-username="<?php echo esc_attr( $user->get_display_name() ); ?>"><?php esc_html_e( 'Unban the user', 'yith-woocommerce-points-and-rewards' ); ?></a>
								<?php endif; ?></p>
							</div>
							<div id="ywpar_points_collected">
								<div>
									<p class="ywpar_label"><?php  esc_html_e( 'Points collected:', 'yith-woocommerce-points-and-rewards' ); ?></p>
									<p class="ywpar_points"><?php echo $points; ?></p>
								</div>
							</div>
						</div>
						<div class="ywpar_update_point">
							<form method="post" class="ywpar_update_point_form">
								<h2><?php esc_html_e( "Update user's points", 'yith-woocommerce-points-and-rewards' ); ?></h2>
								<div class="ywpar-input-wrapper">
									<p>
										<label for="user_points_action"><?php esc_html_e( 'Action:', 'yith-woocommerce-points-and-rewards' ); ?></label>
										<select id="user_points_action" name="user_points_action">
											<option value="add"><?php esc_attr_e( 'Add', 'yith-woocommerce-points-and-rewards' ); ?></option>
											<option value="remove"><?php esc_attr_e( 'Remove', 'yith-woocommerce-points-and-rewards' ); ?></option>
										</select>
									</p>
									<p>
										<label for="user_points"><?php esc_html_e( 'Points:', 'yith-woocommerce-points-and-rewards' ); ?></label>
										<input type="number" value="" id="user_points" name="user_points" placeholder="0"/>
									</p>
									<p>
										<label for="description"><?php esc_html_e( 'Optional decription:', 'yith-woocommerce-points-and-rewards' ); ?></label>
										<input type="text" class="description" name="description" id="description" />
									</p>
								<input type="hidden" name="action" value="save"/>
								<input type="hidden" name="user_id" value="<?php echo esc_attr( $user_id ); ?>"/>
								<?php wp_nonce_field( 'update_points', 'ywpar_update_points' ); ?>
									<p>
										<input type="submit" class="ywpar_update_points button button-primary action" value="<?php esc_attr_e( 'Update Points', 'yith-woocommerce-points-and-rewards' ); ?>"/>
									</p>
								</div>
							</form>
						</div>
					</div>
					<div class="history-table">
						<div class="meta-box-sortables ui-sortable">
							<h2><?php esc_html_e( 'Points history', 'yith-woocommerce-points-and-rewards' ); ?></h2>

							<?php
							$this->cpt_obj->prepare_items();
							$this->cpt_obj->display();
							?>

						</div>
					</div>
				</div>
				<br class="clear">
			</div>
			<br class="clear">
		</div>
	<?php else : ?>
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">
						<?php $this->cpt_obj->search_box( 'search', 'search_id' ); ?>
						<form method="post">
							<?php
							$this->cpt_obj->prepare_items();
							$this->cpt_obj->display();
							?>
						</form>
					</div>
				</div>
			</div>
			<br class="clear">
		</div>
	<?php endif; ?>
</div>
</div>