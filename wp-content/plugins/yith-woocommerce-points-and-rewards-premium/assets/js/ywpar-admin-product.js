/**
 * admin-product.js
 *
 * @author Your Inspiration Themes
 * @package YITH Infinite Scrolling Premium
 * @version 1.0.0
 */

jQuery( function ( $ ) {
	"use strict";

	$( '#general_product_data #_ywpar_override_points_date' ).on( 'change', function () {
		if ( 'yes' === $( this ).val() ) {
			$( '#general_product_data ._ywpar_override_points_date_fields' ).css( 'display', 'table-row' );
		} else {
			$( '#general_product_data ._ywpar_override_points_date_fields' ).css( 'display', 'none' );
		}

	} );

	$( '#general_product_data #_ywpar_override_points_date' ).change();

	$( '#woocommerce-product-data' ).on(
		'woocommerce_variations_loaded',
		function () {
			// product variation options.
			$( '.woocommerce_variation #ywpar_product_otions #variable_ywpar_override_points_earning' ).change(
				function () {
					if ( 'yes' === $( this ).val() ) {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_fixed_or_percentage' ).css( 'display', 'table-row' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_point_earned' ).css( 'display', 'table-row' );
					} else {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_fixed_or_percentage' ).css( 'display', 'none' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_point_earned' ).css( 'display', 'none' );
					}

				}
			).change()

			$( '.woocommerce_variation #ywpar_product_otions .variable_ywpar_override_points_date input[type=radio]' ).change(
				function () {
					if ( 'yes' === $( this ).val() ) {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_override_points_date_fields' ).css( 'display', 'table-row' );
					} else {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_override_points_date_fields' ).css( 'display', 'none' );
					}

				}
			).change()

			$( '.woocommerce_variation #ywpar_product_otions #variable_ywpar_override_maximum_discount' ).change(
				function () {
					if ( 'yes' === $( this ).val() ) {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_redemption_percentage_discount' ).css( 'display', 'table-row' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_max_point_discount' ).css( 'display', 'table-row' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_maximum_discount_type' ).css( 'display', 'table-row' );
					} else {
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_redemption_percentage_discount' ).css( 'display', 'none' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_max_point_discount' ).css( 'display', 'none' );
						$( this ).closest( '#ywpar_product_otions' ).find( '.variable_ywpar_maximum_discount_type' ).css( 'display', 'none' );
					}

				}
			).change()

			// Earned points percentage or fixed

			$( '.woocommerce_variation #ywpar_product_otions .variable_ywpar_fixed_or_percentage' ).on( 'change', function () {
				var _value = $(this).find('input:checked').val(),
					  pointsEarned_variable = $(this).next('.variable_ywpar_point_earned'),
					  pointsEarnedWrapper_variable = pointsEarned_variable.find('.yith-plugin-fw-field-wrapper');

				if ( 'percentage' === _value ) {
					pointsEarnedWrapper_variable.addClass( 'ywpar-field-with-percentage' );
					pointsEarnedWrapper_variable.next('.ywpar_type_sign').css('display', 'none');
				} else if ( 'fixed' === _value ) {
					pointsEarnedWrapper_variable.removeClass( 'ywpar-field-with-percentage' );
					pointsEarnedWrapper_variable.next('.ywpar_type_sign').css('display', 'inline');
				} else {
					pointsEarnedWrapper_variable.removeClass( 'ywpar-field-with-percentage' );
					pointsEarnedWrapper_variable.next('.ywpar_type_sign').css('display', 'none');
				}
			} ).change();

		} //end function


	);

	// Earned points percentage or fixed
	var pointsEarned        = $( '#_ywpar_point_earned' ),
		pointsEarnedWrapper = pointsEarned.closest( '.yith-plugin-fw-field-wrapper' ),
		fixedOrPercentage   = $( '._ywpar_fixed_or_percentage' );

	fixedOrPercentage.on( 'change', function () {
		var _value = $( this ).find( 'input:checked' ).val();
		if ( 'percentage' === _value ) {
			pointsEarnedWrapper.addClass( 'ywpar-field-with-percentage' );
			pointsEarnedWrapper.next('.ywpar_type_sign').css('display', 'none');
		} else {
			pointsEarnedWrapper.removeClass( 'ywpar-field-with-percentage' );
			pointsEarnedWrapper.next('.ywpar_type_sign').css('display', 'inline');
		}
	} ).change();



	// Tweak: fix rows visibility when changing product type
	var overridePointsEarning = $( '#_ywpar_override_points_earning' );
	$( 'body' ).on( 'woocommerce-product-type-change', function () {
		overridePointsEarning.change();
	} );

} );
