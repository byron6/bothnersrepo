<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$tab = array(
	'customers-tab' => array(
		'customers-tab-options' => array(
			'type'     => 'multi_tab',
			'sub-tabs' => array(
				'customers-tab-customers'    => array(
					'title' => __( 'Customer points', 'yith-woocommerce-points-and-rewards' ),
				),
				'customers-tab-bulk'        => array(
					'title' => __( 'Bulk Actions', 'yith-woocommerce-points-and-rewards' ),
				),
			),
		),
	),
);

return $tab;