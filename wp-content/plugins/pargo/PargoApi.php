<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      PargoApi
 * @package    pargo_plugin
 * @subpackage pargo_plugin/includes
 * @author     Gevann Mullins <gevann.mullins@pargo.co.za>
 */
class PargoApi
{

  /**
   * @param $url
   * @param array $body
   * @param array $headers
   * @return string
   */
  public function postApi($url, $body = array(), $headers = array())
  {


      $response = wp_remote_post(
      $url,
      array(
        'method'      => 'POST',
        'timeout'     => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking'    => true,
        'headers'     => $headers,
        'body'        => $body,
        'cookies'     => array()
      )
    );

    if ( is_wp_error( $response ) ) {
      $error_message = $response->get_error_message();
      return "Something went wrong: $error_message";
    } else {
       return $response;
    }

  }


  /**
   * @return mixed
   */
  public function AuthToken($isDev=false)
  {
    if ((isset($_SESSION['access_token'])) && (time() < $_SESSION['access_token_expiry'])) {
      return $_SESSION['access_token'];
    }

    $apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
    $lastChar = $apiUrl[strlen($apiUrl)-1];
    if ($lastChar !== '/') {
      $apiUrl = $apiUrl . '/';
    }


    /**
     * Get the pargo api url
     */
    $url = $apiUrl . 'auth';

    /**
     * Get the pargo username and password
     */
    $data = array(
      'username' =>  get_option('woocommerce_wp_pargo_settings')['pargo_username'],
      'password' => get_option('woocommerce_wp_pargo_settings')['pargo_password']
    );
    $headers = array();

    /**
     * Get Auth data from the API
     */
    $returnData = $this->postApi($url, $data, $headers);

    /**
     * Check if an error is returned else return the authToken
     */
    if ( is_wp_error( $returnData ) ) {

      $error_message = $returnData->get_error_message();
      return "Something went wrong: $error_message";

    } else {

      $response = wp_remote_retrieve_body( $returnData );

      $accessToken = json_decode($response)->access_token;
      $expiresIn = json_decode($response)->expires_in;
      $_SESSION['access_token'] = $accessToken;
      $_SESSION['access_token_expiry'] = $expiresIn;

      if($isDev){
        return $returnData;
      } else {
        return $accessToken;
      }

    }

  }

  public function getAuthToken($isDev=false)
  {
    if ((isset($_SESSION['access_token'])) && (time() < $_SESSION['access_token_expiry'])) {
      return $_SESSION['access_token'];
    }

    $apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
    $lastChar = $apiUrl[strlen($apiUrl)-1];
    if ($lastChar !== '/') {
      $apiUrl = $apiUrl . '/';
    }


    /**
     * Get the pargo api url
     */
    $url = $apiUrl . 'auth';

    /**
     * Get the pargo username and password
     */
    $data = array(
      'username' =>  get_option('woocommerce_wp_pargo_settings')['pargo_username'],
      'password' => get_option('woocommerce_wp_pargo_settings')['pargo_password']
    );
    $headers = array();

    /**
     * Get Auth data from the API
     */
    $returnData = $this->postApi($url, $data, $headers);

    /**
     * Check if an error is returned else return the authToken
     */
    if ( is_wp_error( $returnData ) ) {

      $error_message = $returnData->get_error_message();
      return "Something went wrong: $error_message";

    } else {

      $response = wp_remote_retrieve_body( $returnData );

      $accessToken = json_decode($response)->access_token;
      $expiresIn = json_decode($response)->expires_in;
      $_SESSION['access_token'] = $accessToken;
      $_SESSION['access_token_expiry'] = $expiresIn;

      if($isDev){
        return $returnData;
      } else {
        return $accessToken;
      }

    }

  }


  /**
   * @return mixed
   */
  public function monitorAuthToken($url, $postData=array(), $headers=array())
  {

    /**
     * Get Auth data from the API
     */
    $returnData = $this->postApi($url, $postData, $headers);

    /**
     * Check if an error is returned else return the authToken
     */
    if ( is_wp_error( $returnData ) ) {

      $error_message = $returnData->get_error_message();
      return "Something went wrong: $error_message";

    } else {

      $response = wp_remote_retrieve_body($returnData);
      $accessToken = json_decode($response)->access_token;
      return $accessToken;

    }

  }


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    public function getFormattedUrl()
    {
        $apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
        $lastChar = $apiUrl[strlen($apiUrl) - 1];
        if ($lastChar !== '/') {
            return $apiUrl . '/';
        }
        return $apiUrl;

    }


    /**
     * @param $username
     * @param $password
     *
     * @return mixed|string, [token_type, expires_in, access_token, refresh_token]
     */
    public function authUserCredentials($username, $password)
    {
        $url = $this->getFormattedUrl();
        $authUrl = $url . "auth";

        $headers = [];

        $credentials = [
            "username" => $username,
            "password" => $password
        ];

        $args = [
            'method' => 'POST',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'blocking' => true,
            'headers' => $headers,
            'body' => $credentials,
            'cookies' => array()
        ];

        $response = wp_remote_post($authUrl, $args);

        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();

            return "Something went wrong: $error_message";
        }

        $response = wp_remote_retrieve_body($response);

        return json_decode($response);

    }


    public function postOrder($order)
    {

        $pargoApi = new PargoApi();

        $data = array(
            "data" => array(
                "type" => "W2P",
                "version" => 1,
                "attributes" => array(
                    "warehouseAddressCode" => null,
                    "returnAddressCode" => null,
                    "trackingCode" => null,
                    "externalReference" => (string)$order->get_id(),
                    "pickupPointCode" => $_SESSION['pargo_shipping_address']['pargoPointCode'],

                    "consignee" => array(
                        "firstName" => $order->get_data()['billing']['first_name'],
                        "lastName" => $order->get_data()['billing']['last_name'],
                        "email" => $order->get_data()['billing']['email'],
                        "phoneNumbers" => array(
                            $order->get_data()['billing']['phone'],
                        ),
                        "address1" => $order->get_data()['billing']['address_1'],
                        "address2" => $order->get_data()['billing']['address_2'],
                        "suburb" => "",
                        "postalCode" => $order->get_data()['billing']['postcode'],
                        "city" => $order->get_data()['billing']['city'],
                        "country" => "ZA"
                    ),
                ),
            ),
        );
        $data = json_encode($data);

        /**
         * Check to see if trailing slash is added else add it
         */
        $apiUrl = get_option('woocommerce_wp_pargo_settings')['pargo_url'];
        $lastChar = $apiUrl[strlen($apiUrl) - 1];
        if ($lastChar !== '/') {
            $apiUrl = $apiUrl . '/';
        }

        $url = $apiUrl . 'orders';

        /**
         * Get an Authentication token
         */
        $accessToken = $pargoApi->getAuthToken();

        $headers = array(
            "Authorization" => "Bearer $accessToken",
            "Content-Type" => "application/json",
            "cache-control" => "no-cache"
        );

        $responseData = $pargoApi->postApi($url, $data, $headers);

        if (is_wp_error($responseData)) {

            $error_message = $responseData->get_error_message();
            return "Something went wrong: $error_message";

        } else {

            $response = wp_remote_retrieve_body($responseData);
            $labelUrl = json_decode($response)->data->attributes->orderData->orderLabel;
            update_post_meta($order->get_id(), 'pargo_waybill', ['waybill' => json_decode($response)->data->attributes->orderData->trackingCode, 'label' => $labelUrl]);
            $my_value = get_post_meta($order->get_id(), 'pargo_waybills', true);

            if (json_decode($response)->success == false) {
                $note = __("Pargo Shipment failed " . json_decode($response)->errors[0]->detail);
                $order->add_order_note($note);
                $order->save();
            } else {
                $note = __("Pargo Shipment processed.");
                $order->add_order_note($note);
                $order->save();
//        print_r($order);
            }

            $url = $_SERVER['REQUEST_URI'];
            $parsed = parse_url($url);
            $path = $parsed['path'];
            unset($_GET['ship-now']);


            return $response;
        }

    }


}
