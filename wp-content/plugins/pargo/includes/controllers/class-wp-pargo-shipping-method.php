<?php


class Wp_Pargo_Shipping_Method extends WC_Shipping_Method
{

	/**
	 * Constructor for your shipping class
	 *
	 * @access public
	 * @return void
	 */
	public function __construct($instance_id = 0)
	{
//		parent::__construct();
		//$this->instance_id = 0;
		$this->id                 = 'wp_pargo';
		$this->method_title       = __('Pargo', 'woocommerce');
		$this->method_description = __('Shipping Method for Pargo', 'woocommerce');
		// Availability & Countries
		$this->availability = 'including';
		$this->countries = array(
			'ZA', //South Africa
		);
		//Woocommerce 3 support
		$this->instance_id = absint($instance_id);
		$this->enabled = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes';
		$this->title = isset($this->settings['title']) ? $this->settings['title'] : __('Pargo', 'woocommerce');

		$this->supports  = array(
			'shipping-zones',
			'instance-settings',
			'instance-settings-modal',
			'settings'
		);

		$this->init_instance_settings();
		$this->init();
	}

	/**
	 * Init your settings
	 *
	 * @access public
	 * @return void
	 */
	public function init()
	{
		// Load the settings API
		$this->init_form_fields();
		$this->init_settings();

		// Save settings in admin if you have any defined
		add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
	}

	/**
	 * Define settings field for Pargo shipping
	 * @return void
	 */
	public function init_form_fields()
	{
		$this->form_fields = array(
			'pargo_enabled' => array(
				'title'       => __('Enable/Disable', 'woocommerce'),
				'type'            => 'checkbox',
				'label'       => __('Enable Pargo', 'woocommerce'),
				'default'         => 'yes'
			),
			'pargo_description' => array(
				'title'       => __('Method Description', 'woocommerce'),
				'type'            => 'text',
				'description'     => __('This controls the description next to the Pargo delivery method.', 'woocommerce'),
				'default'     => __('Never miss a delivery again! Choose your most convenient Pargo pickup point', 'woocommerce'),
			),
			'pargo_use_api' => array(
				'title'      => __('Use backend shipment', 'woocommerce'),
				'type'          => 'checkbox',
			),
			'pargo_auto_select' => array(
				'title'       => __('Auto Select', 'woocommerce'),
				'type'            => 'checkbox',
				'description'     => __('This option will display the nearest 3 pickup points to your customer.', 'woocommerce'),
				'label'       => __('Auto select nearest Pups', 'woocommerce'),
				'default'         => 'yes'
			),
//			'pargo_url' => array(
//				'title'       => __('Pargo API url', 'woocommerce'),
//				'type'            => 'text',
//				'description'     => __('This is your Pargo API URL.', 'woocommerce'),
//			),
//			'pargo_username' => array(
//				'title'       => __('Pargo Username', 'woocommerce'),
//				'type'            => 'text',
//				'description'     => __('This is your unique username from Pargo.', 'woocommerce'),
//			),
//			'pargo_password' => array(
//				'title'       => __('Pargo Password', 'woocommerce'),
//				'type'            => 'text',
//				'description'     => __('This is you unique password from Pargo.', 'woocommerce'),
//			),
			/*'pargo_auth_token' => array(
								'title'       => __('Pargo Auth Token', 'woocommerce'),
								'type'            => 'text',
								'description'     => __('This is you unique auth token from Pargo.', 'woocommerce'),
								'default'     => __('abcdefghijklmnopqrstuvwxyz', 'woocommerce'),
							),*/
//			'pargo_map_token' => array(
//				'title'       => __('Pargo Map Token', 'woocommerce'),
//				'type'            => 'text',
//				'description'     => __('This is you unique map token from Pargo.', 'woocommerce'),
//				'default'     => __('abcdefghijklmnopqrstuvwxyz', 'woocommerce'),
//			),
			'pargo_buttoncaption' => array(
				'title'       => __('Pickup Button Caption Before Pickup Point Selection', 'woocommerce'),
				'type'            => 'text',
				'description'     => __('Sets the caption of the button that allows users to choose a Pargo pickup point.', 'woocommerce'),
				'default'     => __('Select a pick up point', 'woocommerce'),
			),
			'pargo_buttoncaption_after' => array(
				'title'       => __('Pickup Button Caption After Pickup Point Selection', 'woocommerce'),
				'type'            => 'text',
				'description'     => __('Sets the caption of the button after a user has selected a Pargo pickup point.', 'woocommerce'),
				'default'     => __('Re-select a pick up point', 'woocommerce'),
			),
			'pargo_style_button' => array(
				'title'       => __('Pickup Button Style', 'woocommerce'),
				'type'          => 'textarea',
				'description'       => __('Sets the style of the button pressed to select a pickup point.', 'woocommerce'),
				'default'         => ''
			),
			'enable_free_shipping' => array(
				'title'      => __('Enable free shipping', 'woocommerce'),
				'type'          => 'checkbox',
			),
			'free_shipping_amount' => array(
				'title'       => __('Set the minimum amount for free shipping', 'woocommerce'),
				'type'          => 'number',
			),
			'pargo_style_title' => array(
				'title'       => __('Pargo Point Title Style', 'woocommerce'),
				'type'          => 'textarea',
				'description'       => __('Set the style of the selected Pargo Point title.', 'woocommerce'),
				'default'         => 'font-size: 16px;font-weight:bold;margin-bottom:0px;margin-top:0px;max-width:250px;'
			),
			'pargo_style_desc' => array(
				'title'       => __('Pargo Point Description Style', 'woocommerce'),
				'type'          => 'textarea',
				'description'       => __('Set the style of the selected Pargo Point line items.', 'woocommerce'),
				'default'         => 'font-size:12px;margin-bottom:0px;margin-top:0px;max-width:250px;'
			),
			'pargo_style_image' => array(
				'title'       => __('Pargo Point Image Style', 'woocommerce'),
				'type'          => 'textarea',
				'description'       => __('Set the style of the selected Pargo Point image', 'woocommerce'),
				'default'         => 'max-width:250px;border:1px solid #EBEBEB;border-radius:2px;'
			),
			'weight' => array(
				'title' => __('Weight (kg)', 'woocommerce'),
				'type' => 'number',
				'description' => __('Maximum allowed weight per item to use for Pargo delivery', 'woocommerce'),
				'id' => 'weight',
				'default' => 15
			),
			'pargo_cost_5' => array(
				'title'       => __('5kg Shipping Cost', 'woocommerce'),
				'type'            => 'number',
				'description'     => __('This controls the cost of Pargo delivery for 0-5kg items.', 'woocommerce'),
				'default'     => __('75', 'woocommerce')
			),
			'pargo_cost_10' => array(
				'title'       => __('10kg Shipping Cost', 'woocommerce'),
				'type'            => 'number',
				'description'     => __('This controls the cost of Pargo delivery for 5-10kg items.', 'woocommerce'),
				'default'     => __('', 'woocommerce')
			),
			'pargo_cost_15' => array(
				'title'       => __('15kg Shipping Cost', 'woocommerce'),
				'type'            => 'number',
				'description'     => __('This controls the cost of Pargo delivery for 10-15kg items.', 'woocommerce'),
				'default'     => __('', 'woocommerce')
			),
			'pargo_cost' => array(
				'title'       => __('No weight Shipping Cost', 'woocommerce'),
				'type'            => 'number',
				'description'     => __('This controls the cost of Pargo delivery without product weight settings.', 'woocommerce'),
				'default'     => __('', 'woocommerce')
			),
			'pargo_map_display' => array(
				'title'       => __('Pargo map display as static widget', 'woocommerce'),
				'type'            => 'checkbox',
				'description'     => __('Display map as a modal or static widget', 'woocommerce'),
				'default'     => __('no', 'woocommerce')
			)

		);
	}

	/**
	 * calculate_shipping function.
	 * WC_Shipping_Method::get_option('pargo_cost_10');
	 * @access public
	 * @param mixed $package
	 * @return void
	 */
	public function getPargoSettings()
	{
		$pargosetting['pargo_description'] = WC_Shipping_Method::get_option('pargo_description');
		$pargosetting['pargo_map_token'] = WC_Shipping_Method::get_option('pargo_map_token');
		$pargosetting['pargo_buttoncaption'] = WC_Shipping_Method::get_option('pargo_buttoncaption');
		$pargosetting['pargo_buttoncaption_after'] = WC_Shipping_Method::get_option('pargo_buttoncaption_after');
		$pargosetting['pargo_style_button'] = WC_Shipping_Method::get_option('pargo_style_button');

		$pargosetting['pargo_style_title'] = WC_Shipping_Method::get_option('pargo_style_title');
		$pargosetting['pargo_style_desc'] = WC_Shipping_Method::get_option('pargo_style_desc');
		$pargosetting['pargo_style_image'] = WC_Shipping_Method::get_option('pargo_style_image');

		return $pargosetting;
	}

	public function calculate_shipping($package = array())
	{

		$weight = 0;
		$cost = 0;
		$country = $package["destination"]["country"];

		foreach ($package['contents'] as $item_id => $values) {
			$_product = $values['data'];
			if ($_product->get_weight() == '' || $_product->get_weight() == null) {
				$weight = 0;
			} else {
				$weight = $weight + $_product->get_weight() * $values['quantity'];
			}
		}

		$weight = wc_get_weight($weight, 'kg');


		if ($weight <= 0) {
			$cost = WC_Shipping_Method::get_option('pargo_cost');
		}
		//change of logic from $weight <=1
		elseif (($weight > 0) && ($weight <= 5)) {

			$cost = WC_Shipping_Method::get_option('pargo_cost_5');
		} elseif ($weight > 5 && $weight <= 10) {

			$cost = WC_Shipping_Method::get_option('pargo_cost_10');
		} elseif ($weight > 10 && $weight <= 15) {

			$cost = WC_Shipping_Method::get_option('pargo_cost_15');
		} elseif ($weight > 15) {

			//$cost=($this->pargocost15*$weight)/15;

			if (WC()->cart->get_cart_contents_count() > 0) {
				$numitems = WC()->cart->get_cart_contents_count();
			}

			$costfor5 = WC_Shipping_Method::get_option('pargo_cost_5');
			$costfor10 = WC_Shipping_Method::get_option('pargo_cost_10');
			$costfor15 = WC_Shipping_Method::get_option('pargo_cost_15');

			//the calculus
			//global $pargopackages;

			$firstadd = 0;
			$secondadd = 0;
			//$pargopackages=0;

			$a = (int) ($weight / 5);
			$b = (int) ($weight / 10);
			$c = (int) ($weight / 15);

			//$pargopackages= min($a, $b ,$c);

			//$a is the smallest - 5
			if (($a <= $b) && ($a <= $c)) {
				$firstadd = $a * $costfor5;
				$d = $weight % 5;
			}

			//$b is the smallest - 10
			if (($b <= $a) && ($b <= $c)) {
				$firstadd = $b * $costfor10;
				$d = $weight % 10;
			}

			//$c is the smallest - 15
			if (($c <= $a) && ($c <= $b)) {
				$firstadd = $c * $costfor15;
				$d = $weight % 15;
			}

			//second

			if (($d <= 5) && ($d > 0)) {
				$secondadd = $costfor5;
				//$pargopackages=$pargopackages+1;
			}

			if (($d > 5) && ($d <= 10) && ($d > 0)) {
				$secondadd = $costfor10;
				//$pargopackages=$pargopackages+1;
			}

			if (($d > 10) && ($d <= 15) && ($d > 0)) {
				$secondadd = $costfor15;
				//$pargopackages=$pargopackages+1;
			}

			$cost = $firstadd + $secondadd;

		}

		$rate = array(
			'id' => $this->id,
			'label' => $this->title . ': ' . WC_Shipping_Method::get_option('pargo_description'),
			'cost' => $cost,
			'calc_tax' => 'per_item'
		);

		if (WC_Shipping_Method::get_option('enable_free_shipping') == 'yes') {
			global $woocommerce;
			$total_cart_amount = (int) WC()->cart->cart_contents_total;

			if ($total_cart_amount >= WC_Shipping_Method::get_option('free_shipping_amount')) {
				$rate['cost'] = 0;
				$rate['label'] = $this->title . ': Free';
			}
		}

		// Register the rate
		$this->add_rate($rate);

	}

}




