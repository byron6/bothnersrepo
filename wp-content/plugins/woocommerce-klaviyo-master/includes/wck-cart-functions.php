<?php
/**
 * WooCommerceKlaviyo Order Functions
 *
 * Functions for order specific things.
 *
 * @author    Klaviyo
 * @category  Core
 * @package   WooCommerceKlaviyo/Functions
 * @version   2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * Insert tracking code code for tracking started checkout.
 *
 * @access public
 * @return void
 */
function wck_insert_checkout_tracking($checkout) {

  global $current_user;
  wp_reset_query();

  wp_get_current_user();

  $cart = WC()->cart;
  $event_data = array(
    '$service' => 'woocommerce',
    'CurrencySymbol' => get_woocommerce_currency_symbol(),
    'Currency' => get_woocommerce_currency(),
    '$value' => $cart->total,
    '$extra' => array(
      'Items' => array(),
      'SubTotal' => $cart->subtotal,
      'ShippingTotal' => $cart->shipping_total,
      'TaxTotal' => $cart->tax_total,
      'GrandTotal' => $cart->total
    )
  );
  $allcategories = array();

  foreach ( $cart->get_cart() as $cart_item_key => $values ) {
    $product = $values['data'];
    $parent_product_id = $product->get_parent_id();

    if ($product->get_parent_id() == 0 ) {
      $parent_product_id = $product->get_id();
    }
    $categories_array = get_the_terms( $parent_product_id, 'product_cat' );
    $categories = wp_list_pluck( $categories_array, 'name' );

    if (is_array($categories)) {
      foreach($categories as $category) {
        array_push($allcategories, $category);
      }
    }

    $event_data['$extra']['Items'] []= array(
      'Quantity' => $values['quantity'],
      'ProductID' => $parent_product_id,
      'VariantID' => $product->get_id(),
      'Name' => $product->get_name(),
      'URL' => $product->get_permalink(),
      'Images' => array(
        array(
          'URL' => wp_get_attachment_url(get_post_thumbnail_id($product->get_id()))
        )
      ),
      'Categories' => $categories,
      'Variation' => $values['variation'],
      'SubTotal' => $values['line_subtotal'],
      'Total' => $values['line_subtotal_tax'],
      'LineTotal' => $values['line_total'],
      'Tax' => $values['line_tax'],
      'TotalWithTax' => $values['line_total'] + $values['line_tax']

    );
    $allcategories = array_unique($allcategories);
    $event_data['Categories'] = $allcategories;
  }

  if ( empty($event_data['$extra']['Items']) ) {
    return;
  }

  echo "\n" . '<!-- Start Klaviyo for WooCommerce // Plugin Version: ' . WooCommerceKlaviyo::getVersion() . ' -->' . "\n";
  echo '<script type="text/javascript">' . "\n";
  echo 'var _learnq = _learnq || [];' . "\n";

  echo 'var WCK = WCK || {};' . "\n";
  echo 'WCK.trackStartedCheckout = function () {' . "\n";
  echo '  _learnq.push(["track", "$started_checkout", ' . json_encode($event_data) . ']);' . "\n";
  echo '};' . "\n\n";
  if ($current_user->user_email) {
    echo '_learnq.push(["identify", {' . "\n";
    echo '  $email : "' . $current_user->user_email . '"' . "\n";
    echo '}]);' . "\n\n";
    echo 'WCK.trackStartedCheckout();' . "\n\n";
  } else {
    // See if current user is a commenter
    $commenter = wp_get_current_commenter();
    if ($commenter['comment_author_email']) {
      echo '_learnq.push(["identify", {' . "\n";
      echo '  $email : "' . $commenter['comment_author_email'] . '"' . "\n";
      echo '}]);' . "\n\n";
      echo 'WCK.trackStartedCheckout();' . "\n\n";
    }
  }

  echo 'if (jQuery) {' . "\n";
  echo '  jQuery(\'input[name="billing_email"]\').change(function () {' . "\n";
  echo '    var elem = jQuery(this),' . "\n";
  echo '        email = jQuery.trim(elem.val());' . "\n\n";

  echo '    if (email && /@/.test(email)) {' . "\n";
  echo '      var params = {$email : email}' . "\n";
  echo '    console.log(params)' . "\n";
  echo '      var first_name = jQuery(\'input[name="billing_first_name"]\').val();' . "\n";
  echo '      var last_name = jQuery(\'input[name="billing_last_name"]\').val();' . "\n";
  echo '      if (first_name) {' . "\n";
  echo '        params["first_name"] = first_name' . "\n";
  echo '      }' . "\n";
  echo '      if (last_name) {' . "\n";
  echo '        params["last_name"] = last_name' . "\n";
  echo '      }' . "\n";
  echo '    console.log(params)' . "\n";

  echo '      _learnq.push(["identify", params]);' . "\n";
  echo '      WCK.trackStartedCheckout();' . "\n";
  echo '    }' . "\n";
  echo '  })' . "\n";
  
  echo '  jQuery(\'input[name="billing_first_name"]\').change(function () {' . "\n";
  echo '    var email = jQuery(\'input[name="billing_first_name"]\').val();' . "\n";
  echo '    if (email){' . "\n";
  echo '    var elem = jQuery(this),' . "\n";
  echo '       first_name = jQuery.trim(elem.val());' . "\n\n";
  echo '      _learnq.push(["identify", {"first_name" : first_name}]);' . "\n";
  echo '    }' . "\n";
  echo '  })' . "\n";
  
  echo '  jQuery(\'input[name="billing_last_name"]\').change(function () {' . "\n";
  echo '    var email = jQuery(\'input[name="billing_email"]\').val();' . "\n";
  echo '    if (email){' . "\n";
  echo '       var elem = jQuery(this),' . "\n";
  echo '       last_name = jQuery.trim(elem.val());' . "\n\n";
  echo '       _learnq.push(["identify", {"last_name" : last_name}]);' . "\n";
  echo '    }' . "\n";
  echo '  })' . "\n";
  echo '}' . "\n";

  echo '</script>' . "\n";
  echo '<!-- end: Klaviyo Code. -->' . "\n";

}


add_action( 'woocommerce_after_checkout_form', 'wck_insert_checkout_tracking' );


function kl_checkbox_custom_checkout_field( $checkout ) {
    $klaviyo_settings = get_option('klaviyo_settings');
    woocommerce_form_field( 'kl_newsletter_checkbox', array(
    'type'          => 'checkbox',
    'class'         => array('kl_newsletter_checkbox_field'),
    'label'         => $klaviyo_settings['klaviyo_newsletter_text'],
    'value'  => true,
    'default' => 0,
    'required'  => false,
    ), $checkout->get_value( 'kl_newsletter_checkbox' ));
}


function kl_add_to_list( $order_id ) {
    $klaviyo_settings = get_option('klaviyo_settings');
    if ($_POST['kl_newsletter_checkbox']){
          $email = $_POST['billing_email'];
          $url = 'https://manage.kmail-lists.com/subscriptions/external/subscribe';
          $response = wp_remote_post( $url, array(
              'method'      => 'POST',
              'httpversion' => '1.0',
              'blocking'    => false,
              'body'        => array(
                  'g' => $klaviyo_settings['klaviyo_newsletter_list'],
                  'email' => $email,
                  '$fields' => '$consent,$source',
                  '$consent' => ['email'],
                  '$source' => 'Accepted at Checkout'
              )
            )
          );
          update_post_meta( $order_id, 'klaviyo-response', $response );
        }
}

$klaviyo_settings = get_option('klaviyo_settings');
if ($klaviyo_settings['klaviyo_newsletter_list_id'] != '') {

// Add the checkbox field
add_action('woocommerce_after_checkout_billing_form', 'kl_checkbox_custom_checkout_field');

// Post list request to Klaviyo
add_action('woocommerce_checkout_update_order_meta', 'kl_add_to_list');

}
