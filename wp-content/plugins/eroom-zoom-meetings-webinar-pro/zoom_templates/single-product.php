<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.

	return;
}

global $product;
$stock      = (int)$product->get_stock_quantity();
$price_html = $product->get_price_html();

$image_array = wp_get_attachment_image_src( $product->get_image_id(), 'large' );
$image_url   = ( ( $image_array ) ? esc_url( $image_array[0] ) : null );

$post_id        = (int) get_the_ID();
$meeting_id     = (int) get_post_meta( $post_id, '_meeting_id', true );

$zoom_post_type = get_post_type( $meeting_id );
$zoom_data = get_post_meta( $meeting_id, 'stm_zoom_data', true );
$password = get_post_meta( $meeting_id, 'stm_password', true );

$meeting_data   = StmZoom::meeting_time_data( $meeting_id );
$meeting_over     = get_post_meta( $post_id, '_meeting_over', true );
$meeting_record     = get_post_meta( $post_id, '_meeting_record', true );

$is_purchased     = stm_zoom_has_bought_items( array( $post_id ) );
$purchased_status = stm_zoom_has_bought_items( array( $post_id ) , true );

$purchased_class  = ( $is_purchased ) ? 'purchased' : null;
$is_started       = false;

if ( ! empty( $meeting_data ) && isset( $meeting_data['is_started'] ) ) {
	$is_started = $meeting_data['is_started'];
	$classes    = $meeting_data['is_started'] ? 'meeting_started ' : 'meeting_not_started ';
	$classes    .= $is_purchased ? 'purchased ' : 'not_purchased ';
}

$date        = get_post_meta( $meeting_id, 'stm_date', true );
$date        = ! empty( $date ) ? strtotime( 'today', ( apply_filters( 'eroom_sanitize_stm_date', $date ) / 1000 ) ) : $date;
$time        = get_post_meta( $meeting_id, 'stm_time', true );
$timezone    = get_post_meta( $meeting_id, 'stm_timezone', true );
$time_format = get_option( 'time_format', 'H:i' );

$is_started_class = $is_started ? 'stm-eroom-wcproduct__started': 'stm-eroom-wcproduct__not_started';

$is_empty_meeting_data = 'stm-eroom-wcproduct__hide';
if ( $product->has_file() || ( ! empty( $meeting_over ) && ! empty( $meeting_record ) ) ) {
	$is_empty_meeting_data = '';
}

get_header( 'shop' ); ?>
<?php while ( have_posts() ) : ?>
    <?php the_post(); ?>
  <div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'stm-eroom-wcproduct', $product ); ?>>
    <div class="curved_box <?php echo esc_attr__($is_started_class).' '.esc_attr__($is_empty_meeting_data);?>" >
      <div class="image_row image_row__bg" style="background-image: url('<?php echo esc_url( $image_url ); ?>');">
          <?php include( STM_ZOOM_PRO_PATH . '/zoom_templates/parts/wc-banner.php' ); ?>
      </div>
      <div class="info_row">
        <div class="meeting-info-row">
          <div class="meeting_countdown">
              <?php include( STM_ZOOM_PRO_PATH . '/zoom_templates/parts/wc-countdown.php' ); ?>
          </div>
          <div class="meeting_data">
              <?php include( STM_ZOOM_PRO_PATH . '/zoom_templates/parts/wc-data.php' ); ?>
          </div>
          <div class="meeting_get_access <?php echo esc_attr( $purchased_class ) ?>">
              <?php include( STM_ZOOM_PRO_PATH . '/zoom_templates/parts/wc-elements.php' ); ?>
          </div>
        </div>
      </div>
      <?php if ( $is_purchased):?>
        <?php if ( !$is_started): ?>
        <div class="stm-product-join-block">
          <div class="stm-product-join-block__access">
            <h3><?php esc_html_e('Meeting Access', 'eroom-zoom-meetings-webinar-pro' );?></h3>
            <?php if ( ! empty( $password ) ): ?>
              <div class="stm-product-join-block__password">
                <span><?php esc_html_e( 'Password', 'eroom-zoom-meetings-webinar-pro' ); ?>:</span>
                <span><?php echo esc_html( $password ); ?></span>
              </div>
            <?php endif;?>
          </div>
          <?php if ( ! empty( $zoom_data ) && ! empty( $zoom_data[ 'id' ] ) ): ?>
          <div class="stm-product-join-block__link">
            <a href="<?php echo add_query_arg( array( 'show_meeting' => '1' ), get_permalink( $meeting_id) ); ?>" class="btn button alt"
               target="_blank"><?php esc_html_e( 'Join in the browser', 'eroom-zoom-meetings-webinar-pro' ); ?>
            </a>
            <a href="https://zoom.us/j/<?php echo esc_attr( $zoom_data[ 'id' ] ); ?>" class="btn button alt"
               target="_blank"><?php esc_html_e( 'Join in Zoom App', 'eroom-zoom-meetings-webinar-pro' ); ?></a>
          </div>
          <?php endif;?>
        </div>
      <?php endif;?>
        <?php if ( !empty($meeting_over) && !empty($meeting_record)):?>
            <div class="stm-product-record">
              <?php echo wp_kses($meeting_record, stm_eroom_wpkses_alowed_tags()); ?>
            </div>
        <?php endif;?>
        <?php if($product->has_file()): ?>
            <div class="stm-product-download">
              <?php foreach( $product->get_downloads() as $key => $each_download ):?>
                <div class="stm-product-download__file">
                  <span><?php esc_html_e($each_download['name'], 'eroom-zoom-meetings-webinar-pro'); ?></span>
                  <a href="<?php echo esc_url($each_download['file']); ?>">
                      <?php esc_html_e('Download', 'eroom-zoom-meetings-webinar-pro');?>
                  </a>
                </div>
              <?php endforeach; ?>
            </div>
        <?php endif;?>
      <?php endif;?>
      <?php include( STM_ZOOM_PRO_PATH . '/templates/single-product/meeting_hosts.php' ); ?>
    </div>
      <div class="stm-eroom-wctabs">
		  <?php
		  /**
		   * Hook: woocommerce_after_single_product_summary.
		   *
		   * @hooked woocommerce_output_product_data_tabs - 10
		   * @hooked woocommerce_upsell_display - 15
		   * @hooked woocommerce_output_related_products - 20
		   */
		  do_action( 'woocommerce_after_single_product_summary' );
		  ?>
      </div>
  </div>

<?php endwhile; // end of the loop. ?>

<?php
get_footer( 'shop' );
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
