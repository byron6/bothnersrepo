<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="stm_zoom_product_title_box <?php echo esc_attr( $classes ); ?>">
  <div class="meeting-title-info">
    <h1 class="meeting_title"><?php the_title(); ?></h1>

	  <?php if ( ! empty( $date ) && !$is_started ) : ?>
        <div class="meeting_date">
			<?php echo date_i18n( 'd M Y', $date ); ?>
			<?php if ( ! empty( $time ) ) {
				$time = strtotime( $time );
				$time = date_i18n( $time_format, $time );
				echo ' - ' . esc_html( $time );
			}
			?>
        </div>
		  <?php if ( ! empty( $timezone ) ) : ?>
          <div class="meeting_timezone">
			  <?php
			  if ( function_exists( 'stm_zoom_get_timezone_options' ) ) {
				  $timezones = stm_zoom_get_timezone_options();
				  if ( ! empty( $timezones[ $timezone ] ) ) {
					  $timezone = $timezones[ $timezone ];
				  }
			  }
			  ?>
			  <?php echo esc_html( $timezone ); ?>
          </div>
		  <?php endif; ?>
	  <?php endif; ?>
  </div>

  <div class="meeting-image">
    <img src="<?php echo esc_url( $image_url ); ?>" alt="">
  </div>
</div>

