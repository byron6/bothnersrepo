<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="product-meeting_info">
	<div class="meeting-info">
		<?php if ( ! empty( $meeting_data ) && isset( $meeting_data[ 'is_started' ] ) && ! empty( $meeting_data[ 'meeting_date' ] ) ) {
			$duration = get_post_meta( $meeting_id, 'stm_duration', true );
			if ( ! empty( $duration ) ) : ?>
				<div class="meeting_info__item">
					<span class="info_tit"><?php esc_html_e( 'Duration:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
					<span class="info_val"><?php echo esc_html( $duration ); ?> <?php esc_html_e( 'min', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
				</div>
			<?php endif; ?>
			<?php if ( ! empty( $stock ) ) : ?>
				<div class="meeting_info__item">
					<span class="info_tit"><?php esc_html_e( 'Places left:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
					<span class="info_val"><?php echo esc_html( $stock ); ?></span>
				</div>
			<?php endif; ?>
			<div class="meeting_info__item">
				<span class="info_tit"><?php esc_html_e( 'Price:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
				<span class="info_val"><?php
					if ( ! empty( $price_html ) ) {
						echo wp_kses_post( $price_html );
					} else {
						esc_html_e( 'Free', 'eroom-zoom-meetings-webinar-pro' );
					}
					?></span>
			</div>
			<?php
		} ?>
	</div>
</div>
