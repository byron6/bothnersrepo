<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php if(!$is_purchased): ?>
  <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
  <div class="cart">
    <?php if(!$product->get_manage_stock() || ($stock > 0)): ?>
      <a href="<?php echo esc_url(wc_get_cart_url().$product->add_to_cart_url() ); ?>" rel="nofollow"
         class="single_add_to_cart_button stm_eroom_single_add_to_cart_button button alt">
      <?php if(!$is_started): ?>
          <?php esc_html_e( 'Get access now', 'eroom-zoom-meetings-webinar-pro' ); ?>
      <?php else: ?>
          <?php esc_html_e( 'Add to cart', 'eroom-zoom-meetings-webinar-pro' ); ?>
      <?php endif; ?>
      </a>
    <?php else: ?>
      <div class="single_add_to_cart_button button alt">
          <?php esc_html_e( 'Sold out', 'eroom-zoom-meetings-webinar-pro' ); ?>
      </div>
    <?php endif; ?>
  </div>
  <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
<?php else: ?>
  <div class="cart">
    <div class="single_add_to_cart_button button alt">
        <?php esc_html_e( 'Purchased', 'eroom-zoom-meetings-webinar-pro' ); ?>
    </div>
  </div>
<?php endif; ?>