<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="product-meeting_info">
	<div class="meeting-info">
		<?php if ( ! empty( $meeting_data ) && isset( $meeting_data[ 'is_started' ] ) && ! empty( $meeting_data[ 'meeting_date' ] ) ) {
			?>
			<?php
			if ( ! $meeting_data[ 'is_started' ] ) {
				?>
				<div class="start-title">
					<?php
					( $zoom_post_type == 'stm-zoom-webinar' ) ? esc_html_e( 'Webinar starts In:', 'eroom-zoom-meetings-webinar-pro' ) : esc_html_e( 'Meeting starts In:', 'eroom-zoom-meetings-webinar-pro' );
					?>
				</div>
				<?php
				echo StmZoom::countdown( $meeting_data[ 'meeting_date' ], true );
			} else {?>
          <div class="stm-meeting-ended">
            <div class="stm-meeting-ended-title">
              <div class="stm-meeting-ended-title-content">
                <span><?php esc_html_e('Meeting Ended', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
                <span class="stm-meeting-ended--time"><?php echo date_i18n( 'd M Y', $date ); ?></span>
              </div>
            </div>
          <?php if($product->has_file()): ?>
            <div class="stm-meeting-ended-file">
                <?php esc_html_e('Attachments included', 'eroom-zoom-meetings-webinar-pro' ); ?>
            </div>
          <?php endif; ?>
          </div>
      <?php }
		} ?>
	</div>
</div>
