<?php

/*
Plugin Name: eRoom - Zoom Meetings & Webinar Pro
Plugin URI: https://wordpress.org/plugins/zoom-video-conference/
Description: eRoom Zoom Meetings & Webinar WordPress Plugin provides you with great functionality of managing Zoom meetings, scheduling options, and users directly from your WordPress dashboard.
The plugin is a free yet robust and reliable extension that enables direct integration of the world’s leading video conferencing tool Zoom with your WordPress website.
Author: StylemixThemes
Author URI: https://stylemixthemes.com/
Text Domain: eroom-zoom-meetings-webinar-pro
Version: 1.1.4
*/

if ( !function_exists( 'eroom_fs' ) ) {
    function eroom_fs()
    {
        global  $eroom_fs ;
        
        if ( !isset( $eroom_fs ) ) {
            require_once dirname( __FILE__ ) . '/freemius/start.php';
            $eroom_fs = fs_dynamic_init( array(
                'id'              => '6034',
                'slug'            => 'eroom-zoom-meetings-webinar-pro',
                'type'            => 'plugin',
                'public_key'      => 'pk_91f2676674910675990e30b3fa84e',
                'is_premium'      => true,
                'is_premium_only' => true,
                'has_addons'      => false,
                'has_paid_plans'  => true,
                'has_affiliation' => 'all',
                'menu'            => array(
                'slug'       => 'stm_zoom_pro',
                'first-path' => 'admin.php?page=stm_zoom_pro',
                'support'    => false,
            ),
                'is_live'         => true,
            ) );
        }
        
        return $eroom_fs;
    }
    
    eroom_fs();
    do_action( 'eroom_fs_loaded' );
}

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
//Exit if accessed directly
define( 'STM_ZOOM_PRO_VERSION', '1.1.4' );
define( 'STM_ZOOM_PRO_FILE', __FILE__ );
define( 'STM_ZOOM_PRO_PATH', dirname( STM_ZOOM_PRO_FILE ) );
define( 'STM_ZOOM_PRO_URL', plugin_dir_url( STM_ZOOM_PRO_FILE ) );
if ( !is_textdomain_loaded( 'eroom-zoom-meetings-webinar-pro' ) ) {
    load_plugin_textdomain( 'eroom-zoom-meetings-webinar-pro', false, 'eroom-zoom-meetings-webinar-pro/languages' );
}
if ( eroom_fs()->is__premium_only() ) {
    
    if ( eroom_fs()->can_use_premium_code() ) {
        register_activation_hook( __FILE__, 'enable_options' );
        require_once STM_ZOOM_PRO_PATH . '/inc/helpers.php';
        if ( !class_exists( 'WP_Request' ) ) {
            require_once STM_ZOOM_PRO_PATH . '/classes/class-wp-request.php';
        }
        if ( !class_exists( 'WP_Router' ) ) {
            require_once STM_ZOOM_PRO_PATH . '/classes/class-wp-router.php';
        }
        add_action( 'plugins_loaded', 'stm_zoom_pro_init' );
        function stm_zoom_pro_init()
        {
            require_once STM_ZOOM_PRO_PATH . '/inc/init.php';
        }
    
    }

}
if ( is_admin() ) {
    require_once STM_ZOOM_PRO_PATH . '/inc/item-announcements.php';
}