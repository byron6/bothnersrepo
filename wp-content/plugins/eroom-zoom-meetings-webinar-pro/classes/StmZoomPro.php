<?php

class StmZoomPro
{
    /**
     * @return StmZoomPro constructor.
     */
    function __construct()
    {
        self::enable_addons();
        add_action( 'admin_menu', function() {
            add_menu_page( 'eRoom PRO', 'eRoom PRO', 'manage_options', 'stm_zoom_pro', array( $this, 'admin_page' ), 'dashicons-video-alt2', 40 );
        }, 100 );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );

        add_action( 'wp_ajax_stm_zoom_update_addon', array( $this, 'update_addon' ) );
    }

    /**
     * Include all Addons
     */
    public static function enable_addons()
    {
        if ( class_exists( 'StmZoom' ) ) {
            $addons = self::addons();
            $enabled_addons = get_option( 'stm_zoom_addons', array( 'stm_zoom_woo' => 'enable' ) );
            foreach ( $addons as $addon ) {
                $slug = $addon['slug'];
                if ( ! empty( $enabled_addons ) && ! empty( $enabled_addons[ $slug ] ) && $enabled_addons[ $slug ] === 'enable' ) {
                    require_once STM_ZOOM_PRO_PATH . '/addons/' . $slug . '.php';
                }
            }
        }
    }

    /**
     * Show All Addons
     * @return mixed
     */
    public static function addons()
    {
        $addons = array(
            array(
                'slug' => 'stm_zoom_woo',
                'name' => esc_html__( 'Purchasable Meetings', 'eroom-zoom-meetings-webinar-pro' )
            )
        );
        return apply_filters( 'stm_zoom_pro_addons', $addons );
    }

    /**
     * Addons Page template
     */
    public function admin_page()
    {
        require_once STM_ZOOM_PRO_PATH . '/admin_pages/addons.php';
    }

    /**
     * Enqueue Styles & Scripts
     */
    public function enqueue()
    {
        wp_enqueue_style( 'stm_zoom_pro_admin', STM_ZOOM_PRO_URL . '/assets/css/admin/main.css', false, STM_ZOOM_PRO_VERSION );

        wp_enqueue_script( 'stm_zoom_pro_admin', STM_ZOOM_PRO_URL . '/assets/js/admin/admin.js', array( 'jquery' ), STM_ZOOM_PRO_VERSION, true );
    }

    /**
     * Addons Switcher
     */
    public function update_addon()
    {
        if ( ! empty( $_POST[ 'addon' ] ) && ! empty( $_POST[ 'status' ] ) ) {
            $addon              = sanitize_text_field( $_POST[ 'addon' ] );
            $status             = sanitize_text_field( $_POST[ 'status' ] );
            $addons             = get_option( 'stm_zoom_addons', array() );
            $addons[ $addon ]   = $status;
            update_option( 'stm_zoom_addons', $addons );
            wp_send_json('Updated');
        }
    }
}