(function ($) {
	'use strict';
	$(document).ready(function () {
		$('.stm_zoom_addons .addon_action input').on('change', function () {
			let addon = $(this).data('addon');
			let status = this.checked ? 'enable' : 'disable';

			$.ajax({
				method: 'post',
				type: 'json',
				url: stm_wpcfto_ajaxurl,
				data: {
					action: 'stm_zoom_update_addon',
					addon: addon,
					status: status,
				}
			});
		})
	});
})(jQuery);