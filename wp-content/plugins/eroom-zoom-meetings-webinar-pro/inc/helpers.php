<?php
add_filter( 'woocommerce_product_tabs', 'custom_description_tab', 101 );
function custom_description_tab( $tabs ) {
    global $product;
    if( 'stm_zoom' == $product->get_type() ) {
        if(!empty($tabs['description'])){
            $tabs['description']['callback'] = 'stm_zoom_description_tab_content';
            $tabs['description']['title'] = esc_html__('Information', 'eroom-zoom-meetings-webinar-pro');
        }
    }
    return $tabs;
}
function stm_zoom_description_tab_content() {
    the_content();
}

function stm_zoom_has_bought_items($ids = array(), $user_id = '') {
    $bought = false;
    if(!is_user_logged_in()) return false;
    if(!empty($ids)){
        if(empty($user_id)) {
            $user_id = get_current_user_id();
        }

        $customer_orders = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => 'shop_order',
            'post_status' => 'wc-completed'
        ) );
        foreach ( $customer_orders as $customer_order ) {
            $order = wc_get_order( $customer_order );

            foreach ($order->get_items() as $item) {

                $product_id = $item->get_product_id();

                if ( in_array( $product_id, $ids ) ) {
                    return true;
                }

            }
        }
        wp_reset_postdata();
    }
    return $bought;
}

function stm_zoom_woo_set_html_content_type() {
    return 'text/html';
}

function stm_zoom_filtered_output($data) {
    return apply_filters('stm_zoom_filtered_output', $data);
}

function stm_eroom_wpkses_alowed_tags() {
	$my_allowed = wp_kses_allowed_html( 'post' );
	// iframe
	$my_allowed['iframe'] = array(
		'src'             => true,
		'height'          => true,
		'width'           => true,
		'frameborder'     => true,
		'allowfullscreen' => true,
	);
	return $my_allowed;
}

function stm_zoom_admin_notice(){
    if ( !class_exists('StmZoom') ) {
        echo '<div class="notice notice-warning is-dismissible">
             <p>' . esc_html__('Please install eRoom – Zoom Meetings & Webinar plugin ', 'eroom-zoom-meetings-webinar-pro') . '<a href="https://wordpress.org/plugins/eroom-zoom-meetings-webinar/" class="button">' . esc_html__('Get now', 'eroom-zoom-meetings-webinar-pro') .'</a></p>
         </div>';
    }
    if ( !is_plugin_active( 'woocommerce/woocommerce.php')) {
        echo '<div class="notice notice-warning is-dismissible">
             <p>' . esc_html__('Please install WooCommerce plugin ', 'eroom-zoom-meetings-webinar-pro') . '<a href="https://wordpress.org/plugins/woocommerce/" class="button">' . esc_html__('Get now', 'eroom-zoom-meetings-webinar-pro') .'</a></p>
         </div>';
    }

}
add_action('admin_notices', 'stm_zoom_admin_notice');

function enable_options(){
    $addons                      = get_option( 'stm_zoom_addons', array() );
    if (empty($addons)) {
         $addon_zoom[ 'stm_zoom_woo' ]= 'enable';
         update_option( 'stm_zoom_addons', $addon_zoom);
    }
}