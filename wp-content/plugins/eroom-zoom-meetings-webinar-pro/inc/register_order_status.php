<?php
class WC_Order_Status_Stm_Zoom {
    
    function __construct()
    {
        add_action('init', array($this, 'register_custom_order_statuses'));
        
        add_filter('wc_order_statuses', array($this, 'add_custom_order_statuses'));
    
        add_filter( 'bulk_actions-edit-shop_order', array( $this, 'custom_dropdown_bulk_actions_shop_order'));
    
        add_filter( 'woocommerce_admin_order_actions', array($this, 'add_custom_order_status_actions_button', 100, 2 ));
    
        add_action( 'admin_head', array($this, 'add_custom_order_status_actions_button_css' ));
    
        add_action( 'woocommerce_order_status_delivered', array($this,'bbloomer_status_custom_notification'));
    
        add_action( 'woocommerce_email_after_order_table', array($this, 'bbloomer_add_content_specific_email'), 10, 4 );
    }
    
    public function register_custom_order_statuses() {
        
        $Delivered = esc_html__( 'Delivered', 'eroom-zoom-meetings-webinar-pro' );
        register_post_status('wc-delivered ', array(
            'label' => __( 'Delivered', 'woocommerce' ),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop($Delivered.' <span class="count">(%s)</span>', $Delivered.' <span class="count">(%s)</span>')
        ));
    }
    
    public function add_custom_order_statuses($order_statuses) {
        $new_order_statuses = array();
        
        // add new order status before processing
        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
            if ('wc-processing' === $key) {
                $new_order_statuses['wc-delivered'] = __('Delivered', 'eroom-zoom-meetings-webinar-pro' );
            }
        }
        return $new_order_statuses;
    }
    
    public function custom_dropdown_bulk_actions_shop_order( $actions ) {
        $new_actions = array();
        
        // add new order status before processing
        foreach ($actions as $key => $action) {
            if ('mark_processing' === $key)
                $new_actions['mark_delivered'] = __( 'Change status to delivered', 'eroom-zoom-meetings-webinar-pro' );
            
            $new_actions[$key] = $action;
        }
        return $new_actions;
    }
    
    public function add_custom_order_status_actions_button( $actions, $order ) {
        // Display the button for all orders that have a 'processing', 'pending' or 'on-hold' status
        if ( $order->has_status( array( 'on-hold', 'processing', 'pending' ) ) ) {
            
            // The key slug defined for your action button
            $action_slug = 'delivered';
            
            // Set the action button
            $actions[$action_slug] = array(
                'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_mark_order_status&status='.$action_slug.'&order_id='.$order->get_id() ), 'woocommerce-mark-order-status' ),
                'name'      => __( 'Delivered', 'eroom-zoom-meetings-webinar-pro' ),
                'action'    => $action_slug,
            );
        }
        return $actions;
    }
    
    public function add_custom_order_status_actions_button_css() {
        $action_slug = "delivered"; // The key slug defined for your action button
        ?>
        <style>
            .wc-action-button-<?php echo $action_slug; ?>::after {
                font-family: woocommerce !important; content: "\e029" !important;
            }
        </style>
        <?php
    }
    
    public function bbloomer_status_custom_notification( $order_id ) {
        $heading = 'Order Delivered';
        $subject = 'Order Delivered';
        $mailer = WC()->mailer()->get_emails();
        $mailer['WC_Email_Customer_Completed_Order']->heading = $heading;
        $mailer['WC_Email_Customer_Completed_Order']->settings['heading'] = $heading;
        $mailer['WC_Email_Customer_Completed_Order']->subject = $subject;
        $mailer['WC_Email_Customer_Completed_Order']->settings['subject'] = $subject;
        $mailer['WC_Email_Customer_Completed_Order']->trigger( $order_id );
    }
    
    public function bbloomer_add_content_specific_email( $order, $sent_to_admin, $plain_text, $email ) {
        if ( $order->get_status() == 'delivered' ) {
            $items = $order->get_items();
    
            foreach ( $items as $item )
            {
                $product_id = $item->get_product_id();
                $meeting_id = get_post_meta($product_id, '_meeting_id', true);
                $meeting_over   = get_post_meta( $product_id, '_meeting_over', true );
              
                if ((!empty($meeting_id ) )  && ($meeting_over == 'yes')) {
                    $record = get_post_meta($product_id, '_meeting_record', true);
                    if(!empty($record)):
                            ?>
                            <div class="meeting_record">
                                <?php echo stm_zoom_filtered_output($record); ?>
                            </div>
                        <?php
                    endif;
                }
            }
        }
    }
}