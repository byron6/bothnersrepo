=== eRoom - Zoom Meetings & Webinar Pro ===

Contributors: StylemixThemes
Donate link: https://stylemixthemes.com/
Tags: zoom, meetings, webinar
Requires at least: 4.6
Tested up to: 5.7
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Changelog ==
= 1.1.4 =
* added: filter stm_remove_woocommerce_email_after_order_table to unhook stm woocommerce email
* fixed: Duplicated messages and time zone in notification email

= 1.1.3 =
* update: Woocommerce tax fields appearance
* update: eRoom purchasable meetings for Woocommerce is enabled by default
* fixed: compatibility issues with WordPress 5.6
* fixed: separate email notifications for meeting participants
* new: Purchasable Meeting product page redesign
* new: Styles for embed video and for downloadable files of purchasable meeting product with selected option 'Meeting is over'

= 1.1.2 =
* fixed: the  implementation of the product page design in themes

= 1.1.1 =
* improvement: Support Page Integrated
* improvement: Enable Addon on plugin activate
* fixed: WooCommerce errors fixed

= 1.1.0 =
* improvement: created order status 'delivered' for Zoom conference products that triggers the email sending if meeting is over.
* improvement: adding meeting / webinar link and password to order notification email
* improvement: place countdown in the product main content.
* improvement: add setting options to disable comments, Ask Mentor and ratings for Zoom conference product
* fixed: the notification time sending on time to customers
* fixed: the issue showing the countdown in two columns in Zoom conference product page
* update: Freemius SDK

= 1.0.3 =
* Zoom Webinars added for Zoom Conference Product.
* WooCommerce Virtual Product option added for Zoom Conference.
* WooCommerce Downloadable Product option added for Zoom Conference.
* WooCommerce Product displaying Conference Date issue fixed.
* WooCommerce Product page "Webinar starts in" duplicated text removed.
* Zoom Pro menu name changed into eRoom Pro.
* STM_ZOOM_PRO Class name changed into StmZoomPro.

= 1.0.2 =
* Minor bug fixes.

= 1.0.1 =
* Meetings Grid shortcode added.
* Single Meeting page added.

= 1.0.0 =
* First Release.
