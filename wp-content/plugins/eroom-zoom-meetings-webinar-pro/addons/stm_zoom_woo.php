<?php
new StmZoomWoo;

class StmZoomWoo
{
    /**
     * @return StmZoomWoo constructor.
     */
    function __construct()
    {
        self::register_product_type();
    
        self::register_order_status();

        self::add_routes();

        add_filter( 'product_type_selector', array( $this, 'add_product_type_selector' ) );

        add_filter( 'product_type_options', array( $this, 'add_product_type_options' ) );

        add_filter( 'woocommerce_product_class', array( $this, 'add_product_class' ), 2, 100 );

        register_activation_hook( __FILE__, array( $this, 'install' ) );

        add_filter( 'template_include', array( $this, 'stm_eroom_single_product_template' ), 20, 1 );

        add_filter( 'woocommerce_template_loader_files', function( $templates, $template_name ){
          // Capture/cache the $template_name which is a file name like single-product.php
          wp_cache_set( 'stm_eroom_wc_main_template', $template_name ); // cache the template name
          return $templates;
        }, 10, 2 );

        add_filter('woocommerce_related_products',  array( $this, 'stm_filter_related_products'), 10);

        add_filter( 'woocommerce_product_related_products_heading', array( $this, 'stm_single_related_text'), 10 );

        add_filter( 'woocommerce_output_related_products_args', array( $this, 'stm_woocommerce_output_related_products_args' ), 10);

        add_action( 'woocommerce_product_options_general_product_data', array( $this, 'add_product_options_data' ) );

        add_action( 'woocommerce_product_options_pricing', array( $this, 'add_product_meeting_options' ) );

        add_action( 'woocommerce_process_product_meta_stm_zoom', array( $this, 'save_product_stm_zoom_options' ) );

        add_action( 'woocommerce_order_status_completed', array( $this, 'product_order_status_completed' ), 10, 1 );

        add_action( 'admin_footer', array( $this, 'product_admin_footer' ) );

        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );

        add_action( 'wp_ajax_stm_zoom_send_message', array( $this, 'send_message' ) );
        add_action( 'wp_ajax_nopriv_stm_zoom_send_message', array( $this, 'send_message' ) );

        add_filter( 'stm_zoom_settings_fields', array( $this, 'add_zoom_settings' ), 11, 1 );

        add_action( 'stm_zoom_after_update_meeting', array( $this, 'create_cron' ), 10, 1 );

        add_action( 'stm_zoom_after_create_meeting', array( $this, 'create_cron' ), 10, 1 );

        add_action( 'stm_zoom_notification', array( $this, 'send_meeting_notification' ), 1, 4 );
    
        add_action( 'init', array($this, 'changeActions' ));

        add_action( 'woocommerce_email_after_order_table', array($this, 'mm_email_after_order_table'), 10, 4 );
    
        add_action( 'init', array($this, 'enable_comments' ),20);
        
        add_action( 'init', array($this, 'order_status_delivered' ));
    }

    public function mm_email_after_order_table( $order, $sent_to_admin, $plain_text, $email ) {
        $stm_disable_button = apply_filters( 'stm_remove_woocommerce_email_after_order_table', false );
        if ($order->status == 'completed' && !$stm_disable_button) {
            $items = $order->get_items();
            
            foreach ( $items as $item )
            {
                $product_id = $item->get_product_id();
                $meeting_id = get_post_meta($product_id, '_meeting_id', true);
                if (!empty($meeting_id )) {
                    $password = get_post_meta($meeting_id, 'stm_password', true);
                    $zoom_data = get_post_meta($meeting_id, 'stm_zoom_data', true);
                    if (!empty($password)) {
                        esc_html_e('Password for access:', 'eroom-zoom-meetings-webinar-pro');
                        echo ' ' . $password . ' ';
                    }
                    ?>

                    <a href="https://zoom.us/j/<?php echo esc_attr($zoom_data['id']); ?>" class="btn button alt"
                       target="_blank">
                        <?php esc_html_e('for zoom conference', 'eroom-zoom-meetings-webinar-pro'); ?>
                        <?php esc_html_e('Join url', 'eroom-zoom-meetings-webinar-pro'); ?>
                    </a><br>&nbsp;</br>
                    <?php
                }
            }
        }
    }
  
    public function changeActions()
    {

        if( function_exists('et_divi_output_product_wrapper') )   remove_action('woocommerce_before_single_product_summary','et_divi_output_product_wrapper',0);
        if( function_exists('et_divi_output_product_wrapper_end') )    remove_action('woocommerce_after_single_product_summary','et_divi_output_product_wrapper_end', 0);;
        
        remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',5);
    }
    
    public function enable_comments()
    {
        $settings = get_option( 'stm_zoom_settings', array() );
        if (empty($settings[ 'enable_comments'])) $settings[ 'enable_comments'] = null;
        if ( $settings[ 'enable_comments'] != 'yes' ) {
            add_filter( 'comments_open', '__return_false' );
            add_filter('wp_head', array($this, 'product_siebar_right'));
        }
    }
    
    // Add items to the header!
    public function product_siebar_right()
    {
       echo '<style>
                body.single-product .product-type-stm_zoom { display: block;}
                .woocommerce div.product div.summary{float: right;}
             </style>';
    }


    /**
     * Register Zoom WooCommerce Product Type
     */
    public static function register_product_type()
    {
        if ( class_exists( 'WooCommerce' ) ) {
            require_once STM_ZOOM_PRO_PATH . '/inc/register_product_type.php';
        }
    }
    
    /**
     * Register Zoom WooCommerce Order Status
     */
    public static function register_order_status()
    {
        if ( class_exists( 'WooCommerce' ) ) {
            require_once STM_ZOOM_PRO_PATH . '/inc/register_order_status.php';
        }
    }
    
    public function order_status_delivered(){
        if ( class_exists( 'WooCommerce' ) )  new WC_Order_Status_Stm_Zoom;
    }

    /**
     * Register Routes
     */
    public static function add_routes()
    {
        global $wp_router;
        if ( ! $wp_router ) {
            $wp_router = new WP_Router();
        }
        $wp_router->get( array(
            'as' => 'simpleRoute',
            'uri' => "/zoom-users/{user_id}",
            'uses' => "StmZoomWoo@stm_zoom_user"
        ) );
    }

    /**
     * Single User public page
     * @param $user_id
     */
    public static function stm_zoom_user( $user_id )
    {
        require_once STM_ZOOM_PRO_PATH . '/templates/users/user.php';
    }

    /**
     * Add Zoom WooCommerce Product Type Selector
     * @param $types
     * @return mixed
     */
    public function add_product_type_selector( $types )
    {
        $types[ 'stm_zoom' ] = esc_html__( 'Zoom conference', 'eroom-zoom-meetings-webinar-pro' );

        return $types;
    }

    /**
     * Add Zoom WooCommerce Product Type Options
     * @param $options
     * @return mixed
     */
    public function add_product_type_options( $options )
    {
        $options['downloadable']['wrapper_class'] = 'show_if_simple show_if_stm_zoom';
        $options['virtual']['wrapper_class'] = 'show_if_simple show_if_stm_zoom';

        return $options;
    }

    /**
     * Add Zoom WooCommerce Product Class
     * @param $classname
     * @param $product_type
     * @return string
     */
    public function add_product_class( $classname, $product_type )
    {
        if ( $product_type == 'stm_zoom' ) {
            $classname = 'WC_Product_Stm_Zoom';
        }
        return $classname;
    }

    /**
     * Insert Zoom WooCommerce Product Term
     */
    public function install()
    {
        if ( ! get_term_by( 'slug', 'stm_zoom', 'product_type' ) ) {
            wp_insert_term( 'stm_zoom', 'product_type' );
        }
    }

	/**
	 * Load Zoom WooCommerce Product Frontend Template
	 *
	 * @param $template
	 *
	 * @return bool|string
	 */
	public function stm_eroom_single_product_template( $template ) {
		if ( $template_name = wp_cache_get( 'stm_eroom_wc_main_template' ) ) {
			wp_cache_delete( 'stm_eroom_wc_main_template' ); // delete the cache
			if ( $template_name == 'single-product.php' ) {
				global $post;
				global $product;
				if ( $post->post_type == 'product' ) {
					$product = wc_get_product( get_the_ID() );
					if ( $product->get_type() === 'stm_zoom' ) {
						$path = self::get_zoom_woo_templates_path( 'single-product.php' );
						if ( $path ) {
							$template = $path;
						}
					}
				}
			}
		}

		return $template;
	}

    /**
     * Zoom WooCommerce Product Frontend Template
     * @param $file
     * @return bool|string
     */
    public static function get_zoom_woo_templates_path( $file )
    {
        $templates = array(
            get_stylesheet_directory() . '/zoom_templates/',
            get_template_directory() . '/zoom_templates/',
            STM_ZOOM_PRO_PATH . '/zoom_templates/',
        );

        $templates = apply_filters( 'stm_zoom_woo_pathes', $templates );

        foreach ( $templates as $template ) {
            if ( file_exists( $template . $file ) ) {
                return $template . $file;
            }
        }

        return false;
    }

	public function stm_woocommerce_output_related_products_args( $args ) {
		global $product;

		if ( 'stm_zoom' == $product->get_type() ) {
			$args = [
				'posts_per_page' => 3,
				'columns' => 3,
				'orderby' => 'rand',
				'post_type' => 'product'
			];
		}

		return $args;
	}

	public function stm_filter_related_products( $related_product_ids ) {
		global $product;
		if ( 'stm_zoom' == $product->get_type() ) {
			// WC source code stores IDs as string in this array, so I did that too
			$args = array(
				'type' => 'stm_zoom',
				'status' => 'publish',
				'limit' => - 1,
				'return' => 'ids'
			);
			$related_product_ids = wc_get_products( $args );
			//Removing the product ID on the current page
			if ( ( $key = array_search( get_the_ID(), $related_product_ids ) ) !== false ) {
				unset( $related_product_ids[ $key ] );
			}
		}

		return $related_product_ids;
	}

	public function stm_single_related_text( $text ) {
		global $product;
		if ( 'stm_zoom' == $product->get_type() ) {
			$text = esc_html__( 'Related Meetings / Webinars', 'eroom-zoom-meetings-webinar-pro' );
		}

		return $text;
	}

    /**
     * Add Zoom WooCommerce Product Admin Options Data
     */
    public function add_product_options_data() {
        echo '<div class="options_group show_if_stm_zoom clear"></div>';
    }

    /**
     * Add Zoom WooCommerce Product Admin Options
     */
    public function add_product_meeting_options()
    {
        global $product_object;
        ?>
        <div class='options_group show_if_stm_zoom'>
            <?php
            woocommerce_wp_select(
                array(
                    'id' => '_meeting_id',
                    'label' => esc_html__( 'Select meeting / webinar', 'eroom-zoom-meetings-webinar-pro' ),
                    'value' => $product_object->get_meta( '_meeting_id', true ),
                    'default' => '',
                    'data_type' => 'price',
                    'options' => get_meetings_webinars()
                )
            );
            woocommerce_wp_checkbox(
                array(
                    'id' => '_meeting_over',
                    'label' => esc_html__( 'Meeting is over', 'eroom-zoom-meetings-webinar-pro' ),
                    'value' => $product_object->get_meta( '_meeting_over', true ),
                    'default' => '',
                    'data_type' => 'price',
                )
            );
            woocommerce_wp_textarea_input(
                array(
                    'id' => '_meeting_record',
                    'label' => esc_html__( 'Meeting record embed code', 'eroom-zoom-meetings-webinar-pro' ),
                    'value' => $product_object->get_meta( '_meeting_record', true ),
                    'default' => '',
                    'data_type' => 'price',
                )
            );
            ?>
        </div>
        <?php
    }

    /**
     * Save Zoom WooCommerce Product Admin Options
     * @param $post_id
     */
    public function save_product_stm_zoom_options( $post_id )
    {
        $meeting_id = isset( $_POST[ '_meeting_id' ] ) ? sanitize_text_field( $_POST[ '_meeting_id' ] ) : '';
        $meeting_over = isset( $_POST[ '_meeting_over' ] ) ? sanitize_text_field( $_POST[ '_meeting_over' ] ) : '';
        $meeting_record = isset( $_POST[ '_meeting_record' ] ) ? stm_zoom_filtered_output( $_POST[ '_meeting_record' ] ) : '';
        update_post_meta( $post_id, '_meeting_id', $meeting_id );
        update_post_meta( $post_id, '_meeting_over', $meeting_over );
        update_post_meta( $post_id, '_meeting_record', $meeting_record );

        $meeting_host = get_post_meta( $meeting_id, 'stm_host', true );

        update_post_meta( $post_id, '_meeting_host', $meeting_host );
    }

    /**
     * Zoom WooCommerce Product Order Status Complete
     * @param $order_id
     */
    public function product_order_status_completed( $order_id )
    {
        $user_id = get_post_meta( $order_id, '_customer_user', true );
        if ( ! empty( $user_id ) ) {
            $order = wc_get_order( $order_id );
            foreach ( $order->get_items() as $item ) {
                $product_id = $item->get_product_id();
                $customers  = array();
                $recipients = get_post_meta( $product_id, 'stm_customer', true );
                if ( ! empty( $recipients ) && is_array( $recipients ) ) {
                    $customers = $recipients;
                }
                $customers[] = $user_id;
                update_post_meta( $product_id, 'stm_customer', $customers );
            }
        }else{
            $order = wc_get_order( $order_id );
            $order_meta = get_post_meta($order_id);
            
            foreach ( $order->get_items() as $item ) {
                $product_id = $item->get_product_id();
                $customers_email  = array();
                $recipients_email = get_post_meta( $product_id, 'stm_customer_guest_customers_email', true );
                if ( ! empty(  $recipients_email ) && is_array(  $recipients_email ) ) {
                    $customers_email =  $recipients_email;
                }
                if (!empty($order_meta['_billing_email'][0]) && (!in_array($order_meta['_billing_email'][0],$customers_email )))  {
                    $customers_email[] = $order_meta['_billing_email'][0];
                    update_post_meta($product_id, 'stm_customer_guest_customers_email',$customers_email);
                }
            }
        }
    }

    /**
     * Add Custom JavaScript to WooCommerce Admin Product page
     */
    public function product_admin_footer()
    {
        global $post, $product_object;

        if ( !$post ) {
            return;
        }
        if ( 'product' != $post->post_type ) :
            return;
        endif;
        $is_stm_zoom = $product_object && 'stm_zoom' === $product_object->get_type() ? true : false;
        ?>
        <script type='text/javascript'>
            jQuery(document).ready(function () {
                //for Price tab
                jQuery('#general_product_data .pricing').addClass('show_if_stm_zoom');
                jQuery('.product_data_tabs .inventory_tab').addClass('show_if_stm_zoom');
                jQuery('.product_data_tabs .shipping_tab').addClass('hide_if_stm_zoom');
                jQuery('.product_data_tabs .linked_product_tab').addClass('hide_if_stm_zoom');
                jQuery('.product_data_tabs .attribute_tab').addClass('hide_if_stm_zoom');
                jQuery('.product_data_tabs .variations_tab').addClass('hide_if_stm_zoom');
                jQuery('.inventory_options').addClass('show_if_stm_zoom').show();
                jQuery('#inventory_product_data ._manage_stock_field').addClass('show_if_stm_zoom').show();
                jQuery('#inventory_product_data ._sold_individually_field').parent().addClass('show_if_stm_zoom').show();
                jQuery('#inventory_product_data ._sold_individually_field').addClass('show_if_stm_zoom').show();
                jQuery('._tax_status_field').parent().addClass('show_if_stm_zoom').show();
                <?php if ( $is_stm_zoom ) { ?>
                jQuery('#general_product_data .pricing').show();
                jQuery('.product_data_tabs .inventory_tab').show();
                jQuery('.product_data_tabs .shipping_tab').hide();
                jQuery('.product_data_tabs .linked_product_tab').hide();
                jQuery('.product_data_tabs .attribute_tab').hide();
                jQuery('.product_data_tabs .variations_tab').hide();

                jQuery('.inventory_options').show();
                jQuery('#inventory_product_data ._manage_stock_field').show();
                jQuery('#inventory_product_data ._sold_individually_field').show();
                jQuery('#inventory_product_data ._sold_individually_field').show();
                <?php } ?>
            });
        </script>
        <?php
    }

    /**
     * Enqueue Frontend Styles & Scripts
     */
    public function enqueue_scripts_styles()
    {
        wp_enqueue_style( 'stm_zoom_woo', STM_ZOOM_PRO_URL . '/assets/css/app.css', false, STM_ZOOM_PRO_VERSION );

        wp_enqueue_script( 'stm_zoom_woo', STM_ZOOM_PRO_URL . '/assets/js/app.js', array( 'jquery' ), STM_ZOOM_PRO_VERSION, true );
    }

    /**
     * Send Mail Message
     */
    public function send_message()
    {
        $name = !empty( $_POST[ 'name' ] ) ? sanitize_text_field( $_POST[ 'name' ] ) : '';
        $email = !empty( $_POST[ 'email' ] ) ? sanitize_text_field( $_POST[ 'email' ] ) : '';
        $message = !empty( $_POST[ 'message' ] ) ? sanitize_text_field( $_POST[ 'message' ] ) : '';
        $host_email = !empty( $_POST[ 'host_email' ] ) ? sanitize_text_field( $_POST[ 'host_email' ] ) : '';
        $r = array(
            'message' => esc_html__( 'Error, email not sent!', 'eroom-zoom-meetings-webinar-pro' ),
            'status' => 'error'
        );
        if ( ! empty( $host_email ) && ! empty( $message ) && ! empty( $email ) && ! empty( $name ) ) {
            add_filter( 'wp_mail_content_type', 'stm_zoom_woo_set_html_content_type' );
            $email_text = '<p><b>' . esc_html__( 'Name: ', 'eroom-zoom-meetings-webinar-pro' ) . '</b></p><p>' . $name . '</p><p><b>' . esc_html__( 'Email: ', 'eroom-zoom-meetings-webinar-pro' ) . '</b></p><p>' . $email . '</p><p><b>' . esc_html__( 'Message: ', 'eroom-zoom-meetings-webinar-pro' ) . '</b></p><p>' . $message . '</p>';
            $sent_email = wp_mail( $host_email, 'The subject', $email_text );

            remove_filter( 'wp_mail_content_type', 'stm_zoom_woo_set_html_content_type' );

            if ( $sent_email ) {
                $r = array(
                    'message' => esc_html__( 'Success, email has been sent!', 'eroom-zoom-meetings-webinar-pro' ),
                    'status' => 'success'
                );
            }
        }
        wp_send_json( $r );
    }

    /**
     * Add WooCommerce Addon Settings
     * @param $fields
     * @return mixed
     */
    public function add_zoom_settings( $fields )
    {
        $fields[ 'tab_2' ] = array(
            'name' => esc_html__( 'Woocommerce addon settings', 'eroom-zoom-meetings-webinar-pro' ),
            'fields' => array(
                'user_notification_time' => array(
                    'type' => 'number',
                    'label' => esc_html__( 'Notification time', 'eroom-zoom-meetings-webinar-pro' ),
                    'value' => '',
                    'description' => esc_html__( 'Enter number of minutes before meeting start', 'eroom-zoom-meetings-webinar-pro' )
                ),
                'enable_comments' => array(
                     'type' => 'radio',
                     'label' => esc_html__( 'Enable Comments/Rating', 'eroom-zoom-meetings-webinar-pro' ),
                     'options'   => array('yes'=>'Yes', 'no' =>'No'),
                     'value' => 'yes',
                     'description' => esc_html__( 'Enable Comments/ratins in Webinar/Meeting Zoom Product Page', 'eroom-zoom-meetings-webinar-pro' )
                    ),
                'enable_ask_question' => array(
                    'type' => 'radio',
                    'label' => esc_html__( 'Enable Ask Question', 'eroom-zoom-meetings-webinar-pro' ),
                    'options'   => array('yes'=>'Yes', 'no' =>'No'),
                    'value' => 'yes',
                    'description' => esc_html__( 'Enable Ask Question/Mentor in Webinar/Meeting Zoom Product Page', 'eroom-zoom-meetings-webinar-pro' )
                )
            )
        );
        return $fields;
    }

    /**
     * Cron on save Meeting
     * @param $post_id
     */
    public function create_cron( $post_id )
    {
        $settings = get_option( 'stm_zoom_settings', array() );
        if ( ! empty( $settings[ 'user_notification_time' ] ) && ! empty( $post_id ) ) {
            $time_offset    = sanitize_text_field( $settings[ 'user_notification_time' ] );
            $start_date     = !empty( $_POST[ 'stm_date' ] ) ? intval( $_POST[ 'stm_date' ] ) : '';
            $start_time     = !empty( $_POST[ 'stm_time' ] ) ? sanitize_text_field( $_POST[ 'stm_time' ] ) : '';
            $timezone       = !empty( $_POST[ 'stm_timezone' ] ) ? sanitize_text_field( $_POST[ 'stm_timezone' ] ) : '';
            $password       = !empty( $_POST[ 'stm_password' ] ) ? sanitize_text_field( $_POST[ 'stm_password' ] ) : '';
            if ( empty( $timezone ) ) {
                $timezone   = 'UTC';
            }
            date_default_timezone_set( $timezone );
            $meeting_start  = strtotime( 'now', ( intval( $start_date ) / 1000 ) );
            if ( ! empty( $start_time ) ) {
                $time = explode( ':', $start_time );
                if ( is_array( $time ) and count( $time ) === 2 ) {
                    $meeting_start = strtotime( "+{$time[0]} hours +{$time[1]} minutes", $meeting_start );
                }
            }
            $timezone1 = new DateTimeZone( 'UTC' );
    
            $dts = wp_date("d-m-Y H:i:s", $meeting_start, $timezone1);
    
            $dt = new DateTime(wp_date($dts ), new DateTimeZone( $timezone));
            $meeting_start = $dt->getTimestamp();
            $clear_date     = $meeting_start;
            $meeting_start  = strtotime( "-{$time_offset} minutes", $meeting_start );
            $timestamp      = $meeting_start;
            $args           = array( $post_id, $clear_date, $timezone, $password );
            
            $old_cron_job   = get_post_meta( $post_id, 'stm_zoom_cron_job', true );
            if ( ! empty( $old_cron_job ) ) {
                $cron_jobs  = get_option( 'cron', array() );
                if ( ! empty( $cron_jobs ) && ! empty( $cron_jobs[ $old_cron_job ] ) ) {
                    unset( $cron_jobs[ $old_cron_job ] );
                    update_option( 'cron', $cron_jobs );
                }
            }
            wp_schedule_single_event( $timestamp, 'stm_zoom_notification', $args );
            update_post_meta( $post_id, 'stm_zoom_cron_job', $timestamp );
        }
    }

    /**
     * Send Meeting Notification
     * @param $post_id
     * @param $date
     * @param $timezone
     * @param $password
     */
    public function send_meeting_notification( $post_id, $date, $timezone, $password )
    {
        if ( ! empty( $post_id ) && ! empty( $date ) && ! empty( $timezone ) ) {
            $meeting_data   = get_post_meta( $post_id, 'stm_zoom_data', true );
            if ( empty( $meeting_data ) ) return;
            $meeting_id     = !empty( $meeting_data[ 'id' ] ) ? $meeting_data[ 'id' ] : '';
            $recipients     = array();
            $posts          = get_posts( array(
                'numberposts'   => -1,
                'meta_key'      => '_meeting_id',
                'meta_value'    => $post_id,
                'post_type'     => 'product',
            ) );

            foreach ( $posts as $post ) {
                $product_id = $post->ID;
                $recipients     = array();
                $customers  = get_post_meta( $product_id, 'stm_customer', true );
                if ( ! empty( $customers ) ) {
                    foreach ( $customers as $customer ) {
                        $user_data = get_userdata( $customer );
                        if ( ! empty( $user_data ) ) {
                            $recipients[] = $user_data->user_email;
                        }
                    }
                }
                $recipients_email = get_post_meta( $product_id, 'stm_customer_guest_customers_email', true );
                foreach ($recipients_email as $em){
                    $recipients[] = $em ;
                }
                if ( ! empty( $recipients ) ) {
                    $message = esc_html__( 'Hello, meeting ', 'eroom-zoom-meetings-webinar-pro' ) . '<a href="' . get_permalink( $product_id ) . '">' . get_the_title( $product_id ) . ' </a>' . esc_html__( 'will begin at ', 'eroom-zoom-meetings-webinar-pro' ) . esc_html( wp_date( 'F j, Y H:i', $date, new DateTimeZone( $timezone ) )  . ' ' . $timezone ) . '<br>';
                    $message .= esc_html__( 'Your meeting url: ', 'eroom-zoom-meetings-webinar-pro' );

                    $message .= '<a href="https://zoom.us/j/' . esc_attr( $meeting_id ) . '" >' . esc_html( 'https://zoom.us/j/' . $meeting_id ) . '</a><br>';
                    $message .= esc_html__( 'Your meeting password: ', 'eroom-zoom-meetings-webinar-pro' );
                    $message .= esc_html( $password );

                    add_filter( 'wp_mail_content_type', 'stm_zoom_woo_set_html_content_type' );

                    $recipients = array_unique($recipients); //remove duplicate emails from an recipients

                    foreach ($recipients as $recipient) {
                        $sent_email = wp_mail( $recipient, esc_html__( 'Meeting notification', 'eroom-zoom-meetings-webinar-pro' ), $message );
                    }
                    remove_filter( 'wp_mail_content_type', 'stm_zoom_woo_set_html_content_type' );
                }
            }
            wp_reset_postdata();
        }
    }
}
