<?php
if( class_exists( 'StmZoom' ) ) {
    $addons = StmZoomPro::addons();
    $enabled_addons = get_option( 'stm_zoom_addons', array( 'stm_zoom_woo' => 'enable' ) );
    if( !empty( $addons ) ): ?>
        <div class="stm_zoom_addons">
            <h1><?php esc_html_e('eRoom Pro Addons', 'eroom-zoom-meetings-webinar-pro'); ?></h1>
            <?php foreach( $addons as $addon ): ?>
                <?php
                $slug = $addon[ 'slug' ];
                $status = 'disable';
                if( !empty( $enabled_addons ) && !empty( $enabled_addons[ $slug ] ) ) {
                    $status = $enabled_addons[ $slug ];
                }
                ?>
                <div class="addon_item">
                    <div class="addon-wrap">
                        <div class="addon_name">
                            <?php echo esc_html( $addon[ 'name' ] ); ?>
                        </div>
                        <div class="addon_action">
                            <div class="addon_action__switch">
                                <input type="checkbox" id="<?php echo esc_attr( $slug ) . '__switcher'; ?>" data-addon="<?php echo esc_attr( $slug ); ?>" <?php echo ( $status == 'enable' ) ? 'checked' : ''; ?>>
                                <label for="<?php echo esc_attr( $slug ) . '__switcher'; ?>"></label>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php
    endif;
}