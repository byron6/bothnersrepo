<?php
$product_id = get_the_ID();
$meeting_id = get_post_meta( $product_id, '_meeting_id', true );
if( !empty( $meeting_id ) ) {
    $host_id = get_post_meta( $meeting_id, 'stm_host', true );
    $users = StmZoom::stm_zoom_get_users();
    $zoom_users = array();
    if( !empty( $users ) ) {
        foreach( $users as $user ) {
            $zoom_timezone = !empty( $user[ 'timezone' ] ) ? $user[ 'timezone' ] : 'UTC';
            $zoom_users[ $user[ 'id' ] ] = array(
                'email' => $user[ 'email' ],
                'first_name' => $user[ 'first_name' ],
                'last_name' => $user[ 'last_name' ],
                'timezone' => $zoom_timezone,
            );
        }
    }

    $user_email = !empty( $zoom_users[ $host_id ][ 'email' ] ) ? $zoom_users[ $host_id ][ 'email' ] : '';
    $first_name = !empty( $zoom_users[ $host_id ][ 'first_name' ] ) ? $zoom_users[ $host_id ][ 'first_name' ] : '';
    $last_name = !empty( $zoom_users[ $host_id ][ 'last_name' ] ) ? $zoom_users[ $host_id ][ 'last_name' ] : '';
    $timezone = !empty( $zoom_users[ $host_id ][ 'timezone' ] ) ? $zoom_users[ $host_id ][ 'timezone' ] : 'UTC';
    if( function_exists( 'stm_zoom_get_timezone_options' ) ) {
        $timezones = stm_zoom_get_timezone_options();
        if( !empty( $timezones[ $timezone ] ) ) {
            $timezone = $timezones[ $timezone ];
        }
    }
    if( !empty( $user_email ) && !empty( $first_name ) && !empty( $last_name ) && !empty( $timezone ) ):
        ?>
        <div class="product-host">
            <h3><?php esc_html_e( 'Speaker', '' ) ?></h3>
            <div class="host_user">
                <div class="user_avatar">
                    <a href="<?php echo get_home_url( '/' ) . '/zoom-users/' . esc_attr( $host_id ); ?>">
                        <?php echo get_avatar( $user_email, 80 ); ?>
                    </a>
                </div>
                <div class="user_info">
                    <a href="<?php echo get_home_url( '/' ) . '/zoom-users/' . esc_attr( $host_id ); ?>">
                        <div class="name"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></div>
                    </a>
                    <div class="user-status"><?php echo esc_html( $timezone ); ?></div>
                </div>
            </div>
        </div>
    <?php
    endif;
}
?>
