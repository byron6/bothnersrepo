<?php
global $product;
$stock          = $product->get_stock_quantity();
$post_id        = get_the_ID();
$meeting_id     = get_post_meta( $post_id, '_meeting_id', true );
$meeting_data   = StmZoom::meeting_time_data( $meeting_id );
$zoom_post_type = get_post_type( $meeting_id );
$price_html     = $product->get_price_html();
$is_purchased   = stm_zoom_has_bought_items( array( $post_id ) );
?>
<div class="product-meeting_sidebar">
    <?php if ( has_post_thumbnail() ) : ?>
        <div class="meeting-image">
            <?php the_post_thumbnail(); ?>
        </div>
    <?php endif; ?>
    <div class="meeting-info">
        <?php if ( ! empty( $meeting_data ) && isset( $meeting_data[ 'is_started' ] ) && ! empty( $meeting_data[ 'meeting_date' ] ) ) {
            ?>
            <?php
            if ( ! $meeting_data[ 'is_started' ] ) {
                ?>
                <div class="start-title">
                    <?php
                    ( $zoom_post_type == 'stm-zoom-webinar' ) ? esc_html_e( 'Webinar starts In:', 'eroom-zoom-meetings-webinar-pro' ) : esc_html_e( 'Meeting starts In:', 'eroom-zoom-meetings-webinar-pro' );
                   
                    ?>
                </div>
                <?php
                echo StmZoom::countdown( $meeting_data[ 'meeting_date' ], true );
                $duration = get_post_meta( $meeting_id, 'stm_duration', true );
            }
            ?>
            <div class="meeting_info">
                <?php if ( ! empty( $duration ) ) : ?>
                    <div class="meeting_info__item">
                        <span><?php esc_html_e( 'Duration:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
                        <span><?php echo esc_html( $duration ); ?></span>
                    </div>
                <?php endif; ?>
                <?php if ( ! empty( $stock ) ) : ?>
                    <div class="meeting_info__item">
                        <span><?php esc_html_e( 'Places left:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
                        <span><?php echo esc_html( $stock ); ?></span>
                    </div>
                <?php endif; ?>
                <div class="meeting_info__item">
                    <span><?php esc_html_e( 'Price:', 'eroom-zoom-meetings-webinar-pro' ); ?></span>
                    <span><?php
                        if ( ! empty( $price_html ) ) {
                            echo wp_kses_post( $price_html );
                        } else {
                            esc_html_e( 'Free', 'eroom-zoom-meetings-webinar-pro' );
                        }
                        ?></span>
                </div>
            </div>
            <?php
        } ?>
    </div>
</div>
