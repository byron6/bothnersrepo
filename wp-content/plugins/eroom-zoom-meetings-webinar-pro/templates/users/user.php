<?php
get_header();
$users = StmZoom::stm_zoom_get_users();
$zoom_users = array();
$post_type = 'product';
if( !empty( $users ) ) {
    foreach( $users as $user ) {
        $timezone = !empty( $user[ 'timezone' ] ) ? $user[ 'timezone' ] : 'UTC';
        $zoom_users[ $user[ 'id' ] ] = array(
            'email' => $user[ 'email' ],
            'first_name' => $user[ 'first_name' ],
            'last_name' => $user[ 'last_name' ],
            'timezone' => $timezone,
        );
    }
}
if( !empty( $zoom_users[ $user_id ] ) ) {
    $user_email = !empty( $zoom_users[ $user_id ][ 'email' ] ) ? $zoom_users[ $user_id ][ 'email' ] : '';
    $first_name = !empty( $zoom_users[ $user_id ][ 'first_name' ] ) ? $zoom_users[ $user_id ][ 'first_name' ] : '';
    $last_name = !empty( $zoom_users[ $user_id ][ 'last_name' ] ) ? $zoom_users[ $user_id ][ 'last_name' ] : '';
    $timezone = !empty( $zoom_users[ $user_id ][ 'timezone' ] ) ? $zoom_users[ $user_id ][ 'timezone' ] : 'UTC';
    if( function_exists( 'stm_zoom_get_timezone_options' ) ) {
        $timezones = stm_zoom_get_timezone_options();
        if( !empty( $timezones[ $timezone ] ) ) {
            $timezone = $timezones[ $timezone ];
        }
    }

    $upcoming_meetings = array();
    $old_meetings = array();

    $user_meetings_args = array(
        'post_type' => 'product',
        'posts_per_page' => '-1',
        'meta_query' => array(
            array(
                'key' => '_meeting_host',
                'value' => $user_id,
            )
        )
    );
    $q = new WP_Query( $user_meetings_args );

    if( $q->have_posts() ) {
        while ( $q->have_posts() ) {
            $q->the_post();
            $meeting_id = get_post_meta( get_the_ID(), '_meeting_id', true );
            $meeting_data = StmZoom::meeting_time_data( $meeting_id );
            if( !empty( $meeting_data ) && isset( $meeting_data[ 'is_started' ] ) ) {
                if( !$meeting_data[ 'is_started' ] ) {
                    $upcoming_meetings[] = get_the_ID();
                }
                else {
                    $old_meetings[] = get_the_ID();
                }
            }
        }
    }
    wp_reset_postdata();
    ?>
    <div class="user_title_box">
        <?php
        if( !empty( $user_email ) && !empty( $first_name ) && !empty( $last_name ) && !empty( $timezone ) ):
            ?>
            <div class="container">
                <div class="product-host">
                    <div class="host_user">
                        <div class="user_avatar">
                            <?php echo get_avatar( $user_email, 200 ); ?>
                        </div>
                        <div class="user_info">
                            <h1 class="name"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></h1>
                            <div class="user-status"><?php echo esc_html( $timezone ); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if( !empty( $upcoming_meetings ) ): ?>
        <div class="author_meetings woocommerce">
            <?php
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => '-1',
                'post__in' => $upcoming_meetings
            );
            $loop = new WP_Query( $args );

            if( $loop->have_posts() ) { ?>
                <h2><?php esc_html_e( 'Upcoming webinars', 'eroom-zoom-meetings-webinar-pro' ); ?></h2>
                <div class="stm_zoom_grid_container">
                    <div class="stm_zoom_grid per_row_3">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post();
                            $path = get_zoom_template('loop/single-meeting.php');
                            include $path;
                        endwhile;
                        ?>
                    </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>

        </div>
    <?php endif; ?>
    <?php if( !empty( $old_meetings ) ): ?>
        <div class="author_meetings woocommerce">
            <?php
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => '-1',
                'post__in' => $old_meetings
            );
            $loop = new WP_Query( $args );

            if( $loop->have_posts() ) { ?>
                <h2><?php esc_html_e( 'Archive webinars', 'eroom-zoom-meetings-webinar-pro' ); ?></h2>
                <div class="stm_zoom_grid_container">
                    <div class="stm_zoom_grid per_row_3">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post();
                            $path = get_zoom_template('loop/single-meeting.php');
                            include $path;
                        endwhile;
                        ?>
                    </div>
                </div>
                <?php
            }
            wp_reset_postdata();
            ?>
        </div>
    <?php endif;
}
get_footer();
