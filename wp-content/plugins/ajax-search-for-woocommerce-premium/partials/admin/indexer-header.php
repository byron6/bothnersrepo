<?php

// Exit if accessed directly
if ( ! defined( 'DGWT_WCAS_FILE' ) ) {
	exit;
}

?>
<div class="dgwt-wcas-indexing-header">
	<span class="dgwt-wcas-indexing-header__title" style="color:<?php echo $statusColor; ?>"><?php echo $text; ?></span>

	<?php if ( ! empty( $lastDate ) ): ?>
		<span class="dgwt-wcas-indexing-header__subtitle">

        <?php if ( ! empty( $totalProducts ) ): ?>
	        <?php echo sprintf( __( 'Indexed <strong>100&#37;</strong>, <strong>%d products</strong>.', 'ajax-search-for-woocommerce' ), $totalProducts ); ?>
        <?php endif; ?>

			<?php
			echo ' ' . sprintf( __( 'Last build %s', 'ajax-search-for-woocommerce' ), $lastDate );
			echo '. ' . __( 'All product changes will be <strong>re-indexed automatically</strong>', 'ajax-search-for-woocommerce' );
			?>.

        </span>
	<?php endif; ?>

	<?php if ( in_array( $status, array( 'error' ) ) ): ?>
		<div class="dgwt-wcas-indexing-header__troubleshooting">
			<h3><?php _e( 'Troubleshooting', 'ajax-search-for-woocommerce' ); ?></h3>
			<ol>
				<li><?php printf( __( 'First try to <a class="%s" href="#">build the index again</a>', 'ajax-search-for-woocommerce' ), 'js-ajax-build-index' ); ?></li>
				<li><?php printf( __( 'Go to <a target="_blank" href="%s">WooCommerce -> Status -> section "%"</a>. Is everything green?', 'ajax-search-for-woocommerce' ), admin_url( 'admin.php?page=wc-status#asfw-requirements' ), DGWT_WCAS_NAME ); ?></li>
				<li><?php printf( __( 'Go to <a target="_blank" href="%s">WooCommerce -> Status -> Logs (tab)</a>. Open last "fatal-errors...". Look for a phrase "ajax-search-for-woocommerce". Did you find anything significant?', 'ajax-search-for-woocommerce' ),
						admin_url( 'admin.php?page=wc-status&tab=logs' ) ); ?></li>
				<li> <?php printf( __( 'Is it still not working? Write a <a target="_blank" href="%s">support request</a>.', 'ajax-search-for-woocommerce' ), dgoraAsfwFs()->contact_url() ); ?></li>
			</ol>
		</div>
	<?php endif; ?>

	<div class="dgwt-wcas-indexing-header__actions">
		<?php echo $actionButton; ?>

		<?php if ( ! in_array( $status, array( 'building', 'cancellation' ) ) ): ?>
			<a href="#" class="<?php echo $isDetails ? 'hide' : 'show'; ?> dgwt-wcas-indexing-details-trigger js-dgwt-wcas-indexing-details-trigger js-dgwt-wcas-indexing__showd"><?php _e( 'Show details', 'ajax-search-for-woocommerce' ); ?></a>
			<a href="#" class="<?php echo $isDetails ? 'show' : 'hide'; ?> dgwt-wcas-indexing-details-trigger js-dgwt-wcas-indexing-details-trigger js-dgwt-wcas-indexing__hided"><?php _e( 'Hide details', 'ajax-search-for-woocommerce' ); ?></a>
		<?php endif; ?>
	</div>

