<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Variation;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;
use DgoraWcas\Multilingual;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Indexer {

	private $parentProductID;
	private $parentProductSKU;
	private $variations = array();
	private $lang = '';

	public function __construct( $parentProductID = 0, $parentProductSKU = '', $variations = array(), $lang = '' ) {

		$this->parentProductID  = absint( $parentProductID );
		$this->parentProductSKU = $parentProductSKU;
		$this->variations       = is_array( $variations ) ? $variations : array();
		$this->lang             = Multilingual::isLangCode( $lang ) ? $lang : Multilingual::getDefaultLanguage();

	}

	/**
	 * Index variation info if necessary
	 *
	 * @return void
	 */
	public function maybeIndex() {
		global $wpdb;

		if ( empty( $this->variations ) || ! is_array( $this->variations ) ) {
			return;
		}

		$rows = array();

		foreach ( $this->variations as $variation ) {
			if ( is_array( $variation ) && ! empty( $variation['sku'] ) ) {

				// Empty variation SKU? return
				if ( ! empty( $this->parentProductSKU ) && $this->parentProductSKU === $variation['sku'] ) {
					continue;
				}

				$variationObj = new \WC_Product_Variation( $variation['variation_id'] );

				$url = $variationObj->get_permalink();
				if ( Multilingual::isMultilingual() ) {
					$url = Multilingual::getPermalink( $this->parentProductID, $variationObj->get_permalink(), $this->lang );
				}

				$variationID = absint( $variationObj->get_id() );

				$title          = (string) $variationObj->get_title();
				$variationAttrs = (string) wc_get_formatted_variation( $variationObj, true, false, false );
				if ( ! empty( $variationAttrs ) ) {
					$title .= ', ' . $variationAttrs;
				}

				$rows[] = array(
					'variation_id' => $variationID,
					'product_id'   => $this->parentProductID,
					'sku'          => (string) $variationObj->get_sku(),
					'title'        => apply_filters( 'dgwt/wcas/variation/title', $title, $variationObj ),
					'description'  => (string) $variationObj->get_description(),
					'image'        => $this->prepareImageSrc( $variationObj->get_image_id(), $variationID ),
					'url'          => apply_filters( 'dgwt/wcas/variation/permalink', $url, $variationObj ),
					'html_price'   => $variationObj->get_price_html(),
					'lang'         => $this->lang
				);

			}
		}

		if ( ! empty( $rows ) ) {

			$sql = "INSERT INTO $wpdb->dgwt_wcas_var_index (
                variation_id,
                product_id,
                sku,
                title,
                description,
                image,
                url,
                html_price,
                lang) VALUES ";

			$i = 1;
			foreach ( $rows as $row ) {
				$sql .= $wpdb->prepare( "(%d, %d, %s, %s, %s, %s, %s, %s, %s)", $row );
				$sql .= count( $rows ) === $i ? ';' : ',';

				$i ++;
			}

			$wpdb->query( $sql );

		}

		do_action( 'dgwt/wcas/variation_index/after_insert', $rows, $this->parentProductID, $this->parentProductSKU, $this->lang );
	}

	/**
	 * Preoare variation Image SCR
	 *
	 * @param int $imageID
	 * @param int $variationID
	 *
	 * @return string
	 */
	public function prepareImageSrc( $imageID, $variationID ) {

		$src = '';

		if ( ! empty( $imageID ) ) {
			$imageSrc = wp_get_attachment_image_src( $imageID, 'dgwt-wcas-product-suggestion' );

			if ( is_array( $imageSrc ) && ! empty( $imageSrc[0] ) ) {
				$src = $imageSrc[0];
			}
		}

		if ( empty( $src ) ) {
			$src = wc_placeholder_img_src();
		}

		return apply_filters( 'dgwt/wcas/variation/thumbnail_src', $src, $this->parentProductID, $variationID );
	}

	/**
	 * Wipe index
	 *
	 * @return bool
	 */
	public function wipe() {
		Database::remove();
		Builder::log( _x( '[Variations index] Cleared', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		return true;
	}

}
