<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Taxonomy;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Vendor;
use DgoraWcas\Helpers;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AsyncProcess extends \WP_Background_Process {

	const CANCELING_PROCESS_TRANSIENT = 'dgwt_wcas_canceling_r_index_build';

	/**
	 * @var string
	 */
	protected $prefix = 'wcas';

	/**
	 * @var string
	 */
	protected $action = 'build_readable_index';

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over
	 *
	 * @return mixed
	 */
	public function task( $itemsSet ) {
		if ( ! defined( 'DGWT_WCAS_READABLE_INDEX_TASK' ) ) {
			define( 'DGWT_WCAS_READABLE_INDEX_TASK', true );
		}

		do_action( 'dgwt/wcas/readable_index/bg_processing/before_task' );

		$indexer = new Indexer;

		foreach ( $itemsSet as $itemID ) {

			if ( version_compare( PHP_VERSION, '7.0' ) < 0 ) {

				$indexer->insert( $itemID );

			} else {
				try {
					$indexer->insert( $itemID );
				} catch ( \Error $e ) {
					$errorString = $e->getMessage() . ' | ' . $e->getFile() . ' Line: ' . $e->getLine();

					Helpers::WCLog( 'error', $errorString );
					Builder::addInfo( 'status', 'error' );
					Builder::log( 'Error: ' . $errorString, true );
					Builder::log( 'Stop building the index. Starting the cancellation process.' );
					Builder::cancelBuildIndex();
					sleep( 5 );
					exit();
				}

			}

		}

		return false;
	}

	public function is_other_process() {
		return ! $this->is_queue_empty();
	}

	/**
	 * Cancel Process
	 *
	 * Stop processing queue items, clear cronjob and delete batch.
	 *
	 * @return void
	 */
	public function cancel_process() {


		if ( ! $this->is_queue_empty() ) {

			$batch = $this->get_batch();

			$this->delete( $batch->key );

			wp_clear_scheduled_hook( $this->cron_hook_identifier );

		}
	}


	/**
	 * Delete queue
	 *
	 * @param string $key Key.
	 *
	 * @return $this
	 */
	public function delete( $key ) {
		if ( delete_site_option( $key ) ) {
			Builder::log( sprintf( _x( '[Readable index] The queue <code>%s</code> was deleted ', 'Admin, logs', 'ajax-search-for-woocommerce' ), $key ) );
		};

		return $this;
	}

	/**
	 * Schedule event
	 */
	protected function schedule_event() {
		if ( ! wp_next_scheduled( $this->cron_hook_identifier ) ) {
			if ( wp_schedule_event( time(), $this->cron_interval_identifier, $this->cron_hook_identifier ) !== false ) {
				Builder::log( sprintf( _x( '[Readable index] Schedule <code>%s</code> was created ', 'Admin, logs', 'ajax-search-for-woocommerce' ),
					$this->cron_hook_identifier ) );
			}
		}
	}

	/**
	 * Save queue
	 *
	 * @return $this
	 */
	public function save() {
		$key = $this->generate_key();

		if ( ! empty( $this->data ) ) {
			update_site_option( $key, $this->data );
			Builder::log( sprintf( _x( '[Readable index] The queue <code>%s</code> was created', 'Admin, logs', 'ajax-search-for-woocommerce' ), $key ) );

		}

		return $this;
	}

	/**
	 * Dispatch job is queue is not empty
	 */
	public function maybe_dispatch() {
		if ( $this->is_queue_empty() ) {
			$this->complete();
		} else {
			$this->dispatch();
		}
	}

	/**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	public function complete() {
		parent::complete();

		Builder::addInfo( 'end_readable_ts', time() );
		Builder::log( _x( '[Readable index] Completed', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		// Build terms index if necessary
		if ( Builder::canBuildTaxonomyIndex() ) {
			Taxonomy\Database::create();
			Taxonomy\Request::handle();
		}

		// Build vendor index if necessary
		if ( Builder::canBuildVendorsIndex() ) {
			Vendor\Database::create();
			Vendor\Request::handle();
		}

		sleep( 1 );
		Builder::maybeMarkAsCompleted();

	}
}
