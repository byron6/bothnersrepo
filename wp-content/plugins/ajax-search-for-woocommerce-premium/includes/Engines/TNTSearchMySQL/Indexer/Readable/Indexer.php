<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable;

use DgoraWcas\Helpers;
use DgoraWcas\Multilingual;
use DgoraWcas\Post;
use DgoraWcas\Product;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Variation\Indexer as VariationIndexer;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Indexer {

	private $product;
	private $post;

	public function __construct() {

	}

	/**
	 * Insert post to the index
	 *
	 * @param int post ID
	 *
	 * @return bool true on success
	 */
	public function insert( $postID ) {
		global $wpdb;
		$success = false;

		$postType = get_post_type( $postID );

		if ( $postType === 'product' ) {

			$this->product = new Product( $postID );

			// Support for multilingual
			if ( Multilingual::isMultilingual() ) {

				$lang = $this->product->getLanguage();
				if ( ! empty( $lang ) && $lang !== Multilingual::getCurrentLanguage() ) {
					Multilingual::switchLanguage( $lang );
				}

				if ( Multilingual::isMultiCurrency() ) {
					Multilingual::setCurrentCurrency( $this->product->getCurrency() );
				}
			}


			$data = $this->getProductData();

			if ( $this->product->isType( 'variable' ) && DGWT_WCAS()->settings->getOption( 'search_in_product_sku' ) === 'on' ) {
				$variantionsIndexer = new VariationIndexer( $this->product->getID(), $this->product->getSKU(), $this->product->getAvailableVariations(), $this->product->getLanguage() );
				$variantionsIndexer->maybeIndex();
			}

		} else {
			$this->post = new Post( $postID, $postType );

			// Support for multilingual
			if ( Multilingual::isMultilingual() ) {

				$lang = $this->post->getLanguage();
				if ( ! empty( $lang ) && $lang !== Multilingual::getCurrentLanguage() ) {
					Multilingual::switchLanguage( $lang );
				}
			}

			$data = $this->getNonProductData();
		}

		if ( ! empty( $data ) ) {

			apply_filters( 'dgwt/wcas/readable_index/insert', $data, $postID, $postType );

			$rows = $wpdb->insert(
				$wpdb->dgwt_wcas_index,
				$data,
				array(
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%f',
					'%f',
					'%d',
					'%d',
					'%s',
				)
			);

			if ( is_numeric( $rows ) ) {
				$success = true;
			}

		}

		do_action( 'dgwt/wcas/readable_index/after_insert', $data, $postID, $postType, $success );

		return $success;

	}

	/**
	 * Get product data to save
	 *
	 * @return array
	 */
	private function getProductData() {
		$data = array();

		if ( is_object( $this->product ) && $this->product->isValid() ) {

			$wordsLimit = - 1;
			if ( DGWT_WCAS()->settings->getOption( 'show_details_box' ) === 'on' ) {
				$wordsLimit = 15;
			}

			$data = array(
				'post_id'        => $this->product->getID(),
				'created_date'   => get_post_field( 'post_date', $this->product->getID(), 'raw' ),
				'name'           => $this->product->getName(),
				'description'    => $this->product->getDescription( 'suggestions', $wordsLimit ),
				'sku'            => $this->product->getSKU(),
				'sku_variations' => '',
				'attributes'     => '',
				'image'          => $this->product->getThumbnailSrc(),
				'url'            => $this->product->getPermalink(),
				'html_price'     => $this->product->getPriceHTML(),
				'price'          => $this->product->getPrice(),
				'average_rating' => $this->product->getAverageRating(),
				'review_count'   => $this->product->getReviewCount(),
				'total_sales'    => $this->product->getTotalSales(),
				'lang'           => $this->product->getLanguage()
			);

			if ( apply_filters( 'dgwt/wcas/tnt/indexer/readable/process_sku_variations', true ) ) {
				$data['sku_variations'] = implode( '|', $this->product->getVariationsSKUs() );
			}

			if ( apply_filters( 'dgwt/wcas/tnt/indexer/readable/process_attributes', true ) ) {
				$data['attributes'] = implode( '|', $this->product->getAttributes( true ) );
			}
		}

		return $data;
	}


	/**
	 * Get post or pages data to save
	 *
	 * @return array
	 */
	private function getNonProductData() {
		$data = array();

		if ( is_object( $this->post ) && $this->post->isValid() ) {
			$data = array(
				'post_id'        => $this->post->getID(),
				'created_date'   => get_post_field( 'post_date', $this->post->getID(), 'raw' ),
				'name'           => $this->post->getTitle(),
				'description'    => $this->post->getDescription( 'short' ),
				'sku'            => '',
				'sku_variations' => '',
				'attributes'     => '',
				'image'          => '',
				'url'            => $this->post->getPermalink(),
				'html_price'     => '',
				'price'          => '',
				'average_rating' => '',
				'review_count'   => '',
				'total_sales'    => '',
				'lang'           => $this->post->getLanguage()
			);
		}

		return $data;
	}

	/**
	 * Update product
	 *
	 * @param post ID
	 *
	 * @return bool true on success
	 */
	public function update( $postID ) {
		$this->delete( $postID );
		$this->insert( $postID );
	}

	/**
	 * Remove record from the index
	 *
	 * @param int postID
	 *
	 * @return bool true on success
	 */
	public function delete( $postID ) {
		global $wpdb;

		$success = $wpdb->delete(
			$wpdb->dgwt_wcas_index,
			array( 'post_id' => $postID ),
			array( '%d' )
		);

		// Variables if exist
		if ( Helpers::isTableExists( $wpdb->dgwt_wcas_var_index ) ) {
			$varDelete = $wpdb->delete(
				$wpdb->dgwt_wcas_var_index,
				array( 'product_id' => $postID ),
				array( '%d' )
			);
			$success   = $success && $varDelete;
		}

		return (bool) $success;

	}

	/**
	 * Wipe index
	 *
	 * @return bool
	 */
	public function wipe() {
		Database::remove();
		Builder::log( _x( '[Readable index] Cleared', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		return true;
	}

}
