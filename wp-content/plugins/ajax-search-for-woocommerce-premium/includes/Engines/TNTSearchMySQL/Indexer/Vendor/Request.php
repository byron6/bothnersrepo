<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Vendor;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Request {

	public static function handle() {

		@set_time_limit( 3600 );

		self::build();

		return false;
	}

	private static function build() {
		Builder::log( _x( '[Vendors index] Building...', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		$indexer = new Indexer;
		$status = $indexer->build();

		$error = false;
		if (
			( empty( $status['success'] ) && empty( $status['error'] ) )
			|| $status['error'] > 0
		) {
			$error = true;
		}
		$text = _x( '[Vendors index] Completed: %s, Not indexed: %s', 'Admin, logs', 'ajax-search-for-woocommerce' );
		Builder::log( sprintf( $text, $status['success'], $status['error'] ), $error );
	}

}
