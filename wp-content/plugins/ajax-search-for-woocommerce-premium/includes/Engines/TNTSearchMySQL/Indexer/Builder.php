<?php


namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable\Indexer as IndexerR;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable\Indexer as IndexerS;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Taxonomy\Indexer as IndexerTax;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Vendor\Indexer as IndexerVendors;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Variation\Indexer as IndexerVar;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable\Database as DatabaseS;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable\Database as DatabaseR;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Variation\Database as DatabaseVar;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable\AsyncProcess as AsyncProcessS;
use DgoraWcas\Helpers;
use DgoraWcas\Multilingual;

class Builder {

	const LAST_BUILD_OPTION_KEY = 'dgwt_wcas_indexer_last_build';
	const DETAILS_DISPLAY_KEY = 'dgwt_wcas_indexer_details_display';
	const INDEXING_PROCESS_EXIST_KEY = 'dgwt_wcas_indexer_process_exist';


	/**
	 * Structure of indexer data
	 *
	 * @return array
	 */
	private static function getIndexInfoStruct() {
		return array(
			'build_id'                        => uniqid(),
			'db'                              => 'MySQL',
			'status'                          => '',
			'start_ts'                        => time(),
			'start_searchable_ts'             => 0,
			'start_readable_ts'               => 0,
			'start_taxonomies_ts'             => 0,
			'end_ts'                          => 0,
			'end_searchable_ts'               => 0,
			'end_readable_ts'                 => 0,
			'end_taxonomies_ts'               => 0,
			'searchable_processed'            => 0,
			'total_products_for_indexing'     => 0,
			'total_non_products_for_indexing' => 0,
			'logs'                            => array()
		);
	}

	/**
	 * @return bool
	 */
	private static function createInfoStruct() {
		return update_option( self::LAST_BUILD_OPTION_KEY, self::getIndexInfoStruct() );
	}

	/**
	 * Add specific info about the last index build
	 *
	 * @return bool
	 */
	public static function addInfo( $key, $value ) {
		$added = false;

		$lastInfo = self::getLastBuildInfo();

		if ( array_key_exists( $key, $lastInfo ) ) {
			$lastInfo[ $key ] = $value;
			$added            = update_option( self::LAST_BUILD_OPTION_KEY, $lastInfo );
		}

		return $added;
	}

	public static function getInfo( $key ) {

		$value    = '';
		$lastInfo = self::getLastBuildInfo();

		if ( array_key_exists( $key, $lastInfo ) ) {
			$value = $lastInfo[ $key ];
		}

		return $value;
	}

	/**
	 * Save log for the indexer
	 *
	 * @param string $message
	 *
	 * @return bool
	 */
	public static function log( $message, $error = false ) {
		$added = false;

		if ( defined( 'DGWT_WCAS_DISABLE_INDEXER_LOGS' ) && DGWT_WCAS_DISABLE_INDEXER_LOGS ) {
			return false;
		}

		$lastInfo = self::getLastBuildInfo();

		if ( ! array_key_exists( 'logs', $lastInfo ) ) {
			return false;
		}

		if ( is_array( $lastInfo['logs'] ) ) {
			$lastInfo['logs'][] = array(
				'time'    => time(),
				'error'   => $error,
				'message' => $message
			);

			$added = update_option( self::LAST_BUILD_OPTION_KEY, $lastInfo );
		}

		return $added;

	}

	/**
	 * Get all logs
	 *
	 * @return array
	 */
	public static function getLogs() {

		$logs     = array();
		$lastInfo = self::getLastBuildInfo();

		if ( ! empty( $lastInfo['logs'] ) ) {
			$logs = $lastInfo['logs'];
		}

		return $logs;
	}

	public static function buildIndex() {

		if ( get_transient( self::INDEXING_PROCESS_EXIST_KEY ) ) {
			self::log( _x( 'Indexer already running', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

			return;
		}

		set_transient( self::INDEXING_PROCESS_EXIST_KEY, true, 5 );

		self::createInfoStruct();

		self::cancelBuildIndex( false );

		self::addInfo( 'status', 'building' );

		self::wipeActionScheduler();

		if ( Multilingual::isMultilingual() ) {
			$mlLogMesssage = _x( 'Multilingual: Yes, Provider: %s, Default: %s, Langs: %s', 'Admin, logs', 'ajax-search-for-woocommerce' );
			$provider      = Multilingual::getProvider();
			self::log( sprintf( $mlLogMesssage, $provider, Multilingual::getDefaultLanguage(), implode( ',', Multilingual::getLanguages() ) ) );
		}

		self::log( _x( 'Indexer started', 'Admin, logs', 'ajax-search-for-woocommerce' ) );
		do_action( 'dgwt/wcas/indexer/started' );

		$source = new SourceQuery( array( 'ids' => true ) );

		$productsSet = array();
		$products    = $source->getData();

		self::addInfo( 'total_products_for_indexing', count( $products ) );

		// Readable
		DatabaseR::create();
		self::addInfo( 'start_readable_ts', time() );
		self::log( _x( '[Readable index] Building...', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		if ( self::canBuildVariationsIndex() ) {
			DatabaseVar::create();
		}

		// Searchable
		DatabaseS::create();
		self::addInfo( 'start_searchable_ts', time() );
		self::log( _x( '[Searchable index] Building...', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		$smallProductsSetCount = apply_filters('dgwt/wcas/indexer/readable-set-items-count', 25);
		$productsSetCount = apply_filters('dgwt/wcas/indexer/searchable-set-items-count', 100);

		if ( defined( 'DGWT_WCAS_BUILD_INDEX_SYNC' ) && DGWT_WCAS_BUILD_INDEX_SYNC ) {
			$productsSet = wp_list_pluck($products, 'ID');
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->task( $productsSet );
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->task( $productsSet );
		} else {
			$i = 0;
			foreach ( $products as $row ) {
				$productsSet[]      = $row['ID'];
				$smallProductsSet[] = $row['ID'];

				if ( count( $smallProductsSet ) === $smallProductsSetCount || $i + 1 === count( $products ) ) {
					DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->push_to_queue( $smallProductsSet );
					$smallProductsSet = array();
				}

				if ( count( $productsSet ) === $productsSetCount || $i + 1 === count( $products ) ) {
					DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->push_to_queue( $productsSet );
					$productsSet = array();
				}

				$i ++;
			}
		}

		// Non-product search
		$types = Helpers::getAllowedPostTypes( 'no-products' );
		if ( ! empty( $types ) ) {
			$totalNonProducts = 0;
			foreach ( $types as $type ) {

				$npSource = new PostsSourceQuery( array(
					'ids'       => true,
					'postTypes' => array( $type )
				) );
				$posts    = $npSource->getData();

				$totalNonProducts = $totalNonProducts + count( $posts );

				$nonProductsSet      = array();
				$smallNonProductsSet = array();

				if ( defined( 'DGWT_WCAS_BUILD_INDEX_SYNC' ) && DGWT_WCAS_BUILD_INDEX_SYNC ) {
					$nonProductsSet = wp_list_pluck( $posts, 'ID' );
					DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->task( $nonProductsSet );
					DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->task( $nonProductsSet );
				} else {
					$i = 0;
					foreach ( $posts as $row ) {
						$nonProductsSet[]      = $row['ID'];
						$smallNonProductsSet[] = $row['ID'];

						if ( count( $smallNonProductsSet ) === $smallProductsSetCount || $i + 1 === count( $posts ) ) {
							DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->push_to_queue( $smallNonProductsSet );
							$smallNonProductsSet = array();
						}

						if ( count( $nonProductsSet ) === $productsSetCount || $i + 1 === count( $posts ) ) {
							DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->push_to_queue( $nonProductsSet );
							$nonProductsSet = array();
						}

						$i ++;
					}
				}
			}

			self::addInfo( 'total_non_products_for_indexing', $totalNonProducts );
		}

		if ( defined( 'DGWT_WCAS_BUILD_INDEX_SYNC' ) && DGWT_WCAS_BUILD_INDEX_SYNC ) {
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->complete();
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->complete();
		} else {
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->save()->maybe_dispatch();
			sleep( 1 );
			DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->save()->maybe_dispatch();
		}
	}

	/**
	 * Stops build index and wipes all processes and data
	 *
	 * @param bool $clearInfo clear info (start time of processes)
	 *
	 * @return void
	 */
	public static function cancelBuildIndex( $clearInfo = true ) {
		$indexerR = new IndexerR;
		$indexerS = new IndexerS;

		DGWT_WCAS()->tntsearchMySql->asynchBuildIndexR->cancel_process();
		DGWT_WCAS()->tntsearchMySql->asynchBuildIndexS->cancel_process();

		$indexerS->wipe();

		if ( self::readableIndexExists() ) {
			$indexerR->wipe();
		}

		if ( self::taxIndexExists() ) {
			$taxIndexer = new IndexerTax();
			$taxIndexer->wipe();
		}

		if ( self::vendorsIndexExists() ) {
			$vendorsIndexer = new IndexerVendors();
			$vendorsIndexer->wipe();
		}

		if ( self::variationsIndexExists() ) {
			$varIndexer = new IndexerVar();
			$varIndexer->wipe();
		}

		Helpers::removeBatchOptions__premium_only();
		delete_option( AsyncProcessS::CURRENT_ITEMS_SET_OPTION_KEY );

		if ( $clearInfo ) {
			self::addInfo( 'start_searchable_ts', '' );
			self::addInfo( 'start_readable_ts', '' );
			self::addInfo( 'start_taxonomies_ts', '' );
		}
	}

	public static function getReadableProgress() {
		global $wpdb;

		$percent    = 0;
		$totalItems = self::getInfo( 'total_products_for_indexing' );

		if ( ! empty( Helpers::getAllowedPostTypes( 'no-products' ) ) ) {
			$npTotalItems = self::getInfo( 'total_non_products_for_indexing' );
			if ( is_numeric( $totalItems ) && is_numeric( $npTotalItems ) && ! empty( $npTotalItems ) ) {
				$totalItems += $npTotalItems;
			}
		}

		if ( self::readableIndexExists() ) {
			$totalIndexed = $wpdb->get_var( 'SELECT COUNT(DISTINCT post_id) FROM ' . $wpdb->dgwt_wcas_index );
		}

		if (
			! empty( $totalItems )
			&& is_numeric( $totalItems )
			&& ! empty( $totalIndexed )
			&& is_numeric( $totalIndexed )
		) {
			$percent = $totalIndexed * 100 / $totalItems;
		}

		return absint( $percent );
	}

	public static function getSearchableProgress() {

		$percent    = 0;
		$totalItems = self::getInfo( 'total_products_for_indexing' );

		if ( ! empty( Helpers::getAllowedPostTypes( 'no-products' ) ) ) {
			$npTotalItems = self::getInfo( 'total_non_products_for_indexing' );
			if ( is_numeric( $totalItems ) && is_numeric( $npTotalItems ) && ! empty( $npTotalItems ) ) {
				$totalItems += $npTotalItems;
			}
		}

		$processed = self::getInfo( 'searchable_processed' );

		if (
			! empty( $totalItems )
			&& is_numeric( $totalItems )
			&& ! empty( $processed )
			&& is_numeric( $processed )
		) {
			$percent = $processed * 100 / $totalItems;
		}

		return absint( $percent );
	}


	public static function getProgressBarValue() {

		if ( self::getInfo( 'status' ) === 'completed' ) {
			return 100;
		}

		$percentR = self::getReadableProgress();
		$percentS = self::getSearchableProgress();

		return apply_filters('dgwt/wcas/indexer/process_status/progress', ( $percentR + $percentS ) / 2, $percentR, $percentS);
	}

	public static function renderIndexingStatus() {
		self::refreshStatus();

		$html = '<div class="js-dgwt-wcas-indexing-wrapper">';
		$html .= self::getIndexHeader();
		$html .= self::getProcessStatus();
		$html .= '</div>';

		return $html;
	}

	public static function refreshStatus() {
		global $wpdb;

		$status = self::getInfo( 'status' );

		$startTs    = self::getInfo( 'start_ts' );
		$sStartTs   = self::getInfo( 'start_searchable_ts' );
		$rStartTs   = self::getInfo( 'start_readable_ts' );
		$taxStartTs = self::getInfo( 'start_taxonomies_ts' );
		$sEndTs     = self::getInfo( 'end_searchable_ts' );
		$rEndTs     = self::getInfo( 'end_readable_ts' );
		$taxEndTs   = self::getInfo( 'end_taxonomies_ts' );

		switch ( $status ) {
			case 'building':

				if ( ( time() - $startTs ) > 120 && empty( $sStartTs ) ) {
					self::addInfo( 'status', 'error' );
					self::log( 'The searchable index did not start', true );
					do_action( 'dgwt/wcas/indexer/status/error' );
					break;
				}

				// Nie udało się zakończyć budowania indeksu searchable 5 godzin po zakończeniu readable
				if ( ! empty( $rEndTs ) && empty( $sEndTs ) && ( time() - $rEndTs ) > 18000 ) {
					self::addInfo( 'status', 'error' );
					self::log( 'The searchable index did not end', true );
					do_action( 'dgwt/wcas/indexer/status/error' );
					break;
				}

				//@TODO Obsługa innych scenariuszy, jest 100%, ale nie zapisał się czas "ts_end"

				break;
			case 'cancellation':

				sleep( 2 );

				self::addInfo( 'status', 'not-exist' );
				self::log( 'Canceling completed' );

				break;
			case 'error':

				self::cancelBuildIndex();

				break;
		}

	}

	public static function getIndexHeader() {
		$text             = '';
		$statusColor      = '';
		$statusText       = '';
		$status           = self::getInfo( 'status' );
		$endTs            = self::getInfo( 'end_ts' );
		$totalProducts    = self::getInfo( 'total_products_for_indexing' );
		$totalNonProducts = self::getInfo( 'total_non_products_for_indexing' );
		switch ( $status ) {
			case 'building':
				$text        = __( 'Wait... Indexing in progress', 'ajax-search-for-woocommerce' );
				$statusText  = __( 'This process will continue in the background. You can leave this page!',
					'ajax-search-for-woocommerce' );
				$statusColor = '#e6a51d';
				break;
			case 'cancellation':
				$text        = __( 'Wait... The index build process is canceling', 'ajax-search-for-woocommerce' );
				$statusText  = __( 'Canceling...', 'ajax-search-for-woocommerce' );
				$statusColor = '#7293b0';
				break;
			case 'completed':
				$lastDate    = ! empty( $endTs ) ? Helpers::localDate( $endTs ) : '-';
				$text        = __( 'The search index was built successfully.', 'ajax-search-for-woocommerce' );
				$statusText  = __( 'Completed. Works.', 'ajax-search-for-woocommerce' );
				$statusColor = '#4caf50';
				break;
			case 'error':
				$text        = __( 'The search index could not be built.', 'ajax-search-for-woocommerce' );
				$statusText  = __( 'Errors', 'ajax-search-for-woocommerce' );
				$statusColor = '#d75f5f';
				break;
			default:
				$text        = __( 'The search index does not exist yet. Build it now.', 'ajax-search-for-woocommerce' );
				$statusText  = __( 'Not exist', 'ajax-search-for-woocommerce' );
				$statusColor = '#aaaaaa';
				break;
		}

		$actionButton = self::getIndexButton();
		$isDetails    = get_transient( self::DETAILS_DISPLAY_KEY );
		$status       = self::getInfo( 'status' );

		ob_start();
		include DGWT_WCAS_DIR . 'partials/admin/indexer-header.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public static function getIndexButton() {
		$status = self::getInfo( 'status' );

		if ( in_array( $status, array( 'building' ) ) ) {
			$html = '<a class="button js-ajax-stop-build-index" href="#">' . __( 'Stop process',
					'ajax-search-for-woocommerce' ) . '</a>';
		} elseif ( in_array( $status, array( 'completed' ) ) ) {
			$html = '<a class="button js-ajax-build-index" href="#">' . __( 'Rebuild index',
					'ajax-search-for-woocommerce' ) . '</a>';
		} elseif ( in_array( $status, array( 'error', 'cancellation' ) ) ) {
			$html = '<a class="button js-ajax-build-index" href="#">' . __( 'Try to build the index again.',
					'ajax-search-for-woocommerce' ) . '</a>';
		} else {
			$html = '<a class="button js-ajax-build-index ajax-build-index-primary" href="#">' . __( 'Build index',
					'ajax-search-for-woocommerce' ) . '</a>';

		}

		return $html;
	}

	/**
	 * Check if readable products table exist
	 *
	 * @return bool
	 */
	public static function readableIndexExists() {
		global $wpdb;

		return Helpers::isTableExists( $wpdb->dgwt_wcas_index );
	}

	/**
	 * Check if readable taxonmies table exist
	 *
	 * @return bool
	 */
	public static function taxIndexExists() {
		global $wpdb;

		return Helpers::isTableExists( $wpdb->dgwt_wcas_tax_index );
	}

	/**
	 * Check if variations table exist
	 *
	 * @return bool
	 */
	public static function variationsIndexExists() {
		global $wpdb;

		return Helpers::isTableExists( $wpdb->dgwt_wcas_var_index );
	}

	/**
	 * Check if vendors table exist
	 *
	 * @return bool
	 */
	public static function vendorsIndexExists() {
		global $wpdb;

		return Helpers::isTableExists( $wpdb->dgwt_wcas_ven_index );
	}

	/**
	 * Check if searchable tables exist
	 *
	 * @param string $currentLang
	 *
	 * @return bool
	 */
	public static function searchableIndexExists($currentLang = '') {
		global $wpdb;
		$isShortInit = defined('SHORTINIT') && SHORTINIT;
		$wordlistExists = false;
		$doclistExists  = false;

		$currentLang = Multilingual::isLangCode($currentLang) ? $currentLang : '';

		$wpdb->hide_errors();

		ob_start();

		$infoExists = Helpers::isTableExists( $wpdb->dgwt_wcas_si_info );

		if ( ! empty( $currentLang ) || ( ! $isShortInit && Multilingual::isMultilingual() ) ) {
			$wordlistInstances = 0;
			$doclistInstances  = 0;

			if ( ! empty( $currentLang ) ) {
				$langs = array( $currentLang );
			}else{
				$langs = Multilingual::getLanguages();
			}

			foreach ( $langs as $lang ) {

				$lang = str_replace('-', '_', $lang);

				$wordlistTable = $wpdb->dgwt_wcas_si_wordlist . '_' . $lang;
				$doclistTable  = $wpdb->dgwt_wcas_si_doclist . '_' . $lang;

				if ( Helpers::isTableExists( $wordlistTable ) ) {
					$wordlistInstances ++;
				}

				if ( Helpers::isTableExists( $doclistTable ) ) {
					$doclistInstances ++;
				}

			}

			if ( $wordlistInstances === count( $langs ) ) {
				$wordlistExists = true;
			}

			if ( $doclistInstances === count( $langs ) ) {
				$doclistExists = true;
			}

		} else {
			$wordlistExists = Helpers::isTableExists( $wpdb->dgwt_wcas_si_wordlist );
			$doclistExists  = Helpers::isTableExists( $wpdb->dgwt_wcas_si_doclist );
		}

		ob_end_clean();


		return $wordlistExists && $doclistExists && $infoExists;
	}


	public static function getReadableTotalIndexed() {
		global $wpdb;
		$count = 0;

		if ( self::readableIndexExists() ) {
			$r = $wpdb->get_var( 'SELECT COUNT(DISTINCT post_id) FROM ' . $wpdb->dgwt_wcas_index );
			if ( ! empty( $r ) && is_numeric( $r ) ) {
				$count = absint( $r );
			}
		}


		return $count;
	}

	public static function getProcessStatus() {

		$info = array();
		foreach ( self::getIndexInfoStruct() as $key => $field ) {
			$offset = get_option( 'gmt_offset' );
			$value  = self::getInfo( $key );
			if ( strpos( $key, '_ts' ) !== false && ! empty( $value ) && ! empty( $offset ) ) {
				$info[ $key ] = $value + ( $offset * 3600 );
			} else {
				$info[ $key ] = $value;
			}
		}

		$progressPercent       = self::getProgressBarValue();
		$logs                  = self::getLogs();
		$isDetails             = get_transient( self::DETAILS_DISPLAY_KEY );
		$canBuildTaxonomyIndex = self::canBuildTaxonomyIndex();
		$canBuildVendorsIndex  = self::canBuildVendorsIndex();

		ob_start();
		include DGWT_WCAS_DIR . 'partials/admin/indexer-body.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	/**
	 * Check if can index taxonomies
	 *
	 * @return bool
	 */
	public static function canBuildTaxonomyIndex() {
		$showTerms = false;

		if ( DGWT_WCAS()->settings->getOption( 'show_matching_categories' ) === 'on'
		     || DGWT_WCAS()->settings->getOption( 'show_matching_tags' ) === 'on'
		     || DGWT_WCAS()->settings->getOption( 'show_matching_brands' ) === 'on'
		) {
			$showTerms = true;
		}

		return $showTerms;
	}

	/**
	 * Check if can index vendors
	 *
	 * @return bool
	 */
	public static function canBuildVendorsIndex() {
		return apply_filters( 'dgwt/wcas/search/vendors', false );
	}

	/**
	 * Check if can build variations index
	 *
	 * @return bool
	 */
	public static function canBuildVariationsIndex() {
		$canBuild = false;

		if ( ! empty( $_POST['dgwt_wcas_settings'] ) ) {

			$settings = $_POST['dgwt_wcas_settings'];

			if ( ! empty( $settings['search_in_product_sku'] ) && $settings['search_in_product_sku'] === 'on' ) {
				$canBuild = true;
			}
		}

		if ( ! $canBuild ) {
			$canBuild = DGWT_WCAS()->settings->getOption( 'search_in_product_sku' ) === 'on';
		}

		return $canBuild;
	}

	/**
	 * Check if index is completed and valid
	 *
	 * @param string $lang
	 *
	 * @return bool
	 */
	public static function isIndexValid( $lang = '' ) {
		global $wpdb;
		$valid = false;

		if ( self::getInfo( 'status' ) === 'completed'
		     && self::searchableIndexExists( $lang )
		     && self::readableIndexExists()
		) {

			$info = $wpdb->get_var( "SELECT ivalue FROM $wpdb->dgwt_wcas_si_info WHERE ikey = 'stemmer'" );

			if ( ! empty( $info ) ) {
				$valid = true;
			}

		}

		return $valid;
	}

	public static function maintenanceOnInit() {

		if ( ! Helpers::isSettingsPage() ) {
			return;
		}

		$status = self::getInfo( 'status' );


		// Build index on start
		if ( empty( $status ) || $status === 'not-exist' ) {
			self::buildIndex();
		}

	}

	/**
	 * Get last index build info
	 *
	 * @return array
	 */
	public static function getLastBuildInfo() {
		global $wpdb;

		$data = array();

		$opt = $wpdb->get_var( $wpdb->prepare( "SELECT SQL_NO_CACHE option_value FROM $wpdb->options WHERE option_name = %s",
			self::LAST_BUILD_OPTION_KEY ) );

		if ( ! empty( $opt ) ) {
			$opt = @unserialize( $opt );
			if ( is_array( $opt ) ) {
				$data = $opt;
			}
		}

		return $data;

	}

	/**
	 * Wipe all data of deprecated SQLite driver
	 *
	 * @return void
	 */
	public static function wipeSQLiteAfterEffects() {

		$uploadDir = wp_upload_dir();
		if ( ! empty( $uploadDir['basedir'] ) ) {


			$directory = $uploadDir['basedir'] . '/wcas-search';
			$file      = $uploadDir['basedir'] . '/wcas-search/products.index';


			if ( file_exists( $file ) && is_writable( $file ) ) {
				@unlink( $file );
			}

			if ( file_exists( $directory ) && is_writable( $directory ) ) {
				@rmdir( $directory );
			}

		}

	}

	/**
	 * Complete the search index
	 *
	 * @return void
	 */
	public static function maybeMarkAsCompleted() {

		$status = self::getInfo( 'status' );
		$sEndTs = self::getInfo( 'end_searchable_ts' );
		$rEndTs = self::getInfo( 'end_readable_ts' );

		if ( 'building' === $status && ! empty( $sEndTs ) && ! empty( $rEndTs ) ) {
			self::addInfo( 'status', 'completed' );
			self::addInfo( 'end_ts', time() );
			self::log( 'Indexing completed' );
			do_action( 'dgwt/wcas/indexer/status/completed' );
		}

	}

	/**
	 * Remove all database tables created by this plugin
	 *
	 * @param bool $networkScope delete tables in whole network
	 *
	 * @return void
	 */
	public static function deleteIndexOptions( $networkScope = false ) {
		global $wpdb;

		$prefix = $wpdb->prefix;

		if ( is_multisite() && $networkScope ) {
			$prefix = $wpdb->base_prefix;
		}

		delete_option( self::LAST_BUILD_OPTION_KEY );
		delete_transient( self::DETAILS_DISPLAY_KEY );

		if ( is_multisite() && $networkScope ) {
			foreach ( get_sites() as $site ) {
				if ( is_numeric( $site->blog_id ) ) {

					$blogID = $site->blog_id == 1 ? '' : $site->blog_id . '_';

					$table = $prefix . $blogID . 'options';

					$wpdb->delete( $table, array( 'option_name' => self::LAST_BUILD_OPTION_KEY ) );

					$wpdb->delete( $table, array( 'option_name' => '_transient_timeout_' . self::DETAILS_DISPLAY_KEY ) );
					$wpdb->delete( $table, array( 'option_name' => '_transient_' . self::DETAILS_DISPLAY_KEY ) );

				}
			}
		}

	}

	/**
	 * Remove all database tables created by this plugin
	 *
	 * @param bool $networkScope delete tables in whole network
	 *
	 * @return void
	 */
	public static function deleteDatabaseTables( $networkScope = false ) {
		global $wpdb;

		// DB tables
		$tables = Utils::getAllPluginTables( $networkScope );

		if ( ! empty( $tables ) ) {
			foreach ( $tables as $table ) {
				$wpdb->query( "DROP TABLE IF EXISTS $table" );
			}
		}
	}

	/**
	 * Removal of planned actions that will update products in the index
	 */
	public static function wipeActionScheduler() {
		$queue = Utils::getQueue();
		if ( empty( $queue ) ) {
			return;
		}

		try {
			$queue->cancel_all( 'dgwt/wcas/tnt/background_product_update' );
		} catch ( Exception $e ) {

		}


	}
}
