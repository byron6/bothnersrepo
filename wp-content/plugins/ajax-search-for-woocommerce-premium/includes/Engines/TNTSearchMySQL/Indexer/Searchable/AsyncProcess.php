<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AsyncProcess extends \WP_Background_Process {

	const CURRENT_ITEMS_SET_OPTION_KEY = 'dgwt_wcas_s_index_current_items_set';

	/**
	 * @var string
	 */
	protected $prefix = 'wcas';

	/**
	 * @var string
	 */
	protected $action = 'build_searchable_index';

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over
	 *
	 * @return mixed
	 */
	public function task( $itemsSet ) {

		if ( ! defined( 'DGWT_WCAS_SEARCHABLE_INDEX_TASK' ) ) {
			define( 'DGWT_WCAS_SEARCHABLE_INDEX_TASK', true );
		}

		do_action( 'dgwt/wcas/searchable_index/bg_processing/before_task' );

		register_shutdown_function( function () {

			$error = error_get_last();

			if ( $error !== null ) {
				$errorString = $error['message'];
				$errorString .= ' File: ' . $error['file'];
				$errorString .= ' Line: ' . $error['line'];

				Builder::log( 'Error: ' . $errorString, true );


				if ( $error['type'] === E_ERROR ) {
					Builder::addInfo( 'status', 'error' );
					Builder::log( 'Stop building the index. Starting the cancellation process.' );
					Builder::cancelBuildIndex();
					sleep( 3 );
				}

			}
		} );

		try {

			update_option( SELF::CURRENT_ITEMS_SET_OPTION_KEY, $itemsSet, false );

			$indexer = new Indexer;

			$indexer->indexByPDO();

			delete_option( SELF::CURRENT_ITEMS_SET_OPTION_KEY );

		} catch ( \Exception $e ) {

		}

		return false;
	}

	public function is_other_process() {
		return ! $this->is_queue_empty();
	}

	/**
	 * Cancel Process
	 *
	 * Stop processing queue items, clear cronjob and delete batch.
	 *
	 * @return void
	 */
	public function cancel_process() {


		if ( ! $this->is_queue_empty() ) {

			$batch = $this->get_batch();

			$this->delete( $batch->key );

			wp_clear_scheduled_hook( $this->cron_hook_identifier );

		}
	}


	/**
	 * Delete queue
	 *
	 * @param string $key Key.
	 *
	 * @return $this
	 */
	public function delete( $key ) {
		if ( delete_site_option( $key ) ) {
			Builder::log( sprintf( _x( '[Searchable index] The queue <code>%s</code> was deleted ', 'Admin, logs', 'ajax-search-for-woocommerce' ), $key ) );
		};

		return $this;
	}

	/**
	 * Schedule event
	 */
	protected function schedule_event() {
		if ( ! wp_next_scheduled( $this->cron_hook_identifier ) ) {
			if ( wp_schedule_event( time(), $this->cron_interval_identifier, $this->cron_hook_identifier ) !== false ) {
				Builder::log( sprintf( _x( '[Searchable index] Schedule <code>%s</code> was created ', 'Admin, logs', 'ajax-search-for-woocommerce' ), $this->cron_hook_identifier ) );
			}
		}
	}

	/**
	 * Save queue
	 *
	 * @return $this
	 */
	public function save() {
		$key = $this->generate_key();

		if ( ! empty( $this->data ) ) {
			update_site_option( $key, $this->data );
			Builder::log( sprintf( _x( '[Searchable index] The queue <code>%s</code> was created', 'Admin, logs', 'ajax-search-for-woocommerce' ), $key ) );

		}

		return $this;
	}

	/**
	 * Dispatch job is queue is not empty
	 */
	public function maybe_dispatch() {
		if ( $this->is_queue_empty() ) {
			$this->complete();
		} else {
			$this->dispatch();
		}
	}

	/**
	 * Complete
	 *
	 * Override if applicable, but ensure that the below actions are
	 * performed, or, call parent::complete().
	 */
	public function complete() {
		parent::complete();

		Builder::addInfo( 'end_searchable_ts', time() );
		Builder::log( _x( '[Searchable index] Completed', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		sleep( 1 );
		Builder::maybeMarkAsCompleted();

	}
}
