<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Taxonomy;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;
use DgoraWcas\Helpers;
use DgoraWcas\Multilingual;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Indexer {

	private $taxonomy = '';

	public function __construct() {

	}

	/**
	 * Set taxonomy
	 *
	 * @param $taxonomy
	 *
	 * @return bool
	 */
	public function setTaxonomy( $taxonomy ) {
		$success = false;
		if ( taxonomy_exists( $taxonomy ) ) {
			$this->taxonomy = $taxonomy;
			$success        = true;
		}

		return $success;
	}

	/**
	 * Insert term to the index
	 *
	 * @param int term ID
	 *
	 * @return bool true on success
	 */
	public function insert( $termID ) {
		global $wpdb;
		$success = false;

		$termLang = Multilingual::getTermLang( $termID, $this->taxonomy );

		if ( Multilingual::isMultilingual() ) {
			$term = Multilingual::getTerm( $termID, $this->taxonomy, $termLang );
		} else {
			$term = get_term( $termID, $this->taxonomy );
		}


		if ( is_object( $term ) && ! is_wp_error( $term ) ) {

			$data = array(
				'term_id'        => $termID,
				'term_name'      => $term->name,
				'term_link'      => get_term_link( $term, $this->taxonomy ),
				'breadcrumbs'    => '',
				'total_products' => $term->count,
				'taxonomy'       => $this->taxonomy,
				'lang'           => $termLang
			);

			if ( $term->taxonomy === 'product_cat' ) {
				$breadcrumbs = Helpers::getTermBreadcrumbs( $termID, 'product_cat', array(), $termLang, array( $termID ) );

				// Fix: Remove last separator
				if ( ! empty( $breadcrumbs ) ) {
					$breadcrumbs = mb_substr( $breadcrumbs, 0, - 3 );
				}
				$data['breadcrumbs'] = $breadcrumbs;
			}

			$rows = $wpdb->insert(
				$wpdb->dgwt_wcas_tax_index,
				$data,
				array(
					'%d',
					'%s',
					'%s',
					'%s',
					'%d',
					'%s',
					'%s',
				)
			);

			if ( is_numeric( $rows ) ) {
				$success = true;
			}

		}

		do_action( 'dgwt/wcas/taxonomy_index/after_insert', $data, $termID, $this->taxonomy, $success );

		return $success;

	}

	/**
	 * Update term
	 *
	 * @param $termID
	 *
	 * @return bool true on success
	 */
	public function update( $termID ) {
		$this->delete( $termID );
		$this->insert( $termID );
	}

	/**
	 * Remove term from the index
	 *
	 * @param int term ID
	 *
	 * @return bool true on success
	 */
	public function delete( $termID ) {
		global $wpdb;
		$success = false;

		$wpdb->delete(
			$wpdb->dgwt_wcas_tax_index,
			array( 'term_id' => $termID ),
			array( '%d' )
		);

		return $success;

	}

	/**
	 * Build index for specific taxonomy
	 *
	 * @return array
	 */
	public function build() {

		$status = array(
			'success' => 0,
			'error'   => 0
		);

		$args = array(
			'taxonomy'   => $this->taxonomy,
			'hide_empty' => true
		);
		$args = apply_filters( 'dgwt/wcas/search/' . $this->taxonomy . '/args', $args );

		if ( Multilingual::isMultilingual() ) {
			$terms = Multilingual::getTermsInAllLangs( $this->taxonomy );
		} else {
			$terms = get_terms( apply_filters( 'dgwt/wcas/search/' . $this->taxonomy . '/args', $args ) );
		}

		if ( ! empty( $terms ) && is_array( $terms ) ) {
			foreach ( $terms as $term ) {
				if ( $this->insert( $term->term_id ) ) {
					$status['success'] ++;
				} else {
					$status['error'] ++;
				}
			}
		}

		return $status;
	}

	/**
	 * Wipe index
	 *
	 * @return bool
	 */
	public function wipe() {
		Database::remove();
		Builder::log( _x( '[Taxonomy index] Cleared', 'Admin, logs', 'ajax-search-for-woocommerce' ) );

		return true;
	}

	/**
	 * Remove DB table
	 *
	 * @return void
	 */
	public static function remove() {
		global $wpdb;

		$wpdb->hide_errors();

		$wpdb->query( "DROP TABLE IF EXISTS $wpdb->dgwt_wcas_tax_index" );

	}

}
