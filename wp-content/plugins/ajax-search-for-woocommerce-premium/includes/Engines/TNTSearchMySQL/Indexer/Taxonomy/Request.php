<?php

namespace DgoraWcas\Engines\TNTSearchMySQL\Indexer\Taxonomy;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Request {

	public static function handle() {

		Builder::addInfo( 'start_taxonomies_ts', time() );

		@set_time_limit( 3600 );

		if ( DGWT_WCAS()->settings->getOption( 'show_matching_categories' ) === 'on' ) {
			self::buildForTaxonomy( 'product_cat' );
		}

		if ( DGWT_WCAS()->settings->getOption( 'show_matching_tags' ) === 'on' ) {
			self::buildForTaxonomy( 'product_tag' );
		}

		if ( DGWT_WCAS()->brands->hasBrands() && DGWT_WCAS()->settings->getOption( 'show_matching_brands' ) === 'on' ) {
			self::buildForTaxonomy( DGWT_WCAS()->brands->getBrandTaxonomy() );
		}

		Builder::addInfo( 'end_taxonomies_ts', time() );


		return false;
	}

	private static function buildForTaxonomy( $taxonomy ) {
		Builder::log( sprintf( _x( '[Taxonomy index] Building %s...', 'Admin, logs', 'ajax-search-for-woocommerce' ), $taxonomy ) );

		$indexer = new Indexer;
		$indexer->setTaxonomy( $taxonomy );
		$status = $indexer->build();

		$error = false;
		if (
			( empty( $status['success'] ) && empty( $status['error'] ) )
			|| $status['error'] > 0
		) {
			$error = true;
		}
		$text = _x( '[Taxonomy index] Completed: %s, Not indexed: %s', 'Admin, logs', 'ajax-search-for-woocommerce' );
		Builder::log( sprintf( $text, $status['success'], $status['error'] ), $error );
	}

}
