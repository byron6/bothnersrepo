<?php

//https://github.com/trilbymedia/grav-plugin-tntsearch/blob/develop/classes/GravTNTSearch.php

namespace DgoraWcas\Engines\TNTSearchMySQL;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\AsyncRebuildIndex;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\BackgroundProductUpdater;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Scheduler;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Updater;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Builder;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable\AsyncProcess as AsyncProcessR;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable\AsyncProcess as AsyncProcessS;
use DgoraWcas\Helpers;
use DgoraWcas\Multilingual;
use DgoraWcas\Product;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class TNTSearch {

	/**
	 * Background processes for the readable index
	 *
	 * @var \DgoraWcas\Engines\TNTSearchMySQL\Indexer\Readable\AsyncProcess
	 *
	 */
	public $asynchBuildIndexR;

	/**
	 * Background processes for the searchable index
	 *
	 * @var \DgoraWcas\Engines\TNTSearchMySQL\Indexer\Searchable\AsyncProcess
	 *
	 */
	public $asynchBuildIndexS;

	/**
	 * Async rebuild whole search index
	 *
	 * @var object WP_Async_Request
	 *
	 */
	public $asyncRebuildIndex;

	/**
	 * TNTSearch constructor.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * TNTSearch init
	 *
	 * @return void
	 */
	private function init() {

		$this->asynchBuildIndexR = new AsyncProcessR();
		$this->asynchBuildIndexS = new AsyncProcessS();

		$this->asyncRebuildIndex = new AsyncRebuildIndex();

		$this->multilingualHooks();

		add_action( 'init', function () {

			if ( DGWT_WCAS()->engine === 'tntsearchMySql' && apply_filters( 'dgwt/wcas/override_search_results_page', true ) ) {
				$this->overrideSearchPage();
			}

			$this->initScheduler();
			$this->initUpdater();
			$this->initBackgroundProductUpdater();
		} );


		add_action( 'wp_ajax_dgwt_wcas_build_index', array( $this, 'ajaxBuildIndex' ) );
		add_action( 'wp_ajax_dgwt_wcas_stop_build_index', array( $this, 'ajaxStopBuildIndex' ) );
		add_action( 'wp_ajax_dgwt_wcas_build_index_heartbeat', array( $this, 'ajaxBuildIndexHeartbeat' ) );
		add_action( 'wp_ajax_dgwt_wcas_index_details_toggle', array( $this, 'ajaxBuildIndexDetailsToggle' ) );

		add_action( 'update_option_' . DGWT_WCAS_SETTINGS_KEY, array( $this, 'buildIndexOnchangeSettings' ), 10, 2 );

		$this->wpBgProcessingBasicAuthBypass();


		// Load dynamic prices
		add_action( 'wc_ajax_' . DGWT_WCAS_GET_PRICES_ACTION, array( $this, 'getDynamicPrices' ) );
	}


	/**
	 * Load background product updater
	 *
	 * @return void
	 */
	private function initBackgroundProductUpdater() {
		$productUpdater = new BackgroundProductUpdater();
		$productUpdater->init();
	}

	/**
	 * Load scheduler
	 *
	 * @return void
	 */
	private function initScheduler() {
		$scheduler = new Scheduler();
		$scheduler->init();
	}

	/**
	 * Load updater
	 *
	 * Listens for changes in posts and taxonomies terms and update index
	 *
	 * @return void
	 */
	private function initUpdater() {
		$updater = new Updater;
		$updater->init();
	}

	/**
	 * Load search page logic
	 *
	 * The class interferes with WordPress search resutls page
	 *
	 * @return void
	 */
	private function overrideSearchPage() {
		$sp = new SearchPage();
		$sp->init();
	}

	/**
	 * Build index
	 *
	 * Admin ajax callback for action "dgwt_wcas_build_index"
	 *
	 * @return void
	 */
	public function ajaxBuildIndex() {

		Builder::buildIndex();

		wp_send_json_success( array(
			'html' => Builder::renderIndexingStatus()
		) );
	}

	/**
	 * Stop building index
	 *
	 * Admin ajax callback for action "dgwt_wcas_stop_build_index"
	 *
	 * @return void
	 */
	public function ajaxStopBuildIndex() {

		Builder::addInfo( 'status', 'cancellation' );
		Builder::log( 'Stop building the index. Starting the cancellation process.' );

		Builder::cancelBuildIndex();

		sleep( 1 );

		wp_send_json_success( array(
			'html' => Builder::renderIndexingStatus()
		) );
	}

	/**
	 * Refresh index status
	 *
	 * Admin ajax callback for action "dgwt_wcas_build_index_heartbeat"
	 *
	 * @return void
	 */
	public function ajaxBuildIndexHeartbeat() {

		$status = Builder::getInfo( 'status' );
		$loop   = false;

		if ( in_array( $status, array( 'building', 'cancellation' ) ) ) {
			$loop = true;
		}

		wp_send_json_success( array(
			'html' => Builder::renderIndexingStatus(),
			'loop' => $loop
		) );
	}


	/**
	 * Show/hide indexer logs
	 *
	 * Admin ajax callback for action "dgwt_wcas_index_details_toggle"
	 *
	 * @return void
	 */
	public function ajaxBuildIndexDetailsToggle() {
		delete_transient( Builder::DETAILS_DISPLAY_KEY );

		if ( ! empty( $_REQUEST['display'] ) && $_REQUEST['display'] == 'true' ) {
			set_transient( Builder::DETAILS_DISPLAY_KEY, 1, 3600 );
		} else {
			set_transient( Builder::DETAILS_DISPLAY_KEY, 0, 3600 );
		}
	}


	/**
	 * Rebuild index after changing some options
	 *
	 * @return void
	 */
	public function buildIndexOnchangeSettings( $newSettings, $oldSettings ) {

		if ( DGWT_WCAS()->engine !== 'tntsearchMySql' ) {
			return;
		};

		$listenKeys = array(
			'search_in_product_content',
			'search_in_product_excerpt',
			'search_in_product_sku',
			'search_in_product_attributes',
			'search_in_custom_fields',
			'search_in_brands',
			'search_in_product_categories',
			'search_in_product_tags',
			'exclude_out_of_stock',
			'filter_products_rules',
			'show_matching_categories',
			'show_matching_tags',
			'show_matching_brands',
			'show_matching_pages',
			'show_matching_posts',
			'search_synonyms'
		);

		foreach ( $listenKeys as $key ) {
			if (
				is_array( $newSettings )
				&& is_array( $oldSettings )
				&& array_key_exists( $key, $newSettings )
				&& array_key_exists( $key, $oldSettings )
			) {
				if ( $newSettings[ $key ] != $oldSettings[ $key ] ) {

					$this->asyncRebuildIndex->dispatch();

					break;
				}
			}
		}

	}

	/**
	 * Bypass for WP Background Processing when BasicAuth is enabled
	 *
	 * @return void
	 */
	public function wpBgProcessingBasicAuthBypass() {

		$authorization = Helpers::getBasicAuthHeader();
		if ( $authorization ) {

			add_filter( 'http_request_args', function ( $r, $url ) {

				if ( strpos( $url, 'wp-cron.php' ) !== false
				     || strpos( $url, 'admin-ajax.php' ) !== false ) {

					$r['headers']['Authorization'] = Helpers::getBasicAuthHeader();

				}

				return $r;
			}, 10, 2 );
		}

	}

	/**
	 * Support for multilingual
	 *
	 * @return void
	 */
	private function multilingualHooks() {

		// Add currency ajax actions to WooCommerce Multilingual
		add_filter( 'wcml_multi_currency_ajax_actions', function ( $ajaxActions ) {

			$ajaxActions[] = 'wcas_build_readable_index';
			$ajaxActions[] = 'dgwt_wcas_result_details';
			$ajaxActions[] = 'wcas_async_product_update';

			return $ajaxActions;

		}, 10, 1 );

		// Currency
		add_filter( 'wcml_client_currency', function ( $currency ) {

			if ( ( defined( 'DGWT_WCAS_READABLE_INDEX_TASK' ) || defined( 'DGWT_WCAS_UPDATER_TASK' ) )
			     && ! empty( Multilingual::getCurrentCurrency() ) ) {
				$currency = Multilingual::getCurrentCurrency();
			}

			return $currency;
		}, 50, 1 );
	}

	/**
	 * Get prices for products
	 * AJAX callback
	 *
	 * @return void
	 */
	public function getDynamicPrices() {

		if ( ! defined( 'DGWT_WCAS_AJAX' ) ) {
			define( 'DGWT_WCAS_AJAX', true );
		}

		$prices = array();

		if ( ! empty( $_POST['items'] ) && array( $_POST['items'] ) ) {
			foreach ( $_POST['items'] as $postID ) {
				if ( ! empty( $postID ) && is_numeric( $postID ) ) {

					$postID = absint( $postID );

					$product = new Product($postID);

					if($product->isCorrect()){
						$prices[] = (object) array(
							'id' => $postID,
							'price' => $product->getPriceHTML()
						);
					}

				}
			}
		}

		wp_send_json_success( $prices );
	}

}
