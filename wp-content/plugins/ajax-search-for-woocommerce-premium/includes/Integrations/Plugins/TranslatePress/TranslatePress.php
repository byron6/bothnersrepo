<?php
/**
 * @dgwt_wcas_premium_only
 */
namespace DgoraWcas\Integrations\Plugins\TranslatePress;

use DgoraWcas\Engines\TNTSearchMySQL\Indexer\SourceQuery;
use DgoraWcas\Engines\TNTSearchMySQL\Indexer\PostsSourceQuery;
use DgoraWcas\Helpers;
use DgoraWcas\Multilingual;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Integration with TranslatePress - Multilingual
 *
 * Plugin URL: https://translatepress.com/
 * Author: Cozmoslabs, Razvan Mocanu, Madalin Ungureanu, Cristophor Hurduban
 */
class TranslatePress {
	public function init() {
		if ( ! defined( 'TRP_PLUGIN_VERSION' ) && ! class_exists( 'TRP_Translate_Press' ) ) {
			return;
		}
		if ( version_compare( TRP_PLUGIN_VERSION, '1.9.4' ) < 0 ) {
			return;
		}

		/*
		 * Multilingual
		 */
		add_filter( 'dgwt/wcas/multilingual/provider', array( $this, 'provider' ) );
		add_filter( 'dgwt/wcas/multilingual/default-language', array( $this, 'defaultLanguage' ) );
		add_filter( 'dgwt/wcas/multilingual/current-language', array( $this, 'currentLanguage' ) );
		add_filter( 'dgwt/wcas/multilingual/languages', array( $this, 'languages' ) );
		add_filter( 'dgwt/wcas/multilingual/terms-in-all-languages', array( $this, 'termsInAllLanguages' ), 10, 2 );
		add_filter( 'dgwt/wcas/multilingual/terms-in-language', array( $this, 'termsInLanguage' ), 10, 3 );
		add_filter( 'dgwt/wcas/multilingual/term', array( $this, 'term' ), 10, 4 );

		/*
		 * Searchable index
		 */
		add_filter( 'dgwt/wcas/tnt/source_query/data', array( $this, 'addLanguageData' ), 10, 3 );
		add_filter( 'dgwt/wcas/tnt/post_source_query/data', array( $this, 'addPostLanguageData' ), 10, 3 );
		add_filter( 'dgwt/wcas/indexer/searchable-set-items-count', array( $this, 'searchableSetItemsCount' ) );
		add_action( 'dgwt/wcas/searchable_index/bg_processing/before_task', array(
			$this,
			'prepareSearchableBgProcessingHooks'
		) );

		/*
		 * Readable index
		 */
		add_action( 'dgwt/wcas/readable_index/after_insert', array(
			$this,
			'insertTranslatedDataIntoReadableIndex'
		), 10, 4 );
		add_action( 'dgwt/wcas/taxonomy_index/after_insert', array(
			$this,
			'insertTranslatedDataIntoTaxonomyIndex'
		), 10, 4 );
		add_action( 'dgwt/wcas/variation_index/after_insert', array(
			$this,
			'insertTranslatedDataIntoVariationIndex'
		), 10, 4 );
		add_action( 'dgwt/wcas/readable_index/bg_processing/before_task', array(
			$this,
			'prepareReadableBgProcessingHooks'
		) );

		add_filter( 'dgwt/wcas/indexer/readable-set-items-count', array( $this, 'readableSetItemsCount' ) );

		/*
		 * Search page
		 */
		add_action( 'plugins_loaded', array( $this, 'removeHooksOnSearchPage' ), 20 );

		/*
		 * Other
		 */
		add_filter( 'trp_skip_selectors_from_dynamic_translation', array(
			$this,
			'skipSelectorsFromDynamicTranslation'
		) );
		add_filter( 'dgwt/wcas/indexer/process_status/progress', array(
			$this,
			'correctProcessProgress'
		), 10, 3 );
	}

	/**
	 * Set provider to TranslatePress
	 *
	 * @param string $provider
	 *
	 * @return string
	 */
	public function provider( $provider ) {
		$provider = 'TranslatePress';

		return $provider;
	}

	/**
	 * Get default language
	 *
	 * @param string $defaultLang
	 *
	 * @return string
	 */
	public function defaultLanguage( $defaultLang ) {
		$trp          = \TRP_Translate_Press::get_trp_instance();
		$trp_settings = $trp->get_component( 'settings' );
		$settings     = $trp_settings->get_settings();

		$slug = self::getLanguageSlug( $settings['default-language'] );
		if ( ! empty( $slug ) ) {
			$defaultLang = $slug;
		}

		return $defaultLang;
	}

	/**
	 * Get current language
	 *
	 * @param string $currentLang
	 *
	 * @return string
	 */
	public function currentLanguage( $currentLang ) {
		global $TRP_LANGUAGE;

		$slug = self::getLanguageSlug( $TRP_LANGUAGE );
		if ( ! empty( $slug ) ) {
			$currentLang = $slug;
		}

		return $currentLang;
	}

	/**
	 * Get defined languages
	 *
	 * @param array $langs
	 *
	 * @return array
	 */
	public function languages( $langs ) {
		$codes = array_keys( trp_get_languages() );
		if ( ! empty( $codes ) ) {
			$langs = array();
			foreach ( $codes as $code ) {
				$slug = self::getLanguageSlug( $code );
				$slug = strtolower( $slug );
				if ( ! empty( $slug ) && Multilingual::isLangCode( $slug ) ) {
					$langs[] = $slug;
				}
			}
		}

		return $langs;
	}

	/**
	 * Get terms in all languages
	 *
	 * @param $terms
	 * @param $taxonomy
	 *
	 * @return int|\WP_Error|\WP_Term[]
	 */
	public function termsInAllLanguages( $terms, $taxonomy ) {
		$terms = get_terms( array(
			'taxonomy'   => $taxonomy,
			'hide_empty' => true,
		) );

		return $terms;
	}

	/**
	 * Get term in specific language
	 *
	 * @param $terms
	 * @param $args
	 * @param $lang
	 *
	 * @return int|\WP_Error|\WP_Term[]
	 */
	public function termsInLanguage( $terms, $args, $lang ) {
		$terms = get_terms( array(
			'taxonomy'   => $args['taxonomy'],
			'hide_empty' => true,
		) );

		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				if ( isset( $term->name ) ) {
					$term->name = $this->translate( $term->name, $lang );
				}
			}
		}

		return $terms;
	}

	/**
	 * Get translated term
	 *
	 * @param $term
	 * @param $termID
	 * @param $taxonomy
	 * @param $lang
	 *
	 * @return array|int|object|\WP_Error|\WP_Term|null
	 */
	public function term( $term, $termID, $taxonomy, $lang ) {
		$term = get_term( $termID, $taxonomy );
		if ( isset( $term->name ) ) {
			$term->name = $this->translate( $term->name, $lang );
		}

		return $term;
	}

	/**
	 * Prepare products data with translations for the indexer
	 *
	 * This filter:
	 * - adds language to received objects
	 * - for each language, it makes a copy of the object and translates all its attributes
	 *
	 * @param array $data
	 * @param SourceQuery $sourceQuery
	 * @param boolean $onlyIDs
	 *
	 * @return array
	 */
	public function addLanguageData( $data, $sourceQuery, $onlyIDs) {
		if ( empty( $data ) || ! is_array( $data ) ) {
			return $data;
		}

		if ( $onlyIDs ) {
			return $data;
		}

		$defaultLang = Multilingual::getDefaultLanguage();

		// Set default language to products
		foreach ( $data as $index => $row ) {
			$data[ $index ]['lang'] = $defaultLang;
		}

		$langs           = Multilingual::getLanguages();
		$additionalLangs = array_diff( $langs, array( $defaultLang ) );
		$additionalData  = array();

		foreach ( $additionalLangs as $lang ) {
			foreach ( $data as $row ) {
				$newRow               = $row;
				$newRow['post_title'] = $this->translate( $newRow['post_title'], $lang );
				if ( isset( $newRow['post_content'] ) ) {
					$newRow['post_content'] = $this->translate( $newRow['post_content'], $lang );
				}
				if ( isset( $newRow['post_excerpt'] ) ) {
					$newRow['post_excerpt'] = $this->translate( $newRow['post_excerpt'], $lang );
				}
				if ( isset( $newRow['brand'] ) ) {
					$newRow['brand'] = $this->translateJoinded( $newRow['brand'], $lang, ' | ' );
				}
				if ( isset( $newRow['sku'] ) ) {
					$newRow['sku'] = $this->translate( $newRow['sku'], $lang );
				}
				if ( isset( $newRow['sku_variations'] ) ) {
					$newRow['sku_variations'] = $this->translateJoinded( $newRow['sku_variations'], $lang, ' | ' );
				}
				if ( isset( $newRow['variations_description'] ) ) {
					$newRow['variations_description'] = $this->translateJoinded( $newRow['variations_description'], $lang, ' | ' );
				}
				if ( isset( $newRow['attributes'] ) ) {
					$newRow['attributes'] = $this->translateJoinded( $newRow['attributes'], $lang, ' | ' );
				}
				foreach ( $newRow as $key => $value ) {
					if ( strpos( $key, 'custom_field_' ) === 0 ) {
						$newRow[ $key ] = $this->translate( $newRow[ $key ], $lang );
					}
				}
				if ( isset( $newRow['tax_product_tag'] ) ) {
					$newRow['tax_product_tag'] = $this->translateJoinded( $newRow['tax_product_tag'], $lang, ' | ' );
				}
				if ( isset( $newRow['tax_product_cat'] ) ) {
					$newRow['tax_product_cat'] = $this->translateJoinded( $newRow['tax_product_cat'], $lang, ' | ' );
				}
				$newRow['lang']   = $lang;
				$additionalData[] = $newRow;
			}
		}

		$data = array_merge( $data, $additionalData );

		return $data;
	}

	/**
	 * Prepare post/page data with translations for the indexer
	 *
	 * This filter:
	 * - adds language to received objects
	 * - for each language, it makes a copy of the object and translates all its attributes
	 *
	 * @param array $data
	 * @param PostsSourceQuery $postsSourceQuery
	 * @param boolean $onlyIDs
	 *
	 * @return array
	 */
	public function addPostLanguageData( $data, $postsSourceQuery, $onlyIDs ) {
		if ( empty( $data ) || ! is_array( $data ) ) {
			return $data;
		}

		if ( $onlyIDs ) {
			return $data;
		}

		$defaultLang = Multilingual::getDefaultLanguage();

		// Set default language to products
		foreach ( $data as $index => $row ) {
			$data[ $index ]['lang'] = $defaultLang;
		}

		$langs           = Multilingual::getLanguages();
		$additionalLangs = array_diff( $langs, array( $defaultLang ) );
		$additionalData  = array();

		foreach ( $additionalLangs as $lang ) {
			foreach ( $data as $row ) {
				$newRow               = $row;
				$newRow['post_title'] = $this->translate( $newRow['post_title'], $lang );
				$newRow['lang']       = $lang;
				$additionalData[]     = $newRow;
			}
		}

		$data = array_merge( $data, $additionalData );

		return $data;
	}

	/**
	 * Prepare to run the searchable indexing process
	 */
	public function prepareSearchableBgProcessingHooks() {
		// Loading a TranslatePress component that normally does not load during AJAX queries
		$trp = \TRP_Translate_Press::get_trp_instance();
		$trp->init_machine_translation();
	}

	/**
	 * Adjusting the size of the queue to the number of languages
	 *
	 * @param $count
	 *
	 * @return int
	 */
	public function searchableSetItemsCount( $count ) {
		$langs = Multilingual::getLanguages();
		if ( count( $langs ) > 1 ) {
			$count = (int) floor( 100 / count( $langs ) );
		}

		return $count;
	}


	/**
	 * Insert translated data into readable index (for products, posts, pages)
	 *
	 * @param $data
	 * @param $postID
	 * @param $postType
	 * @param $success
	 */
	public function insertTranslatedDataIntoReadableIndex( $data, $postID, $postType, $success ) {
		global $wpdb;

		if ( ! $success ) {
			return;
		}
		if ( Multilingual::getProvider() !== 'TranslatePress' ) {
			return;
		}

		$defaultLang     = Multilingual::getDefaultLanguage();
		$langs           = Multilingual::getLanguages();
		$additionalLangs = array_diff( $langs, array( $defaultLang ) );

		foreach ( $additionalLangs as $lang ) {
			$translatedData                = $data;
			$translatedData['description'] = $this->translate( $translatedData['description'], $lang );
			$translatedData['name']        = $this->translate( $translatedData['name'], $lang );
			$translatedData['lang']        = $lang;
			$translatedData['url']         = $this->convertUrl( $translatedData['url'], $lang );

			$rows = $wpdb->insert(
				$wpdb->dgwt_wcas_index,
				$translatedData,
				array(
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%f',
					'%f',
					'%d',
					'%d',
					'%s',
				)
			);
		}
	}

	/**
	 * Insert translated taxonomy data into readable index
	 *
	 * @param $data
	 * @param $termID
	 * @param $taxonomy
	 * @param $success
	 */
	public function insertTranslatedDataIntoTaxonomyIndex( $data, $termID, $taxonomy, $success ) {
		global $wpdb;

		if ( ! $success ) {
			return;
		}
		if ( Multilingual::getProvider() !== 'TranslatePress' ) {
			return;
		}

		$defaultLang     = Multilingual::getDefaultLanguage();
		$langs           = Multilingual::getLanguages();
		$additionalLangs = array_diff( $langs, array( $defaultLang ) );

		foreach ( $additionalLangs as $lang ) {
			$translatedData              = $data;
			$translatedData['term_name'] = $this->translate( $translatedData['term_name'], $lang );
			$translatedData['term_link'] = $this->convertUrl( $translatedData['term_link'], $lang );
			$translatedData['lang']      = $lang;

			$rows = $wpdb->insert(
				$wpdb->dgwt_wcas_tax_index,
				$translatedData,
				array(
					'%d',
					'%s',
					'%s',
					'%s',
					'%d',
					'%s',
					'%s',
				)
			);
		}
	}

	/**
	 * Insert translated taxonomy data into readable index
	 *
	 * @param $rows
	 * @param $parentProductID
	 * @param $parentProductSKU
	 * @param $lang
	 */
	public function insertTranslatedDataIntoVariationIndex( $rows, $parentProductID, $parentProductSKU, $lang ) {
		global $wpdb;

		if ( empty( $rows ) ) {
			return;
		}
		if ( Multilingual::getProvider() !== 'TranslatePress' ) {
			return;
		}

		$langs           = Multilingual::getLanguages();
		$additionalLangs = array_diff( $langs, array( $lang ) );

		foreach ( $additionalLangs as $lang ) {
			$sql = "INSERT INTO $wpdb->dgwt_wcas_var_index (
                variation_id,
                product_id,
                sku,
                title,
                description,
                image,
                url,
                html_price,
                lang) VALUES ";

			$i = 1;
			foreach ( $rows as $row ) {
				$translatedRow = $row;
				// Warning: title may has different delimiter
				$translatedRow['title']       = $this->translateJoinded( $translatedRow['title'], $lang, ', ' );
				$translatedRow['description'] = $this->translate( $translatedRow['description'], $lang );
				$translatedRow['url']         = $this->convertUrl( $translatedRow['url'], $lang );
				$translatedRow['sku']         = $this->translate( $translatedRow['sku'], $lang );
				$translatedRow['lang']        = $lang;

				$sql .= $wpdb->prepare( "(%d, %d, %s, %s, %s, %s, %s, %s, %s)", $translatedRow );
				$sql .= count( $rows ) === $i ? ';' : ',';

				$i ++;
			}

			$wpdb->query( $sql );
		}
	}

	/**
	 * Get lang slug from code
	 *
	 * @param string $code
	 *
	 * @return string
	 */
	public static function getLanguageSlug( $code ) {
		if ( Multilingual::getProvider() !== 'TranslatePress' ) {
			return '';
		}

		$trp          = \TRP_Translate_Press::get_trp_instance();
		$trp_settings = $trp->get_component( 'settings' );
		$settings     = $trp_settings->get_settings();

		if ( isset( $settings['url-slugs'][ $code ] ) ) {
			return $settings['url-slugs'][ $code ];
		}

		return '';
	}

	/**
	 * Disable TranslatePress hooks on serach page
	 */
	public function removeHooksOnSearchPage() {
		$trp    = \TRP_Translate_Press::get_trp_instance();
		$search = $trp->get_component( 'search' );
		remove_filter( 'pre_get_posts', array( $search, 'trp_search_filter' ), 10 );
		remove_filter( 'get_search_query', array( $search, 'trp_search_query' ), 10 );
	}

	/**
	 * Adjusting the size of the queue to the number of languages
	 *
	 * @param $count
	 *
	 * @return int
	 */
	public function readableSetItemsCount( $count ) {
		$langs = Multilingual::getLanguages();
		if ( count( $langs ) > 1 ) {
			$count = (int) floor( 25 / count( $langs ) );
		}

		return $count;
	}

	/**
	 * Prepare to run the readable indexing process
	 */
	public function prepareReadableBgProcessingHooks() {
		/**
		 * When we get product, post and page permalinks, the TRP_Url_Converter::add_language_to_home_url()
		 * method is used, but in its inside the is_admin_request() method is called,
		 * which when it returns "true", causes that we do not get the correct permalinks
		 * in other languages. These permalinks are needed to build the readable index.
		 * This filter is the only method to prevent this problem.
		 */
		add_filter( 'admin_url', function ( $url, $path, $blog_id ) {
			if ( Helpers::is_running_inside_class( 'TRP_Url_Converter' ) ) {
				$url = '-';
			}

			return $url;
		}, 10, 3 );

		$trp = \TRP_Translate_Press::get_trp_instance();
		$trp->init_machine_translation();
	}

	/**
	 * Prevent to dynamic translate search results
	 *
	 * @param array $selectors
	 *
	 * @return array
	 */
	public function skipSelectorsFromDynamicTranslation( $selectors ) {
		$selectors[] = '.dgwt-wcas-suggestions-wrapp';
		$selectors[] = '.dgwt-wcas-details-wrapp';

		return $selectors;
	}

	/**
	 * Indexing progress correction
	 *
	 * Due to the addition of indexed data on the fly, the progress is poorly calculated and needs to be adjusted.
	 *
	 * @param $progress
	 * @param $percentR
	 * @param $percentS
	 *
	 * @return float|int
	 */
	public function correctProcessProgress( $progress, $percentR, $percentS ) {
		$count = count( Multilingual::getLanguages() );
		if ( $count > 1 ) {
			$progress = ( $percentR + ( $percentS / $count ) ) / 2;
		}

		return $progress;
	}

	/**
	 * Convert URL to given language
	 *
	 * @param $url
	 * @param $lang
	 *
	 * @return string
	 */
	private function convertUrl( $url, $lang ) {
		$output       = '';
		$trp          = \TRP_Translate_Press::get_trp_instance();
		$urlConverter = $trp->get_component( 'url_converter' );

		$code = $this->getLanguageCode( $lang );
		if ( $code ) {
			$output = $urlConverter->get_url_for_language( $code, $url, '' );
		}

		return $output;
	}

	/**
	 * Get language code from slug (en >> en_US)
	 *
	 * @param $slug
	 *
	 * @return false|int|string
	 */
	private function getLanguageCode( $slug ) {
		$trp = \TRP_Translate_Press::get_trp_instance();
		/**
		 * @var \TRP_Settings $trpSettings
		 */
		$trpSettings = $trp->get_component( 'settings' );
		$settings    = $trpSettings->get_settings();

		return array_search( $slug, $settings['url-slugs'] );
	}

	/**
	 * Translate content to given language
	 *
	 * @param $content
	 * @param $lang
	 *
	 * @return string
	 */
	private function translate( $content, $lang ) {
		global $TRP_LANGUAGE;

		$output = '';
		$trp    = \TRP_Translate_Press::get_trp_instance();
		/**
		 * @var \TRP_Translation_Render $translationRender
		 */
		$translationRender = $trp->get_component( 'translation_render' );

		$trpLanguageBackup = $TRP_LANGUAGE;
		$code              = $this->getLanguageCode( $lang );
		if ( $code ) {
			$TRP_LANGUAGE = $code;
			$output       = $translationRender->translate_page( $content );
			$TRP_LANGUAGE = $trpLanguageBackup;
		}

		return $output;
	}

	/**
	 * Translate content divided by delimiter
	 *
	 * Eg. "Black | Blue | Green" - we have 3 strings to translate separated by " | " delimiter
	 *
	 * @param $content
	 * @param $lang
	 * @param $delimiter
	 *
	 * @return string
	 */
	private function translateJoinded( $content, $lang, $delimiter ) {
		$arr           = explode( $delimiter, $content );
		$arrTranslated = array();
		if ( ! empty( $arr ) ) {
			foreach ( $arr as $item ) {
				$arrTranslated[] = $this->translate( $item, $lang );
			}
		}

		return join( $delimiter, $arrTranslated );
	}
}
